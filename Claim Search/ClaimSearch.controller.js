angular.module("responsive.coaches")
    .controller("Claims", ["$scope", "$http", "$window", "$element", "Coach", function ($scope, $http, $window, $element, Coach) {
        "use strict";
        angular.extend($scope, new Coach($scope, $element));
        $scope.isSearch = true;
        $scope.claimSummaryParams = {};
        $scope.dateFrom;
        $scope.dateTo;
        $scope.currentPosition = 1;
        $scope.resultCount;
        $scope.totalPages;
        var pageSize = 7;


        // $http.get("/rest/bpm/wle/v1/exposed/service?includeServiceSubTypes=url")
        //     .then(function (response) {

        //         $scope.MyResponse = response.data;
        //         console.log($scope.MyResponse);
        //         for (var i = 0; i < $scope.MyResponse.data.exposedItemsList.length; i++) {
        //             if ($scope.MyResponse.data.exposedItemsList[i].display == 'Claim-OD-Search')
        //                 $scope.RedirectURL = $scope.MyResponse.data.exposedItemsList[i].runURL + "&tw.local.FeatureSummaryId=";
        //         }
        //         console.log($scope.RedirectURL);

        //     });


        $(".datetimepickerfrom").on("dp.change", function(e) {
		
			$scope.dateFrom = $(".datetimepickerfrom").val();
            $scope.$apply();
            console.log($scope.dateFrom);
        });
        

        $(".datetimepickerto").on("dp.change", function(e) {
		
			$scope.dateTo = $(".datetimepickerto").val();
            $scope.$apply();
            console.log($scope.dateTo);
        });
        $("select").tooltip();
        $(".cssbutton").tooltip({
            placement: "top"
        });
        
        $scope.getClaimSearch = function (position) {

            $scope.currentPosition = position;
            var input = {
                "SearchClaimInput":  $scope.claimSummaryParams,
                "Position" : (position-1)*7,
                "NumberOfRows" : 7
                
            }

            var serviceArgs = {
                params: JSON.stringify(input),
                load: function (data) {

                    debugger;
                    if (!!data.resultCount.items && data.resultCount.items.length > 0){
                    $scope.resultCount = data.resultCount.items[0].RESULTCOUNT;
                    }
                    $scope.claimSummary = (!!data && !!data.SearchClaimOutput.items) ? data.SearchClaimOutput.items : [];
                    console.log($scope.claimSummary.length);
                    $scope.isSearch = false;
                    if ($scope.claimSummary.length > 0) {
                        $scope.isClaimFound = true;
                    } else {
                        $scope.isClaimFound = false;
                    }
                    $scope.selectedClaim = -1;

                    $scope.assignPagVariables();

                },
                error: function (e) {
                    $scope.claimSummary = [];
                }
            };

            $scope.context.options.searchClaim(serviceArgs);

        };

        $scope.goBack = function(){
            $scope.isSearch = true;
        }

        $scope.highlight =function(index){
            $scope.selectedClaim = index;
        }
        $scope.openClaim = function(index){
            $scope.selectedClaim = index;
           $scope.bindingValue.ClaimNumber = $scope.claimSummary[index].CLAIMNUMBER;
           $scope.bindingValue.FeatureSummaryId = $scope.claimSummary[index].FEATURESUMMARYID;
            // console.log($scope.RedirectURL + $scope.claimSummary[index].FEATURESUMMARYID);
            // $window.open($scope.RedirectURL + $scope.claimSummary[index].FEATURESUMMARYID);
            // $scope.$apply();
        }

        $scope.modalOpen = function (claim){
            $scope.claim = claim;
            $("#ClaimDetailsModal").modal({ backdrop: 'static', keyboard: false }, 'show');

        }

        $scope.assignPagVariables = function(){

            $scope.totalPages = Math.ceil($scope.resultCount/pageSize);

            switch($scope.totalPages){

                case 1:
                    $scope.previousPage = 0;
                    $scope.nextPage = 0;
                    $scope.middlePage = 1;
                    break;
                case 2:
                    if($scope.currentPosition == 1){
                        $scope.nextPage = 2;
                        $scope.previousPage = 0;
                        
                    }else if($scope.currentPosition == 2){
                        $scope.previousPage = 1;
                        $scope.nextPage = 0;
                    }
                    $scope.middlePage = $scope.currentPosition;
                    break;
                default:
                    if($scope.currentPosition == 1){

                        $scope.previousPage = 1;
                        $scope.middlePage = $scope.currentPosition + 1;
                        $scope.nextPage = $scope.currentPosition + 2;

                    }else if($scope.currentPosition == $scope.totalPages){

                        $scope.previousPage = $scope.currentPosition - 2;
                        $scope.middlePage = $scope.currentPosition - 1;
                        $scope.nextPage = $scope.currentPosition;
                        
                    }else{
                        $scope.previousPage = $scope.currentPosition - 1;
                        $scope.middlePage = $scope.currentPosition;
                        $scope.nextPage = $scope.currentPosition + 1;
                    }
                    
            }

            $scope.$apply();

        }


        $scope.removeFilter = function(){
            $scope.claimSummaryParams = {};
            $scope.dateFrom = null;
            $scope.dateTo = null;
        }

        $scope.getReport = function(type){

            debugger;
            var input = {
                "exportReport": {
                    "TemplateName" : "ClaimOD.rptdesign",
                    "OutputFileType" : type,
                    "Params" : {
                        "Param" : [{
                            "ParamName" : "featureSummaryId",
                            "ParamValue" : $scope.claimSummary[$scope.selectedClaim].FEATURESUMMARYID
                        },
                        {
                            "ParamName" : "featureNumber",
                            "ParamValue" : $scope.claimSummary[$scope.selectedClaim].FEATURENUMBER
                        }]
                    }
                }
            }

            var serviceArgs = {
                params: JSON.stringify(input),
                load: function (data) {

                    debugger;
                    console.log(data);

                    var fileContent = 'data:application/octet-stream;base64,' + data.fileContent;
             

                    $("#reportGen").attr('href', fileContent);
                    var downloadFileName = "Feature Number:" + $scope.claimSummary[$scope.selectedClaim].FEATURENUMBER +   " " +new Date().toLocaleDateString() + "." + type; 
                    $('#reportGen').attr('download', downloadFileName);
                    $('#reportGen').get(0).click();
                },
                error: function (e) {
                    debugger;
                    alert("Error " + e);
                   
                }
            };

            $scope.context.options.generateReport(serviceArgs);



        }

    }]);