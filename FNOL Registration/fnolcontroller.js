/*FNOL-Call Center Controller */
angular
	.module("responsive.coaches")
	/*Pincode service*/
	.service('PincodeAutoCompleteService', function () {

		this.pincodeService = function ($scope, input) {
			if (input.pincode != null && input.pincode.length >= 3) {

				if ($scope.cachedPincodeAutocomplete[input.pincode]) {
					$scope.pincodeItems = $scope.cachedPincodeAutocomplete[input.pincode];

					return $scope.pincodeItems;
				}

				else {
					var pincodeServiceArgs = {
						params: JSON.stringify(input),
						load: function (data) {

							$scope.pincodeItems = (!!data && !!data.results && !!data.results.items) ? data.results.items : [];



							$scope.cachedPincodeAutocomplete[input.pincode] = $scope.pincodeItems;
							$scope.$apply();
							return $scope.pincodeItems;
						},
						error: function (e) {

						}
					};
					$scope.context.options.PincodeAutoCompleteOnCityState(pincodeServiceArgs);
				}
			}
		}
	})

	.service('AreaAutoCompleteService', function () {

		this.areaService = function ($scope, input) {
			if (input.searchValue != null && input.searchValue.length >= 3) {
				var areaServiceArgs = {
					params: JSON.stringify(input),
					load: function (data) {

						$scope.areaItems = (!!data && !!data.results && !!data.results.items) ? data.results.items : [];
						$scope.$apply();
						return $scope.areaItems;
					},
					error: function (e) {

					}
				};
				$scope.context.options.areaAutocompletionService(areaServiceArgs);
			}
		}
	}
	)

	.service('CityOnStateAutoCompleteService', function () {

		this.cityOnStateService = function ($scope, input) {
			if (input.City != null && input.City.length >= 3) {
				var CityOnStateServiceArgs = {
					params: JSON.stringify(input),
					load: function (data) {

						$scope.CityItems = (!!data && !!data.results && !!data.results.items) ? data.results.items : [];
						$scope.$apply();
						return $scope.CityItems;
					},
					error: function (e) {

					}
				};
				$scope.context.options.CityOnStateAutocompletionService(CityOnStateServiceArgs);
			}
		}
	}
	)

	.service('StateAutoCompleteService', function () {

		this.StateService = function ($scope, input) {
			if (input.StateAutoCompleteServiceInput != null && input.StateAutoCompleteServiceInput.length >= 3) {
				var StateServiceArgs = {
					params: JSON.stringify(input),
					load: function (data) {

						$scope.StateItems = (!!data && !!data.StateAutoCompleteServiceOutput && !!data.StateAutoCompleteServiceOutput.items) ? data.StateAutoCompleteServiceOutput.items : [];
						$scope.$apply();
						return $scope.StateItems;
					},
					error: function (e) {

					}
				};
				$scope.context.options.StateAutocompletionService(StateServiceArgs);
			}
		}
	}
	)



	.directive("datePicker", function () {
		return {
			restrict: "A",
			require: "ngModel",
			link: function (scope, element, attrs, ngModelCtrl) {
				if (attrs.whichDate && attrs.whichDate == "past") {
					$(element).datetimepicker({
						format: "DD-MM-YYYY",
						maxDate: moment(),
						useCurrent: false
					});
					var parent = $(element).parent().children('.input-group-addon');
					parent.click(function () {
						$(element).data('DateTimePicker').toggle();
					});

				} else if (attrs.whichDate && attrs.whichDate == "future") {
					$(element).datetimepicker({
						format: "DD-MM-YYYY",
						minDate: moment(),
						useCurrent: false
					});

					var parent = $(element).parent().children('.input-group-addon');
					parent.click(function () {
						$(element).data('DateTimePicker').toggle();
					});

				}


				else if (attrs.whichDate && attrs.whichDate == "year") {

					debugger;
					$(element).datetimepicker({
						format: "YYYY",

						minDate: moment(),
						maxDate: moment(),
						useCurrent: false
					});

					var parent = $(element).parent().children('.input-group-addon');
					parent.click(function () {
						$(element).data('DateTimePicker').toggle();
					});
				}

				else {
					$(element).datetimepicker({
						format: "DD-MM-YYYY",
						useCurrent: false
					});

					var parent = $(element).parent().children('.input-group-addon');
					parent.click(function () {
						$(element).data('DateTimePicker').toggle();
					});

				}

				$(element).on("dp.change", function (e) {
					if (!e.date) {
						ngModelCtrl.$setViewValue(null);
					} else {
						ngModelCtrl.$setViewValue(moment(e.date).format("YYYY-MM-DD"));
					}
					scope.$apply();
				});
				$(parent).on("dp.change", function (e) {
					if (!e.date) {
						ngModelCtrl.$setViewValue(null);
					} else {
						ngModelCtrl.$setViewValue(moment(e.date).format("YYYY-MM-DD"));
					}
					scope.$apply();
				});
				$(element).blur(function () {
					var temp = $(element).val();
					$(element).val(temp);
					$(element).trigger("input");
					scope.$apply();
				});
			}
		};
	})


	.directive('forWithoutPercentage', function () {
		return {
			require: 'ngModel',
			restrict: 'A',
			link: function (scope, element, attrs, modelCtrl) {
				modelCtrl.$parsers.push(function (inputValue) {
					cleanInputValue = inputValue.replace(/[%]/gi, '');
					modelCtrl.$setViewValue(cleanInputValue);
					modelCtrl.$render();
					return cleanInputValue;
				});

			}
		}
	})





	.directive('forAlphaNumeric', function () {
		return {
			require: 'ngModel',
			restrict: 'A',
			link: function (scope, element, attrs, modelCtrl) {
				modelCtrl.$parsers.push(function (inputValue) {
					if (inputValue == null)
						return ''
					cleanInputValue1 = inputValue.replace(/[^\w\s ]/gi, '');
					cleanInputValue = cleanInputValue1.replace(/[_]/gi, "");
					if (cleanInputValue != inputValue) {
						modelCtrl.$setViewValue(cleanInputValue);
						modelCtrl.$render();
					}
					return cleanInputValue;
				});
			}
		}
	})

	.directive('forName', function () {
		return {
			require: 'ngModel',
			restrict: 'A',
			link: function (scope, element, attrs, modelCtrl) {
				modelCtrl.$parsers.push(function (inputValue) {
					if (inputValue == null)
						return ''
					cleanInputValue1 = inputValue.replace(/[^\w\s .]/gi, '');
					cleanInputValue = cleanInputValue1.replace(/[_]/gi, "");
					if (cleanInputValue != inputValue) {
						modelCtrl.$setViewValue(cleanInputValue);
						modelCtrl.$render();
					}
					return cleanInputValue;
				});
			}
		}
	})

	.directive("forFirGd", function () {
		return {
			require: "ngModel",
			restrict: "A",
			link: function (scope, element, attrs, modelCtrl) {
				modelCtrl.$parsers.push(function (inputValue) {
					if (inputValue == null) return "";
					cleanInputValue1 = inputValue.replace(/[_]/gi, "");
					cleanInputValue = cleanInputValue1.replace(/[^\w\s\-\\ /]/gi, "");
					if (cleanInputValue != inputValue) {
						modelCtrl.$setViewValue(cleanInputValue);
						modelCtrl.$render();
					}
					return cleanInputValue;
				});
			}
		};
	})




	.directive('forEmail', function () {
		return {
			require: 'ngModel',
			restrict: 'A',
			link: function (scope, element, attrs, modelCtrl) {
				modelCtrl.$parsers.push(function (inputValue) {
					if (inputValue == null)
						return ''
					cleanInputValue = inputValue.replace(/[^\w\s@.]/gi, '');
					if (cleanInputValue != inputValue) {
						modelCtrl.$setViewValue(cleanInputValue);
						modelCtrl.$render();
					}
					return cleanInputValue;
				});
				element.bind('keypress', function (event) {
					if (event.keyCode === 32) {
						event.preventDefault();
					}
				});
			}
		}
	})
	.directive('forDigit', function () {
		return {
			require: 'ngModel',
			restrict: 'A',
			link: function (scope, element, attrs, modelCtrl) {
				modelCtrl.$parsers.push(function (inputValue) {
					if (inputValue == null)
						return ''
					cleanInputValue = inputValue.replace(/[^\d]/gi, '');
					if (cleanInputValue != inputValue) {
						modelCtrl.$setViewValue(cleanInputValue);
						modelCtrl.$render();
					}
					return cleanInputValue;
				});
				element.bind('keypress', function (event) {
					if (event.keyCode === 32) {
						event.preventDefault();
					}
				});
			}
		}
	})
	.directive('timePicker', function () {
		return {
			require: 'ngModel',
			restrict: 'A',
			link: function (scope, element, attrs, modelCtrl) {
				modelCtrl.$parsers.push(function (inputValue) {
					if (inputValue == null)
						return ''
					cleanInputValue = inputValue.replace(/[^\d]:/gi, '');
					if (cleanInputValue != inputValue) {
						modelCtrl.$setViewValue(cleanInputValue);
						modelCtrl.$render();
					}
					return cleanInputValue;
				});
				element.bind('keypress', function (event) {
					if (event.keyCode === 32) {
						event.preventDefault();
					}
				});
			}
		}
	})
	.directive('forAddress', function () {
		return {
			require: 'ngModel',
			restrict: 'A',
			link: function (scope, element, attrs, modelCtrl) {
				modelCtrl.$parsers.push(function (inputValue) {
					if (inputValue == null)
						return ''
					cleanInputValue = inputValue.replace(/[^\w\ /`!@#$%^&*()=+?{}\<.>\[\] \\]/gi, '');
					if (cleanInputValue != inputValue) {
						modelCtrl.$setViewValue(cleanInputValue);
						modelCtrl.$render();
					}
					return cleanInputValue;
				});
			}
		}
	})
	.directive('forAlpha', function () {
		return {
			require: 'ngModel',
			restrict: 'A',
			link: function (scope, element, attrs, modelCtrl) {
				modelCtrl.$parsers.push(function (inputValue) {
					if (inputValue == null)
						return ''
					cleanInputValue = inputValue.replace(/[^a-z ]/gi, '');
					if (cleanInputValue != inputValue) {
						modelCtrl.$setViewValue(cleanInputValue);
						modelCtrl.$render();
					}
					return cleanInputValue;
				});
			}
		}
	})

	.directive('forDateCalendar', function () {
		return {
			require: 'ngModel',
			restrict: 'A',
			link: function (scope, element, attrs, modelCtrl) {
				modelCtrl.$parsers.push(function (inputValue) {
					if (inputValue == null)
						return ''
					cleanInputValue = inputValue.replace(/[^0-9-]/gi, '');
					if (cleanInputValue != inputValue) {
						modelCtrl.$setViewValue(cleanInputValue);
						modelCtrl.$render();
					}
					return cleanInputValue;
				});
			}
		}
	})

	.directive("forDecimal", function () {
		return {
			require: "?ngModel",
			link: function (scope, element, attrs, ngModelCtrl) {
				if (!ngModelCtrl) {
					return;
				}

				ngModelCtrl.$parsers.push(function (val) {
					if (angular.isUndefined(val)) {
						var val = "";
					}

					var clean = val.replace(/[^0-9\.]/g, "");
					if (clean[clean.length - 1] != "." && clean != "")
						clean = parseFloat(clean) + "";
					var decimalCheck = clean.split(".");

					if (!angular.isUndefined(decimalCheck[1])) {
						decimalCheck[1] = decimalCheck[1].slice(0, 2);
						clean = decimalCheck[0] + "." + decimalCheck[1];
					}

					if (val !== clean) {
						ngModelCtrl.$setViewValue(clean);
						ngModelCtrl.$render();
					}
					return clean;
				});

				element.bind("keypress", function (event) {
					if (event.keyCode === 32) {
						event.preventDefault();
					}
				});
			}
		};
	})


	.directive('forAlphaNumericCaps', function () {
		return {
			require: 'ngModel',
			restrict: 'A',
			link: function (scope, element, attrs, modelCtrl) {
				modelCtrl.$parsers.push(function (inputValue) {

					curenvalue = inputValue.toUpperCase();
					cleanInputValue1 = curenvalue.replace(/[^\w]/gi, '');
					cleanInputValue = cleanInputValue1.replace(/[_]/gi, "");
					cleanInputValue = cleanInputValue.trim();
					modelCtrl.$setViewValue(cleanInputValue);
					modelCtrl.$render();
					return cleanInputValue;
				});
				element.bind('keypress', function (event) {
					if (event.keyCode === 32) {
						event.preventDefault();
					}
				});
			}
		}
	})
	.directive('forAlphaNumericCapsWithSpace', function () {
		return {
			require: 'ngModel',
			restrict: 'A',
			link: function (scope, element, attrs, modelCtrl) {
				modelCtrl.$parsers.push(function (inputValue) {

					curenvalue = inputValue.toUpperCase();
					cleanInputValue1 = curenvalue.replace(/[^\w ]/gi, '');
					cleanInputValue = cleanInputValue1.replace(/[_]/gi, "");
					modelCtrl.$setViewValue(cleanInputValue);
					modelCtrl.$render();
					return cleanInputValue;
				});

			}
		}
	})
	/*Garage Autocomplete Service*/
	.service('garageAutoCompleteService', function () {

		this.garageService = function ($scope, input) {


			var serviceArgs1 = {

				params: JSON.stringify(input),
				load: function (data) {


					$scope.garageItems = (!!data && !!data.results && !!data.results.items) ? data.results.items : [];
					$scope.$apply();
					return $scope.garageItems;
				},
				error: function (e) {

				}
			};

			$scope.context.options.GarageDetailsAutocompletionService(serviceArgs1);

		}
	})


	/*City of Repair Autocomplete */
	.service('cityAutoCompleteService', function () {

		this.cityService = function ($scope, input) {


			var serviceArgs1 = {

				params: JSON.stringify(input),
				load: function (data) {

					$scope.cityItems = (!!data && !!data.results && !!data.results.items) ? data.results.items : [];
					$scope.$apply();
					return $scope.cityItems;
				},
				error: function (e) {

				}
			};

			$scope.context.options.CityAutocompletionService(serviceArgs1);
		}
	})

	.service('policeStationAutoCompleteService', function () {

		this.policeStationService = function ($scope, input) {


			var serviceArgs1 = {

				params: JSON.stringify(input),
				load: function (data) {
					$scope.policeStationItems = (!!data && !!data.results && !!data.results.items) ? data.results.items : [];
					$scope.$apply();
					return $scope.policeStationItems;
				},
				error: function (e) {

				}
			};
			$scope.context.options.PoliceStationAutoCompletionService(serviceArgs1);
		}
	})

	.service('courtAutoCompleteService', function () {

		this.courtService = function ($scope, input) {


			var serviceArgs1 = {

				params: JSON.stringify(input),
				load: function (data) {

					$scope.courtItems = (!!data && !!data.results && !!data.results.items) ? data.results.items : [];

					$scope.$apply();
					return $scope.courtItems;
				},
				error: function (e) {

				}
			};
			$scope.context.options.CourtAutoCompletionService(serviceArgs1);
		}
	})

	/*Controller*/
	.controller('fnolcontroller', ['$scope', '$element', '$timeout', 'Coach', 'PincodeAutoCompleteService', 'garageAutoCompleteService', 'cityAutoCompleteService', 'policeStationAutoCompleteService', 'courtAutoCompleteService', 'AreaAutoCompleteService', 'CityOnStateAutoCompleteService', 'StateAutoCompleteService', function ($scope, $element, $timeout, Coach, PincodeAutoCompleteService, garageAutoCompleteService, cityAutoCompleteService, policeStationAutoCompleteService, courtAutoCompleteService, AreaAutoCompleteService, CityOnStateAutoCompleteService, StateAutoCompleteService) {
		"use strict";
		angular.extend($scope, new Coach($scope, $element, $timeout));

		var formatDateToDatePicker = function (date) {

			var d = new Date(date),
				month = '' + (d.getMonth() + 1),
				day = '' + d.getDate(),
				year = d.getFullYear();

			if (month.length < 2) month = '0' + month;
			if (day.length < 2) day = '0' + day;

			return [day, month, year].join('-');

		}

		// dd-mm-yyyy to yyyy-mm-dd
		var makeSQLServerComp = function (date) {
			if (!date)
				return "";
			else {
				var pos = date.indexOf("-");
				if (pos == 2) {
					return date.split("-").reverse().join("-");
				} else {
					return date;
				}
			}
		}

		//opposite
		var makePickerComp = function (date) {
			if (!date) {
				return "";
			} else {
				var pos = date.indexOf("-");
				if (pos != 2) {
					return date.split("-").reverse().join("-");
				} else {
					return date;
				}
			}
		}

		$scope.incServiceCallCounter = function () {
			$scope.$evalAsync(function () {
				$scope.serviceCallCount++;
				if ($scope.serviceCallCount > 0) {
					$('#cover').show();
				}
			});
		}

		$scope.decServiceCallCounter = function () {
			$scope.$evalAsync(function () {
				$scope.serviceCallCount--;
				if ($scope.serviceCallCount <= 0) {
					$('#cover').hide();
				}
			});
		}


		function DateDifferenceCalculation(date1, date2) {
			date1 = new Date(date1);
			date2 = new Date(date2);
			var diff = Math.floor(date2.getTime() - date1.getTime());
			var day = 1000 * 60 * 60 * 24;


			var days = Math.floor(diff / day);
			var years = Math.trunc(days / 365);
			var remYearDays = days % 365
			if (remYearDays > 0) {
				var months = Math.trunc(remYearDays / 31);
				var remMonthsDays = days % 31
			}


			var message = "";
			if (years > 1) {
				message += years + " years "
			}
			else {
				message += years + " year "
			}
			message += months + " months "
			message += remMonthsDays + " days "



			$scope.LOSSDETAILS.DATEDIFFERENCE = message;
		}

		/*Intializing Variables*/
		$scope.newClaimSearch = true;
		$scope.featureClaimSearch = false;
		$scope.LOSSDETAILS = {};
		$scope.FEATURESUMMARY = {};
		$scope.CALLERDETAILS = {};
		$scope.CLAIMANTDETAILS = {};
		$scope.fnol = false;
		$scope.callerMobileNumberList = [];
		$scope.callerInsuredEmailList = [];
		$scope.emailIDCommunicationList = [];
		$scope.callerMobileNumberList[0] = '';
		$scope.callerInsuredEmailList[0] = '';
		$scope.emailIDCommunicationList[0] = '';
		$scope.FEATURESUMMARY.REPORTEDDATE = new Date();
		$scope.FEATURESUMMARY.REPORTEDDATE = formatDateToDatePicker($scope.FEATURESUMMARY.REPORTEDDATE);
		$scope.modaltotalPages = 0;
		$scope.WCPA = false;
		$scope.catastropheDetail = {};
		$scope.HOSPITALDETAILS = {};
		$scope.CATASTROPHE = {};
		$scope.FIRDETAILS = {};
		$scope.hospitalSearchParam = 'HOSPITALNAME';
		$scope.FNOLInfo = false;
		$scope.isChecked = [];
		$scope.isCheckedExpense = [];
		for (var i = 0; i < $scope.isChecked.length; i++) {
			$scope.isChecked[i] = false;
			$scope.isCheckedExpense[i] = false;
		}
		$scope.reserve = [];
		$scope.coverList = [];
		/* $scope.callerMobileNumberList = [];
		$scope.callerInsuredEmailList = [];
		$scope.emailIDCommunicationList = []; */


		var d = new Date();
		var h = addZero(d.getHours());
		var m = addZero(d.getMinutes());

		function addZero(i) {
			if (i < 10) {
				i = "0" + i;
			}
			return i;
		}

		$scope.FEATURESUMMARY.REPORTEDTIME = [h, m].join(':');


		$scope.serviceCallCount = 0;
		$('#cover').hide();

		Date.prototype.withoutTime = function () {
			var d = new Date(this);
			d.setHours(0, 0, 0, 0);
			return d;
		};

		//date


		//Old fnol start

		$scope.getGCRealTime = function () {

			if (!!$scope.LOSSDETAILS.DATEOFLOSS) {
				$scope.policyNoModalValue = $scope.FEATURESUMMARY.POLICYNUMBER;
				$scope.lossDateModalValue = $scope.LOSSDETAILS.DATEOFLOSS;
				$scope.lossTimeModalValue = $scope.LOSSDETAILS.LOSSTIME;
			}

			else {
				if ($scope.GCPolicySearchForm.$valid) {
					//$('#PolicyDetailsNotFound').modal('toggle');
				}
				else {
					$scope.showWarningMessage("Enter Policy No., Loss Date and Loss Time !!");
					return;
				}

			}


			var lossDateEndorsement = makeSQLServerComp($scope.lossDateModalValue);
			var lossDateRealTime = $scope.lossDateModalValue;
			var lossTime = !!$scope.lossTimeModalValue ? " " + $scope.lossTimeModalValue : "";



			var input = {

				"lossDate": lossDateEndorsement,
				"policyNumber": $scope.policyNoModalValue,
				"claimFor": $scope.FEATURESUMMARY.CLAIMFOR,
				"gcRealTime": $scope.GCRealTime,
				"lossDateRealTime": lossDateRealTime + lossTime

			}

			var serviceArgs = {

				params: JSON.stringify(input),
				load: function (data) {
					debugger;
					$scope.decServiceCallCounter();

					//$scope.FEATURESUMMARY.RESTRICTPROCESSING = data.restrictProcessing;
					$scope.cancelledEndorsement = data.cancelledEndorsement;
					$scope.ValidPolicy = '';
					$scope.validPolicy = {};
					//$scope.lossTimeCall = 0;

					/* 	if (!!data.endorsementType) {
							$scope.FEATURESUMMARY.ENDORSEMENTTYPE = data.endorsementType;
						}
	
						if (!!data.endorsementName) {
							$scope.FEATURESUMMARY.ENDORSEMENTNAME = data.endorsementName;
						}
	 */

					if (!!data.GCPolicySearchOutput) {

						if (!!data.GCPolicySearchOutput.ErrorText) {
							$scope.GCRealTime = false;
							$scope.policySelected = false;
							$scope.showErrorMessage(data.GCPolicySearchOutput.ErrorText, "Data error from GC. Claim will be generated without policy");

						}
						else {
							if (!!data.GCPolicySearchOutput.GetPolicyCoverResult) {

								if (!!data.GCPolicySearchOutput.GetPolicyCoverResult.ErrorText) {
									$scope.GCRealTime = false;
									$scope.policySelected = false;
									$scope.showErrorMessage(data.GCPolicySearchOutput.ErrorText, "Data error from GC. Claim will be generated without policy");
								}

								else {
									$scope.GCRealTime = true;

									if (data.GCPolicySearchOutput.GetPolicyCoverResult.TabClaimCoverStagingData.items.length > 0) {


										if (!!data.GCPolicySearchOutput.GetPolicyCoverResult.TabClaimCoverStagingData.items[0].ErrorText) {
											$scope.GCRealTime = false;
											$scope.policySelected = false;
											$scope.showErrorMessage(data.GCPolicySearchOutput.GetPolicyCoverResult.TabClaimCoverStagingData.items[0].ErrorText, "Data error from GC. Claim will be generated without policy");
											return;
										}

										else {
											$scope.GCCoverList = data.GCPolicySearchOutput.GetPolicyCoverResult.TabClaimCoverStagingData.items;
											delete $scope.GCCoverList["$$hashKey"];
											delete $scope.GCCoverList["@metadata"];

											$scope.GCAllCovers = Object.assign([], $scope.GCCoverList);


										}


									}

									if (data.GCPolicySearchOutput.GetPolicyCoverResult.GcClaimPolicyDetailsData.items.length > 0) {


										if (!!data.GCPolicySearchOutput.GetPolicyCoverResult.GcClaimPolicyDetailsData.items[0].ErrorText) {
											$scope.GCRealTime = false;
											$scope.policySelected = false;
											$scope.showErrorMessage(e, "Error while fetching from GC");
											$scope.showErrorMessage(data.GCPolicySearchOutput.GetPolicyCoverResult.GcClaimPolicyDetailsData.items[0].ErrorText, "Data error from GC. Claim will be generated without policy");
											return;
										}

										else {
											$scope.policySelected = true;

											$scope.selectedPolicy = data.GCPolicySearchOutput.GetPolicyCoverResult.GcClaimPolicyDetailsData.items[0];
											$scope.selectedPolicy.NUM_REFERENCE_NUMBER = data.ProposalNo;

											delete $scope.selectedPolicy.BPM_FLAG
											delete $scope.selectedPolicy.JOB_ID;
											delete $scope.selectedPolicy.ErrorText;
											delete $scope.selectedPolicy["$$hashKey"];
											delete $scope.selectedPolicy["@metadata"];
											//delete $scope.policyDetails["ErrorText"];

											$scope.FEATURESUMMARY.POLICYNUMBER = $scope.policyNoModalValue;
											$scope.LOSSDETAILS.DATEOFLOSS = $scope.lossDateModalValue;
											$scope.LOSSDETAILS.LOSSTIME = $scope.lossTimeModalValue;

											$scope.next();
										}

									}





								}


							}


						}
					}

					$scope.$apply();
				},
				error: function (error) {
					$scope.decServiceCallCounter();
					$scope.showErrorMessage(error, "Error ");
					$scope.context.log($scope.messages.ajaxServiceError + "selectionService", e);
				}
			};
			$scope.incServiceCallCounter();
			$scope.context.options.GetValidEndorsement(serviceArgs);
		};



		$scope.lossTimeChangeModal = function () {
			$scope.lossTimeModalValue = $(".datetimepickerlosstimeModal").val();

			if ($scope.lossTimeModalValue.indexOf(':') == -1) {
				$scope.lossTimeModalValue = '';
			}
		}


		$scope.addRowCaller = function (x) {

			if (x == 'callerMobile') {
				$scope.callerMobileNumberList[$scope.callerMobileNumberList.length] = '';
			}

			else if (x == 'callerEmail') {
				$scope.callerInsuredEmailList[$scope.callerInsuredEmailList.length] = '';
			}

			else {
				$scope.emailIDCommunicationList[$scope.emailIDCommunicationList.length] = '';
			}
		}

		$scope.deleteRowCaller = function (x) {

			if (x == 'callerMobile') {
				$scope.callerMobileNumberList.pop();
			}

			else if (x == 'callerEmail') {
				$scope.callerInsuredEmailList.pop();
			}

			else {
				$scope.emailIDCommunicationList.pop();
			}
		}

		$scope.getValidPolicy = function () {
			debugger;

			if (!$scope.policySelected && (!$scope.selectedPolicyIntial)) {
				$scope.lossDateChange();
				return;
			}

			if ($scope.LOSSDETAILS.DATEOFLOSS == undefined || $scope.LOSSDETAILS.DATEOFLOSS == '' || $scope.LOSSDETAILS.DATEOFLOSS == null) {
				return;
			}

			var lossDateEndorsement = makeSQLServerComp($scope.LOSSDETAILS.DATEOFLOSS);
			var lossDateRealTime = $scope.LOSSDETAILS.DATEOFLOSS;
			var lossTime = !!$scope.LOSSDETAILS.LOSSTIME ? " " + $scope.LOSSDETAILS.LOSSTIME : "";

			var input = {

				"lossDate": lossDateEndorsement,
				"policyNumber": $scope.selectedPolicyIntial,
				"claimFor": $scope.FEATURESUMMARY.CLAIMFOR,
				"gcRealTime": $scope.GCRealTime,
				"lossDateRealTime": lossDateRealTime + lossTime

			}

			var serviceArgs = {

				params: JSON.stringify(input),
				load: function (data) {
					debugger;
					$scope.decServiceCallCounter();

					//$scope.FEATURESUMMARY.RESTRICTPROCESSING = data.restrictProcessing;
					$scope.cancelledEndorsement = data.cancelledEndorsement;
					$scope.ValidPolicy = '';
					$scope.validPolicy = {};
					$scope.lossTimeCall = 0;

					/* 	if (!!data.endorsementType) {
							$scope.FEATURESUMMARY.ENDORSEMENTTYPE = data.endorsementType;
						}
	
						if (!!data.endorsementName) {
							$scope.FEATURESUMMARY.ENDORSEMENTNAME = data.endorsementName;
						}
	
	 */

					if (!!data.GCPolicySearchOutput.ErrorText) {

						//$scope.GCRealTime = false;
						$scope.policySelected = false;
						$scope.showErrorMessage(data.GCPolicySearchOutput.ErrorText, "Data error from GC. Claim will be generated without policy");
					}

					else {
						if (!!data.GCPolicySearchOutput.GetPolicyCoverResult.ErrorText) {
							//$scope.GCRealTime = false;
							$scope.policySelected = false;
							$scope.showErrorMessage(data.GCPolicySearchOutput.ErrorText, "Data error from GC. Claim will be generated without policy");

						}
						else {

							if (data.GCPolicySearchOutput.GetPolicyCoverResult.TabClaimCoverStagingData.items.length > 0) {


								if (!!data.GCPolicySearchOutput.GetPolicyCoverResult.TabClaimCoverStagingData.items[0].ErrorText) {
									//$scope.GCRealTime = false;
									$scope.policySelected = false;
									$scope.showErrorMessage(data.GCPolicySearchOutput.GetPolicyCoverResult.TabClaimCoverStagingData.items[0].ErrorText, "Data error from GC. Claim will be generated without policy");

								}

								else {
									$scope.GCCoverList = data.GCPolicySearchOutput.GetPolicyCoverResult.TabClaimCoverStagingData.items;

									delete $scope.GCCoverList["$$hashKey"];
									delete $scope.GCCoverList["@metadata"];
									delete $scope.GCCoverList.ErrorText;


									$scope.GCAllCovers = Object.assign([], $scope.GCCoverList);


									//$scope.GCAllCovers = $scope.GCCoverList;


									if (data.GCPolicySearchOutput.GetPolicyCoverResult.GcClaimPolicyDetailsData.items.length > 0) {


										if (!!data.GCPolicySearchOutput.GetPolicyCoverResult.GcClaimPolicyDetailsData.items[0].ErrorText) {
											//$scope.GCRealTime = false;
											$scope.validPolicy = {};
											$scope.ValidPolicy = '';
											$scope.GCCoverList = [];
											$scope.GCAllCovers = [];
											$scope.policySelected = false;
											$scope.showErrorMessage(data.GCPolicySearchOutput.GetPolicyCoverResult.GcClaimPolicyDetailsData.items[0].ErrorText, "Data error from GC. Claim will be generated without policy");
										}

										else {
											$scope.policySelected = true;

											$scope.selectedPolicy = data.GCPolicySearchOutput.GetPolicyCoverResult.GcClaimPolicyDetailsData.items[0];
											$scope.validPolicy = $scope.selectedPolicy;
											$scope.ValidPolicy = $scope.selectedPolicy.TXT_POLICY_NO_CHAR;
											$scope.selectedPolicy.NUM_REFERENCE_NUMBER = data.ProposalNo;

											delete $scope.selectedPolicy.BPM_FLAG
											delete $scope.selectedPolicy.JOB_ID;
											delete $scope.selectedPolicy.ErrorText;
											delete $scope.policyDetails["$$hashKey"];
											delete $scope.policyDetails["@metadata"];

										}

									}
								}


							}


						}
					}




					if ((!!$scope.ValidPolicy)) {
						/*if (!$scope.GCRealTime) {
							$scope.selectedPolicy = data.validPolicy.items[0];
							$scope.validPolicy = data.validPolicy.items[0];
							$scope.ValidPolicy = data.validPolicy.items[0].TXT_POLICY_NO_CHAR;
						}*/

						$scope.expiryDate = '';
						$scope.odExpiry = '';
						$scope.tpExpiry = '';


						if ($scope.selectedPolicy.OD_TERM != undefined && $scope.selectedPolicy.OD_TERM != '' && $scope.selectedPolicy.OD_TERM != null && $scope.selectedPolicy.OD_TERM != '0') {
							$scope.odExpiry = new Date($scope.selectedPolicy.DAT_POLICY_STARTDATE);
							$scope.odExpiry.setFullYear($scope.odExpiry.getFullYear() + parseInt($scope.selectedPolicy.OD_TERM));
							$scope.odExpiry.setDate($scope.odExpiry.getDate() - 1);
						}

						else {
							if ($scope.selectedPolicy.TXT_PRODUCT_NAME == 'MotorTwoWheelerPolicy') {
								if ($scope.selectedPolicy.TXT_POL_TERM != undefined && $scope.selectedPolicy.TXT_POL_TERM != '' && $scope.selectedPolicy.TXT_POL_TERM != null && $scope.selectedPolicy.TXT_POL_TERM != '0') {
									$scope.odExpiry = new Date($scope.selectedPolicy.DAT_POLICY_STARTDATE);
									$scope.odExpiry.setFullYear($scope.odExpiry.getFullYear() + parseInt($scope.selectedPolicy.TXT_POL_TERM));
									$scope.odExpiry.setDate($scope.odExpiry.getDate() - 1);
									$scope.selectedPolicy.OD_TERM = $scope.selectedPolicy.TXT_POL_TERM.toString();

								}
								else {
									if (!!$scope.selectedPolicy.DAT_POLICY_ENDDATE) {
										$scope.odExpiry = new Date($scope.selectedPolicy.DAT_POLICY_ENDDATE);
									}
								}
							}
							else {
								if (!!$scope.selectedPolicy.DAT_POLICY_ENDDATE) {
									$scope.odExpiry = new Date($scope.selectedPolicy.DAT_POLICY_ENDDATE);
								}
							}
						}

						if (($scope.selectedPolicy.OD_EXP_DT == '' || $scope.selectedPolicy.OD_EXP_DT == undefined || $scope.selectedPolicy.OD_EXP_DT == null) && (!!$scope.odExpiry)) {
							$scope.selectedPolicy.OD_EXP_DT = $scope.odExpiry;
							$scope.selectedPolicy.OD_EXP_DT = formatDateToDatePicker($scope.selectedPolicy.OD_EXP_DT);


						}


						if (($scope.selectedPolicy.OD_TERM != undefined && $scope.selectedPolicy.OD_TERM != '0' && $scope.selectedPolicy.OD_TERM != '' && $scope.selectedPolicy.OD_TERM != null)) {
							if ($scope.selectedPolicy.TXT_PRODUCT_NAME == 'MotorTwoWheelerPolicy') {
								$scope.tpExpiry = new Date($scope.selectedPolicy.DAT_POLICY_STARTDATE);
								$scope.tpExpiry.setFullYear($scope.tpExpiry.getFullYear() + 5);
								$scope.tpExpiry.setDate($scope.tpExpiry.getDate() - 1);
								$scope.selectedPolicy.TP_TERM = '5';

							}

							else if ($scope.selectedPolicy.TXT_PRODUCT_NAME == 'PrivateCarInsurancePolicy') {
								$scope.tpExpiry = new Date($scope.selectedPolicy.DAT_POLICY_STARTDATE);
								$scope.tpExpiry.setFullYear($scope.tpExpiry.getFullYear() + 3);
								$scope.tpExpiry.setDate($scope.tpExpiry.getDate() - 1);
								$scope.selectedPolicy.TP_TERM = '3';

							}

							else {
								if ($scope.selectedPolicy.TP_TERM != undefined && $scope.selectedPolicy.TP_TERM != '' && $scope.selectedPolicy.TP_TERM != null && $scope.selectedPolicy.TP_TERM != '0') {
									$scope.tpExpiry = new Date($scope.selectedPolicy.DAT_POLICY_STARTDATE);
									$scope.tpExpiry.setFullYear($scope.tpExpiry.getFullYear() + parseInt($scope.selectedPolicy.TP_TERM));
									$scope.tpExpiry.setDate($scope.tpExpiry.getDate() - 1);
								}

								else {
									$scope.tpExpiry = new Date($scope.selectedPolicy.DAT_POLICY_ENDDATE);
								}
							}

						}


						else {
							if ($scope.selectedPolicy.TP_TERM != undefined && $scope.selectedPolicy.TP_TERM != '' && $scope.selectedPolicy.TP_TERM != null && $scope.selectedPolicy.TP_TERM != '0') {
								$scope.tpExpiry = new Date($scope.selectedPolicy.DAT_POLICY_STARTDATE);
								$scope.tpExpiry.setFullYear($scope.tpExpiry.getFullYear() + parseInt($scope.selectedPolicy.TP_TERM));
								$scope.tpExpiry.setDate($scope.tpExpiry.getDate() - 1);
							}

							else {
								$scope.tpExpiry = new Date($scope.selectedPolicy.DAT_POLICY_ENDDATE);
							}
						}

						if (($scope.selectedPolicy.TP_EXP_DT == '' || $scope.selectedPolicy.TP_EXP_DT == undefined || $scope.selectedPolicy.TP_EXP_DT == null) && !!($scope.tpExpiry)) {
							$scope.selectedPolicy.TP_EXP_DT = $scope.tpExpiry;
							$scope.selectedPolicy.TP_EXP_DT = formatDateToDatePicker($scope.selectedPolicy.TP_EXP_DT);


						}

						/* if ($scope.FEATURESUMMARY.CLAIMFOR == 'Third Party') {
							$scope.expiryDate = $scope.tpExpiry;
						}
						else {
							$scope.expiryDate = $scope.odExpiry;
						} */

						delete $scope.selectedPolicy["$$hashKey"];
						delete $scope.selectedPolicy["@metadata"];
						$scope.selectedPolicy.DAT_POLICY_STARTDATE = new Date($scope.selectedPolicy.DAT_POLICY_STARTDATE);
						$scope.selectedPolicy.DAT_POLICY_ENDDATE = new Date($scope.selectedPolicy.DAT_POLICY_ENDDATE);
						$scope.policyStartDate = formatDateToDatePicker($scope.selectedPolicy.DAT_POLICY_STARTDATE);
						$scope.policyEndDate = formatDateToDatePicker($scope.selectedPolicy.DAT_POLICY_ENDDATE);
						$scope.FEATURESUMMARY.LINEOFBUSINESS = $scope.selectedPolicy.TXT_LOB_NAME;
						$scope.FEATURESUMMARY.PRODUCTNAME = 'AIGCombinedPackagePolicy';
						$scope.FEATURESUMMARY.INSUREDNAME = $scope.selectedPolicy.TXT_INSURED_NAME;
						$scope.FEATURESUMMARY.POLICYNUMBER = $scope.selectedPolicy.TXT_POLICY_NO_CHAR;
						$scope.FEATURESUMMARY.POLICYSTATUS = $scope.selectedPolicy.TXT_POLICY_STATUS;
						$scope.policyNumber = $scope.selectedPolicy.TXT_POLICY_NO_CHAR;
						$scope.certificateNo = $scope.selectedPolicy.TXT_CERT_NUM;

						/* 	if (!!$scope.selectedPolicy.TXT_NCB_CONFIRM) {
								if ($scope.selectedPolicy.TXT_NCB_CONFIRM.toUpperCase() == 'NOT APPLICABLE') {
									$scope.FEATURESUMMARY.NCBRESTRICT = 'false';
									$scope.FEATURESUMMARY.NCB = $scope.selectedPolicy.TXT_NCB_CONFIRM;
									$scope.selectedPolicy.TXT_NCB_CONFIRM = 'NA';
								}
	
	
								if ($scope.selectedPolicy.TXT_NCB_CONFIRM.toUpperCase() == 'NCB CONFIRMATION AWAITED') {
									$scope.FEATURESUMMARY.NCBRESTRICT = 'false';
									$scope.FEATURESUMMARY.NCB = $scope.selectedPolicy.TXT_NCB_CONFIRM;
									$scope.selectedPolicy.TXT_NCB_CONFIRM = 'No';
								}
	
	
	
								if ($scope.selectedPolicy.TXT_NCB_CONFIRM.toUpperCase() == 'NCB CONFIRMED') {
									$scope.FEATURESUMMARY.NCBRESTRICT = 'false';
									$scope.FEATURESUMMARY.NCB = $scope.selectedPolicy.TXT_NCB_CONFIRM;
									$scope.selectedPolicy.TXT_NCB_CONFIRM = 'Yes';
								}
	
	
	
								if ($scope.selectedPolicy.TXT_NCB_CONFIRM.toUpperCase() == 'NCB MIS - DECLARATION') {
									$scope.FEATURESUMMARY.NCB = $scope.selectedPolicy.TXT_NCB_CONFIRM;
									$scope.FEATURESUMMARY.NCBRESTRICT = 'true';
									$scope.selectedPolicy.TXT_NCB_CONFIRM = 'Yes';
								}
	
	
	
								if ($scope.selectedPolicy.TXT_NCB_CONFIRM.toUpperCase() == 'INVALID POLICY') {
									$scope.FEATURESUMMARY.NCBRESTRICT = 'false';
									$scope.FEATURESUMMARY.NCB = $scope.selectedPolicy.TXT_NCB_CONFIRM;
									$scope.selectedPolicy.TXT_NCB_CONFIRM = 'NA';
								}
	
								if ($scope.selectedPolicy.TXT_NCB_CONFIRM.toUpperCase() == 'NCB MISMATCH') {
									$scope.FEATURESUMMARY.NCBRESTRICT = 'true';
									$scope.FEATURESUMMARY.NCB = $scope.selectedPolicy.TXT_NCB_CONFIRM;
									$scope.selectedPolicy.TXT_NCB_CONFIRM = 'Yes';
								}
	
							}
	 */

						$scope.policyStartDate = formatDateToDatePicker($scope.selectedPolicy.DAT_POLICY_STARTDATE);
						$scope.policyEndDate = formatDateToDatePicker($scope.selectedPolicy.DAT_POLICY_ENDDATE);

						if (!!$scope.selectedPolicy.DAT_MAN_CN_DATE)
							$scope.selectedPolicy.DAT_MAN_CN_DATE = new Date($scope.selectedPolicy.DAT_MAN_CN_DATE);
						if (!!$scope.selectedPolicy.DAT_INSERT_DATE)
							$scope.selectedPolicy.DAT_INSERT_DATE = new Date($scope.selectedPolicy.DAT_INSERT_DATE);
						if (!!$scope.selectedPolicy.DAT_ISSUE_DATE)
							$scope.selectedPolicy.DAT_ISSUE_DATE = new Date($scope.selectedPolicy.DAT_ISSUE_DATE);
						if (!!$scope.selectedPolicy.TXT_AUTO_ASS_EXPIRY_DATE)
							$scope.selectedPolicy.TXT_AUTO_ASS_EXPIRY_DATE = new Date($scope.selectedPolicy.TXT_AUTO_ASS_EXPIRY_DATE);
						if (!!$scope.selectedPolicy.DAT_INSTRUMENT_DATE)
							$scope.selectedPolicy.DAT_INSTRUMENT_DATE = new Date($scope.selectedPolicy.DAT_INSTRUMENT_DATE);
						if (!!$scope.selectedPolicy.DAT_RECONCILE_DATE)
							$scope.selectedPolicy.DAT_RECONCILE_DATE = new Date($scope.selectedPolicy.DAT_RECONCILE_DATE);
						if (!!$scope.selectedPolicy.DAT_ACCEPTANCE_DATE)
							$scope.selectedPolicy.DAT_ACCEPTANCE_DATE = new Date($scope.selectedPolicy.DAT_ACCEPTANCE_DATE);
						if (!!$scope.selectedPolicy.DAT_INSPECTION_DT)
							$scope.selectedPolicy.DAT_INSPECTION_DT = new Date($scope.selectedPolicy.DAT_INSPECTION_DT);
						if (!!$scope.selectedPolicy.RR_LIC_START_DT)
							$scope.selectedPolicy.RR_LIC_START_DT = new Date($scope.selectedPolicy.RR_LIC_START_DT);
						if (!!$scope.selectedPolicy.RR_LIC_EXP_DT)
							$scope.selectedPolicy.RR_LIC_EXP_DT = new Date($scope.selectedPolicy.RR_LIC_EXP_DT);
						if (!!$scope.selectedPolicy.RR_TRADEVALFRM)
							$scope.selectedPolicy.RR_TRADEVALFRM = new Date($scope.selectedPolicy.RR_TRADEVALTO);
						if (!!$scope.selectedPolicy.RT_ISSUE_DT)
							$scope.selectedPolicy.RT_ISSUE_DT = new Date($scope.selectedPolicy.RT_ISSUE_DT);
						if (!!$scope.selectedPolicy.RT_TRANSIT_DT)
							$scope.selectedPolicy.RT_TRANSIT_DT = new Date($scope.selectedPolicy.RT_TRANSIT_DT);


						/* 
												$scope.FEATURESUMMARY.ALTERNATEPOLICYNUMBER = $scope.selectedPolicy.ALTERNATE_POLNO;
												$scope.FEATURESUMMARY.POLICYPLAN = $scope.selectedPolicy.POLICYPLAN;
						 */

						$scope.getPolicyStatus();
						$scope.getCoverages($scope.FEATURESUMMARY);
						//$scope.lossDateChange();
					}
					else {



						if ((!!$scope.MappedPolicy)) {

							var mappedPolicyStart = new Date($scope.mappedPolicy.DAT_POLICY_STARTDATE);
							var mappedPolicyEnd = new Date($scope.mappedPolicy.DAT_POLICY_ENDDATE);
							var lossDate = new Date($scope.FEATURESUMMARY.DATEOFLOSS);

							var mappedPolicyLossDateRange = true;
							if (lossDate.withoutTime().getTime() > mappedPolicyEnd.withoutTime().getTime()) {

								$scope.policySelected = false;
								$scope.selectedPolicy = {};
								$scope.FEATURESUMMARY.POLICYNUMBER = '';
								/* 		$scope.FEATURESUMMARY.CBCSTATUS = '';
		
										$scope.FEATURESUMMARY.PREINSPECTION = '';
										$scope.FEATURESUMMARY.NCB = '';
										$scope.FEATURESUMMARY.CHASISNUMBER = '';
										$scope.FEATURESUMMARY.ENGINENUMBER = '';
		
										$scope.FEATURESUMMARY.VEHICLEREGISTRATIONNUMBER = '';
										$scope.FEATURESUMMARY.PRODUCERCODE = '';
										$scope.FEATURESUMMARY.CERTIFICATENUMBER = ''; */
								$scope.TabClaimCoverStagingData = [];
								$scope.GCCoverList = [];
								$scope.getCoverages($scope.FEATURESUMMARY);
								$scope.lossDateChange();
								mappedPolicyLossDateRange = false;
							}
							else if (mappedPolicyStart.withoutTime().getTime() > lossDate.withoutTime().getTime()) {
								//$('#lossDateCheck').modal('toggle');
								$scope.policySelected = false;
								$scope.selectedPolicy = {};
								$scope.FEATURESUMMARY.POLICYNUMBER = '';
								/* $scope.FEATURESUMMARY.CBCSTATUS = '';

								$scope.FEATURESUMMARY.PREINSPECTION = '';
								$scope.FEATURESUMMARY.NCB = '';
								$scope.FEATURESUMMARY.CHASISNUMBER = '';
								$scope.FEATURESUMMARY.ENGINENUMBER = '';

								$scope.FEATURESUMMARY.VEHICLEREGISTRATIONNUMBER = '';
								$scope.FEATURESUMMARY.PRODUCERCODE = '';
								$scope.FEATURESUMMARY.CERTIFICATENUMBER = ''; */
								$scope.TabClaimCoverStagingData = [];
								$scope.GCCoverList = [];
								$scope.getCoverages($scope.FEATURESUMMARY);
								$scope.lossDateChange();
								mappedPolicyLossDateRange = false;
							}

							else {
								$('#ChangeToMappedPolicy').modal('toggle');

							}
						}


						else {
							$scope.ValidPolicy = '';
							$scope.validPolicy = {};
							$scope.policySelected = false;
							$scope.selectedPolicy = {};
							$scope.FEATURESUMMARY.POLICYNUMBER = '';
							//$scope.FEATURESUMMARY.CBCSTATUS = '';
							$scope.FEATURESUMMARY.CLAIMSTATUS = 'Suspended';
							/* $scope.FEATURESUMMARY.PREINSPECTION = '';
							$scope.FEATURESUMMARY.NCB = '';
							$scope.FEATURESUMMARY.CHASISNUMBER = '';
							$scope.FEATURESUMMARY.ENGINENUMBER = '';

							$scope.FEATURESUMMARY.VEHICLEREGISTRATIONNUMBER = '';
							$scope.FEATURESUMMARY.PRODUCERCODE = '';
							$scope.FEATURESUMMARY.CERTIFICATENUMBER = ''; */
							$scope.TabClaimCoverStagingData = [];
							$scope.GCCoverList = [];
							$scope.mappedPolicy = {};
							$scope.MappedPolicy = '';
							$scope.getCoverages($scope.FEATURESUMMARY);
							$scope.lossDateChange();
							//$('#lossDateCheck').modal('toggle');
						}

					}

					$scope.$apply();
				},
				error: function (error) {
					$scope.decServiceCallCounter();
					$scope.showErrorMessage(error, "Error ");
					$scope.ValidPolicy = '';
					$scope.validPolicy = {};
					$scope.policySelected = false;
					$scope.FEATURESUMMARY.CLAIMSTATUS = 'Suspended';
					$scope.selectedPolicy = {};
					$scope.FEATURESUMMARY.POLICYNUMBER = '';
					/* 	$scope.FEATURESUMMARY.CBCSTATUS = '';
	
						$scope.FEATURESUMMARY.PREINSPECTION = '';
						$scope.FEATURESUMMARY.NCB = '';
						$scope.FEATURESUMMARY.CHASISNUMBER = '';
						$scope.FEATURESUMMARY.ENGINENUMBER = '';
	
						$scope.FEATURESUMMARY.VEHICLEREGISTRATIONNUMBER = '';
						$scope.FEATURESUMMARY.PRODUCERCODE = '';
						$scope.FEATURESUMMARY.CERTIFICATENUMBER = ''; */
					$scope.TabClaimCoverStagingData = [];
					$scope.GCCoverList = [];
					$scope.mappedPolicy = {};
					$scope.MappedPolicy = '';
					$scope.coverList = [];
					$scope.coverList1 = [];
					$scope.getCoverages($scope.FEATURESUMMARY);
					$scope.lossDateChange();
					$scope.context.log($scope.messages.ajaxServiceError + "selectionService", e);
				}
			};
			$scope.incServiceCallCounter();
			$scope.context.options.GetValidEndorsement(serviceArgs);
		};

		$scope.getCoverages = function (FeatureSummaryObj) {


			if ($scope.FEATURESUMMARY.CASEYEAR == "" || $scope.FEATURESUMMARY.DATEOFLOSS == null) {
				$scope.FEATURESUMMARY.CASEYEAR = null;
			}
			if (!!FeatureSummaryObj.REPORTEDDATE) {
				FeatureSummaryObj.REPORTEDDATE = makeSQLServerComp($scope.FEATURESUMMARY.REPORTEDDATE);
			}

			if ($scope.FEATURESUMMARY.DATEOFLOSS != "" && $scope.FEATURESUMMARY.DATEOFLOSS != undefined) {
				FeatureSummaryObj.DATEOFLOSS = makeSQLServerComp($scope.FEATURESUMMARY.DATEOFLOSS);
			}
			else {
				FeatureSummaryObj.DATEOFLOSS = undefined;
			}

			if ($scope.policySelected == true) {
				var input = {
					"GetCoveragesFNOLInput": {
						"featureSummary": FeatureSummaryObj,
						"ProposalNo": $scope.selectedPolicy.NUM_REFERENCE_NUMBER
					},
					"GCGetPolicy": $scope.GCRealTime,
					"TabClaimCoverStagingData": $scope.GCCoverList
				}
			}

			else {
				var input = {
					"GetCoveragesFNOLInput": {
						"featureSummary": FeatureSummaryObj,

					},
					"GCGetPolicy": $scope.GCRealTime
				}

			}

			var serviceArgs = {

				params: JSON.stringify(input),
				load: function (data) {
					$scope.decServiceCallCounter();
					var a = 0;
					var b = 0;
					var c = 0;
					$scope.coverList = [];
					$scope.coverLength = data.GetCoveragesFNOLOutput.coverageList.items.length;
					for (var i = 0; i < $scope.coverLength; i++) {
						$scope.isAdded[i] = false;
					}


					if (data.GetCoveragesFNOLOutput.coverageList.items.length > 0) {
						$scope.coverList1 = JSON.parse(JSON.stringify(data.GetCoveragesFNOLOutput.coverageList.items));
						$scope.coverList = data.GetCoveragesFNOLOutput.coverageList.items;

						var uniqueNames = [];
						var uniqueCovercode = [];
						for (i = 0; i < $scope.coverList.length; i++) {
							if (uniqueCovercode.indexOf($scope.coverList[i].COVERCODE) === -1) {
								uniqueNames.push($scope.coverList[i]);
								uniqueCovercode.push($scope.coverList[i].COVERCODE);
							}
						}


						$scope.coverList = uniqueNames;
						$scope.coverList1 = JSON.parse(JSON.stringify($scope.coverList));

						/* var uniqueNames = [];
						var uniqueCovercode = [];
						for (i = 0; i < $scope.coverList1.length; i++) {
							if (uniqueCovercode.indexOf($scope.coverList1[i].COVERCODE) === -1) {
								uniqueNames.push($scope.coverList1[i]);
								uniqueCovercode.push($scope.coverList1[i].COVERCODE);
							}
						}

						$scope.coverList1 = uniqueNames; */

						$scope.coverList = $scope.coverList.filter(function (item) {
							return item.TXT_COVER_DESCRIPTION == "Accidental external means Own Damage"
								|| item.TXT_COVER_DESCRIPTION == "Accident - General, External Means"
								|| item.TXT_COVER_DESCRIPTION == "Theft"
								|| item.TXT_COVER_DESCRIPTION == "Key Replacement"
								|| item.TXT_COVER_DESCRIPTION == "Engine Secure"
								|| item.TXT_COVER_DESCRIPTION == "Depreciation reimbursement"
								|| item.COVER == "Depreciation reimbursement"
								|| item.COVER == "Accidental external means Own Damage"
								|| item.COVER == "Theft"
								|| item.COVER == "Key Replacement"
								|| item.COVER == "Engine Secure";
						})
						$scope.lowerPartCoverList = $scope.coverList1.filter(function (item) {
							return item.TXT_COVER_DESCRIPTION != "Accidental external means Own Damage"
								&& item.TXT_COVER_DESCRIPTION != "Accident - General, External Means"
								&& item.TXT_COVER_DESCRIPTION != "Theft"
								&& item.TXT_COVER_DESCRIPTION != "Key Replacement"
								&& item.TXT_COVER_DESCRIPTION != "Engine Secure"
								&& item.TXT_COVER_DESCRIPTION != "Depreciation reimbursement"
								&& item.COVER != "Accidental external means Own Damage"
								&& item.COVER != "Theft"
								&& item.COVER != "Key Replacement"
								&& item.COVER != "Engine Secure"
								&& item.COVER != "Depreciation reimbursement"
								&& item.COVER != "Death Only"
								&& item.COVER != "PA - Owner - Injury"
								&& item.TXT_COVER_DESCRIPTION != "Death Only"
								&& item.TXT_COVER_DESCRIPTION != "PA - Owner - Injury";
						})

						$scope.middlePartCoverList = $scope.coverList1.filter(function (item) {
							return item.TXT_COVER_DESCRIPTION == "Death Only"
								|| item.TXT_COVER_DESCRIPTION == "PA - Owner - Injury"
								|| item.COVER == "Death Only"
								|| item.COVER == "PA - Owner - Injury"
						})





						//angular.extend($scope.coverList,$scope.coverList,$scope.lowerPartCoverList); 
						$scope.coverList.sort(sortByProperty('TXT_COVER_DESCRIPTION'));
						$scope.lowerPartCoverList.sort(sortByProperty('TXT_COVER_DESCRIPTION'));
						$scope.coverList = $scope.coverList.concat($scope.middlePartCoverList);
						$scope.coverList = $scope.coverList.concat($scope.lowerPartCoverList);
						$scope.coverList1 = JSON.parse(JSON.stringify($scope.coverList));

					}

					for (var i = 0; i < $scope.coverList.length; i++) {
						$scope.isCheckedPA[i] = false;
						$scope.isCheckedExpenseReservePA[i] = false;
					}



					if (data.GetCoveragesFNOLOutput.coverageList.items <= 0) {
						$scope.noFeatures = true;
						$scope.coverList = {};
						$scope.coverList1 = {};
					}

					$scope.FEATURESUMMARY.REPORTEDDATE = makePickerComp($scope.FEATURESUMMARY.REPORTEDDATE);
					if ($scope.FEATURESUMMARY.DATEOFLOSS != "" || $scope.FEATURESUMMARY.DATEOFLOSS != undefined) {
						$scope.FEATURESUMMARY.DATEOFLOSS = makePickerComp($scope.FEATURESUMMARY.DATEOFLOSS);
					}

					if ($scope.featureClaim == true) {
						$scope.getClaimFeatureMappingDetails();
					}

					if (!!$scope.ValidPolicy) {
						if ($scope.feature.length > 0) {
							if ($scope.coverList.length > 0) {
								for (var i = 0; i < $scope.feature.length; i++) {
									for (var j = 0; j < $scope.coverList.length; j++) {
										if ($scope.feature[i].COVERCODE == $scope.coverList[j].COVERCODE) {
											$scope.isCheckedPA[j] = true;


											$scope.coverList[j].INDEMNITYRESERVEAMOUNT = $scope.feature[i].INDEMNITYRESERVEAMOUNT;
											if (!!$scope.feature[i].INDEMNITYRESERVEAMOUNT) {
												$scope.disableIndemnity[j] = true;
											}

											if (parseInt($scope.coverList[j].SUM_INSURED) != 0) {
												$scope.feature[i].INSUREDAMOUNT = $scope.coverList[j].SUM_INSURED;
											}
											else {
												$scope.feature[i].INSUREDAMOUNT = '9999999999';
												$scope.coverList[j].SUM_INSURED = '9999999999';
											}

											break;
										}

										else {
											if (j == ($scope.coverList.length - 1)) {
												$scope.feature.splice(i, 1);

												$scope.isCheckedPA[i] = false;
												$scope.isCheckedExpenseReservePA[i] = false;
												$scope.coverList[i].INDEMNITYRESERVEAMOUNT = "";
												$scope.coverList[i].EXPENSERESERVEAMOUNT = "";
											}
										}
									}
								}
								$scope.featureIndex = $scope.feature.length;
							}
							else {
								$scope.feature = [];
								$scope.isCheckedPA[i] = [];
								$scope.isCheckedExpenseReservePA[i] = [];

							}
						}
					}

					else {
						$scope.feature = [];
						$scope.isCheckedPA[i] = [];
						$scope.isCheckedExpenseReservePA[i] = [];

					}



					$scope.$apply();

					//}
				},
				error: function (error) {
					$scope.decServiceCallCounter();
					$scope.showErrorMessage(e, "Error");
					$scope.context.log($scope.messages.ajaxServiceError + "selectionService", e);
				}
			};
			$scope.incServiceCallCounter();
			$scope.context.options.getCoveragesFNOL(serviceArgs);
		};

		$scope.getPolicyStatus = function () {

			var input = {

				"ProposalNo": $scope.selectedPolicy.NUM_REFERENCE_NUMBER,
				"PolicyStatusCode": $scope.selectedPolicy.TXT_POLICY_STATUS

			}

			var serviceArgs = {

				params: JSON.stringify(input),
				load: function (data) {
					debugger;
					$scope.decServiceCallCounter();
					if (data.GetPolicyStatusOutput.policyStatus.items.length > 0) {
						$scope.selectedPolicy.TXT_POLICY_STATUS = data.GetPolicyStatusOutput.policyStatus.items[0].CLAIMSTATUS;
						$scope.FEATURESUMMARY.POLICYSTATUS = data.GetPolicyStatusOutput.policyStatus.items[0].CLAIMSTATUS;

						if ($scope.selectedPolicy.TXT_POLICY_STATUS == "Cancelled" || $scope.selectedPolicy.TXT_POLICY_STATUS == "cancelled") {
							//$scope.FEATURESUMMARY.CLAIMSTATUS = "Suspended";
							//$scope.suspendedClaim = true;

							$scope.cancelledPolicy = true;
							//$('.reserveLink').bind('click', false);
							//$scope.hideReserveTab = true;
							if (!!$scope.LOSSDETAILS.DATEOFLOSS) {
								$('#cancelledPolicyModal').modal('toggle');
							}
							else {
								//$scope.CheckReinsurance();
							}
							$scope.showWarningMessage('Claim Status is Suspended');
						}

						else {
							//$scope.CheckReinsurance();
						}
					}

					else {
						//$scope.CheckReinsurance();

					}

					$scope.$apply();
				},
				error: function (error) {
					$scope.decServiceCallCounter();
					$scope.showErrorMessage(error, "Error ");
					$scope.context.log($scope.messages.ajaxServiceError + "selectionService", e);
				}
			};
			$scope.incServiceCallCounter();
			$scope.context.options.GetPolicyStatusService(serviceArgs);
		};

		$scope.productNameChange= function(){
			if ($scope.FEATURESUMMARY.PRODUCTNAME == 'WorkmensCompensation') {
				$scope.WCPA = true;
				$scope.HOSPITALDETAILS={};
				$scope.FIRDETAILS={};
				$scope.CLAIMANTDETAILS.INTERNATIONALLOCATION='';
				$scope.CLAIMANTDETAILS.INTERNATIONALLANDMARK = '';
				$scope.CLAIMANTDETAILS.INTERNATIONALCOUNTRY = '';
				$scope.CLAIMANTDETAILS.INTERNATIONALCITY = '';
			}
			$scope.coverList=[];
			$scope.reserve = [];
			$scope.getCoversFNOL();
		}
		$scope.next = function () {
			$scope.FNOLForm.$setUntouched();
			$scope.fnol = true;
			
			$scope.FEATURESUMMARY.LINEOFBUSINESS = 'AIGC';
			$scope.FEATURESUMMARY.CLAIMSTATUS = 'Suspended';
			if (!$scope.featureClaim) {
				$scope.ODParentClaim = false;
				$scope.TPFeatureClaim = false;
			}
			$scope.ODFeature = true;
			$scope.TPFeature = true;
			//$scope.FEATURESUMMARY.CLAIMFOR = "";

			$('#callerIntimatorIdentification').css({
				'display': 'block'
			});

			if ($scope.policySelected == true) {
				if (!$scope.GCRealTime) {
					$scope.selectedPolicy = JSON.parse(JSON.stringify($scope.policyDetails[$scope.indexSelected]));
				}

				$scope.selectedPolicyIntial = '';
				$scope.selectedPolicyIntial = $scope.selectedPolicy.TXT_POLICY_NO_CHAR;

				var lossDate = makeSQLServerComp($scope.LOSSDETAILS.DATEOFLOSS);
				DateDifferenceCalculation($scope.selectedPolicy.DAT_POLICY_STARTDATE, lossDate);




				if ($scope.selectedPolicy.OD_TERM != undefined && $scope.selectedPolicy.OD_TERM != '' && $scope.selectedPolicy.OD_TERM != null && $scope.selectedPolicy.OD_TERM != '0') {
					$scope.odExpiry = new Date($scope.selectedPolicy.DAT_POLICY_STARTDATE);
					$scope.odExpiry.setFullYear($scope.odExpiry.getFullYear() + parseInt($scope.selectedPolicy.OD_TERM));
					$scope.odExpiry.setDate($scope.odExpiry.getDate() - 1);
				}

				else {
					if ($scope.selectedPolicy.TXT_PRODUCT_NAME == 'MotorTwoWheelerPolicy') {
						if ($scope.selectedPolicy.TXT_POL_TERM != undefined && $scope.selectedPolicy.TXT_POL_TERM != '' && $scope.selectedPolicy.TXT_POL_TERM != null && $scope.selectedPolicy.TXT_POL_TERM != '0') {
							$scope.odExpiry = new Date($scope.selectedPolicy.DAT_POLICY_STARTDATE);
							$scope.odExpiry.setFullYear($scope.odExpiry.getFullYear() + parseInt($scope.selectedPolicy.TXT_POL_TERM));
							$scope.odExpiry.setDate($scope.odExpiry.getDate() - 1);
							$scope.selectedPolicy.OD_TERM = $scope.selectedPolicy.TXT_POL_TERM.toString();

						}
					}
					else {
						if (!!$scope.selectedPolicy.DAT_POLICY_ENDDATE) {
							$scope.odExpiry = new Date($scope.selectedPolicy.DAT_POLICY_ENDDATE);
						}
					}
				}

				if (($scope.selectedPolicy.OD_EXP_DT == '' || $scope.selectedPolicy.OD_EXP_DT == undefined || $scope.selectedPolicy.OD_EXP_DT == null) && (!!$scope.odExpiry)) {
					$scope.selectedPolicy.OD_EXP_DT = $scope.odExpiry;
					$scope.selectedPolicy.OD_EXP_DT = formatDateToDatePicker($scope.selectedPolicy.OD_EXP_DT);


				}


				if ($scope.selectedPolicy.PRODUCT_VARIENT != 'StandAloneOD') {
					if (($scope.selectedPolicy.OD_TERM != undefined && $scope.selectedPolicy.OD_TERM != '0' && $scope.selectedPolicy.OD_TERM != '' && $scope.selectedPolicy.OD_TERM != null)) {
						if ($scope.selectedPolicy.TXT_PRODUCT_NAME == 'MotorTwoWheelerPolicy') {
							$scope.tpExpiry = new Date($scope.selectedPolicy.DAT_POLICY_STARTDATE);
							$scope.tpExpiry.setFullYear($scope.tpExpiry.getFullYear() + 5);
							$scope.tpExpiry.setDate($scope.tpExpiry.getDate() - 1);
							$scope.selectedPolicy.TP_TERM = '5';

						}

						else if ($scope.selectedPolicy.TXT_PRODUCT_NAME == 'PrivateCarInsurancePolicy') {
							$scope.tpExpiry = new Date($scope.selectedPolicy.DAT_POLICY_STARTDATE);
							$scope.tpExpiry.setFullYear($scope.tpExpiry.getFullYear() + 3);
							$scope.tpExpiry.setDate($scope.tpExpiry.getDate() - 1);
							$scope.selectedPolicy.TP_TERM = '3';

						}

						else {
							if ($scope.selectedPolicy.TP_TERM != undefined && $scope.selectedPolicy.TP_TERM != '' && $scope.selectedPolicy.TP_TERM != null && $scope.selectedPolicy.TP_TERM != '0') {
								$scope.tpExpiry = new Date($scope.selectedPolicy.DAT_POLICY_STARTDATE);
								$scope.tpExpiry.setFullYear($scope.tpExpiry.getFullYear() + parseInt($scope.selectedPolicy.TP_TERM));
								$scope.tpExpiry.setDate($scope.tpExpiry.getDate() - 1);
							}

							else {
								$scope.tpExpiry = new Date($scope.selectedPolicy.DAT_POLICY_ENDDATE);
							}
						}

					}



					else {
						if ($scope.selectedPolicy.TP_TERM != undefined && $scope.selectedPolicy.TP_TERM != '' && $scope.selectedPolicy.TP_TERM != null && $scope.selectedPolicy.TP_TERM != '0') {
							$scope.tpExpiry = new Date($scope.selectedPolicy.DAT_POLICY_STARTDATE);
							$scope.tpExpiry.setFullYear($scope.tpExpiry.getFullYear() + parseInt($scope.selectedPolicy.TP_TERM));
							$scope.tpExpiry.setDate($scope.tpExpiry.getDate() - 1);
						}

						else {
							$scope.tpExpiry = new Date($scope.selectedPolicy.DAT_POLICY_ENDDATE);
						}
					}
				}

				if (($scope.selectedPolicy.TP_EXP_DT == '' || $scope.selectedPolicy.TP_EXP_DT == undefined || $scope.selectedPolicy.TP_EXP_DT == null) && !!($scope.tpExpiry)) {
					$scope.selectedPolicy.TP_EXP_DT = $scope.tpExpiry;
					$scope.selectedPolicy.TP_EXP_DT = formatDateToDatePicker($scope.selectedPolicy.TP_EXP_DT);


				}

				$scope.ODlossDateRange = true;
				$scope.TPlossDateRange = true;
				if ($scope.policySelected) {
					var today = new Date();
					if ($scope.expiryDate == undefined || $scope.expiryDate == null || $scope.expiryDate == '') {
						var policyEndDate = new Date($scope.selectedPolicy.DAT_POLICY_ENDDATE);
					}
					else {
						var policyEndDate = $scope.expiryDate;
					}
					var policyStartDate = new Date($scope.selectedPolicy.DAT_POLICY_STARTDATE);

					if (today.withoutTime().getTime() > $scope.odExpiry.withoutTime().getTime()) {
						$scope.ODlossDateRange = false;

					}

					else if (policyStartDate.withoutTime().getTime() > today.withoutTime().getTime()) {
						$scope.ODlossDateRange = false;
						//$('#PolicyPeriodCheck').modal('toggle');
					}

					if ($scope.selectedPolicy.PRODUCT_VARIENT != 'StandAloneOD') {

						if (today.withoutTime().getTime() > $scope.tpExpiry.withoutTime().getTime()) {
							$scope.TPlossDateRange = false;
						}

						else if (policyStartDate.withoutTime().getTime() > today.withoutTime().getTime()) {
							$scope.TPlossDateRange = false;
						}
					}
					if (!$scope.ODlossDateRange && !$scope.TPlossDateRange) {
						$("#PolicyPeriodCheck").modal({ backdrop: 'static' }, 'show');
						$scope.customPolicyMessage = "The selected policy is expired as on date.Do you want to still proceed?"
					}

					else if (!$scope.ODlossDateRange) {
						$("#PolicyPeriodCheck").modal({ backdrop: 'static' }, 'show');
						$scope.customPolicyMessage = "Cannot generate OD Claim in open mode as on date. Do you wish to continue?"
					}

					else if (!$scope.TPlossDateRange) {
						$("#PolicyPeriodCheck").modal({ backdrop: 'static' }, 'show');
						$scope.customPolicyMessage = "Cannot generate TP Claim in open mode as on date. Do you wish to continue?"
					}
					else {
						$scope.getPolicyStatus();
					}

				}

				$scope.suspenseReason = false;

				/* 	if ($scope.selectedPolicy.TXT_PRODUCT_NAME == "MotorCommercialVehiclePolicy") {
						$scope.FEATURESUMMARY.VEHICLETYPE = "Commercial Vehicle";
					} else if ($scope.selectedPolicy.TXT_PRODUCT_NAME == "MotorTwoWheelerPolicy") {
						$scope.FEATURESUMMARY.VEHICLETYPE = "Two Wheeler";
					}
					else if ($scope.selectedPolicy.TXT_PRODUCT_NAME == "MotorPrivateCarPolicyInsurance") {
						$scope.FEATURESUMMARY.VEHICLETYPE = "New Private Car";
					}
					else {
						$scope.FEATURESUMMARY.VEHICLETYPE = "Private Car";
					}
					if ($scope.selectedPolicy.PRODUCT_VARIENT == 'TwoWheelerExtendedWarranty') {
						$scope.FEATURESUMMARY.VEHICLETYPE = 'Extended Warranty';
					}
					if ($scope.selectedPolicy.TXT_PRODUCT_NAME == "TradeRoadRiskPolicy" || $scope.selectedPolicy.TXT_PRODUCT_NAME == "MotorRoadTransitPolicy") {
						$scope.certificateNo = $scope.selectedPolicy.TXT_CERT_NUM;
	
						$('#certificateNumber').css({
							'display': 'block'
						});
					} */

				/* if (!!$scope.selectedPolicy.TXT_NCB_CONFIRM) {
					if ($scope.selectedPolicy.TXT_NCB_CONFIRM.toUpperCase() == 'NOT APPLICABLE') {
						$scope.FEATURESUMMARY.NCBRESTRICT = 'false';
						$scope.FEATURESUMMARY.NCB = $scope.selectedPolicy.TXT_NCB_CONFIRM;
						$scope.selectedPolicy.TXT_NCB_CONFIRM = 'NA';
					}


					if ($scope.selectedPolicy.TXT_NCB_CONFIRM.toUpperCase() == 'NCB CONFIRMATION AWAITED') {
						$scope.FEATURESUMMARY.NCBRESTRICT = 'false';
						$scope.FEATURESUMMARY.NCB = $scope.selectedPolicy.TXT_NCB_CONFIRM;
						$scope.selectedPolicy.TXT_NCB_CONFIRM = 'No';
					}



					if ($scope.selectedPolicy.TXT_NCB_CONFIRM.toUpperCase() == 'NCB CONFIRMED') {
						$scope.FEATURESUMMARY.NCBRESTRICT = 'false';
						$scope.FEATURESUMMARY.NCB = $scope.selectedPolicy.TXT_NCB_CONFIRM;
						$scope.selectedPolicy.TXT_NCB_CONFIRM = 'Yes';
					}



					if ($scope.selectedPolicy.TXT_NCB_CONFIRM.toUpperCase() == 'NCB MIS - DECLARATION') {
						$scope.FEATURESUMMARY.NCB = $scope.selectedPolicy.TXT_NCB_CONFIRM;
						$scope.FEATURESUMMARY.NCBRESTRICT = 'true';
						$scope.selectedPolicy.TXT_NCB_CONFIRM = 'Yes';
					}



					if ($scope.selectedPolicy.TXT_NCB_CONFIRM.toUpperCase() == 'INVALID POLICY') {
						$scope.FEATURESUMMARY.NCBRESTRICT = 'false';
						$scope.FEATURESUMMARY.NCB = $scope.selectedPolicy.TXT_NCB_CONFIRM;
						$scope.selectedPolicy.TXT_NCB_CONFIRM = 'NA';
					}

					if ($scope.selectedPolicy.TXT_NCB_CONFIRM.toUpperCase() == 'NCB MISMATCH') {
						$scope.FEATURESUMMARY.NCBRESTRICT = 'true';
						$scope.FEATURESUMMARY.NCB = $scope.selectedPolicy.TXT_NCB_CONFIRM;
						$scope.selectedPolicy.TXT_NCB_CONFIRM = 'Yes';
					}

				} */
				//console.log("Function called");
				delete $scope.selectedPolicy["$$hashKey"];
				delete $scope.selectedPolicy["@metadata"];
				$scope.selectedPolicy.DAT_POLICY_STARTDATE = new Date($scope.selectedPolicy.DAT_POLICY_STARTDATE);
				$scope.selectedPolicy.DAT_POLICY_ENDDATE = new Date($scope.selectedPolicy.DAT_POLICY_ENDDATE);
				$scope.policyStartDate = formatDateToDatePicker($scope.selectedPolicy.DAT_POLICY_STARTDATE);
				$scope.policyEndDate = formatDateToDatePicker($scope.selectedPolicy.DAT_POLICY_ENDDATE);
				$scope.FEATURESUMMARY.LINEOFBUSINESS = $scope.selectedPolicy.TXT_LOB_NAME;
				$scope.FEATURESUMMARY.PRODUCTNAME = 'AIGCombinedPackagePolicy';

				if ($scope.FEATURESUMMARY.PRODUCTNAME == 'WorkmensCompensation'){
					$scope.WCPA=true;
					$scope.HOSPITALDETAILS = {};
					$scope.FIRDETAILS = {};
					$scope.CLAIMANTDETAILS.INTERNATIONALLOCATION = '';
					$scope.CLAIMANTDETAILS.INTERNATIONALLANDMARK = '';
					$scope.CLAIMANTDETAILS.INTERNATIONALCOUNTRY = '';
					$scope.CLAIMANTDETAILS.INTERNATIONALCITY = '';
				}

				$scope.FEATURESUMMARY.INSUREDNAME = $scope.selectedPolicy.TXT_INSURED_NAME;
				$scope.FEATURESUMMARY.POLICYNUMBER = $scope.selectedPolicy.TXT_POLICY_NO_CHAR;
				$scope.FEATURESUMMARY.POLICYSTATUS = $scope.selectedPolicy.TXT_POLICY_STATUS;
				$scope.policyNumber = $scope.selectedPolicy.TXT_POLICY_NO_CHAR;
				$scope.certificateNo = $scope.selectedPolicy.TXT_CERT_NUM;

				if (!!$scope.selectedPolicy.DAT_MAN_CN_DATE)
					$scope.selectedPolicy.DAT_MAN_CN_DATE = new Date($scope.selectedPolicy.DAT_MAN_CN_DATE);
				if (!!$scope.selectedPolicy.DAT_INSERT_DATE)
					$scope.selectedPolicy.DAT_INSERT_DATE = new Date($scope.selectedPolicy.DAT_INSERT_DATE);
				if (!!$scope.selectedPolicy.DAT_ISSUE_DATE)
					$scope.selectedPolicy.DAT_ISSUE_DATE = new Date($scope.selectedPolicy.DAT_ISSUE_DATE);
				if (!!$scope.selectedPolicy.TXT_AUTO_ASS_EXPIRY_DATE)
					$scope.selectedPolicy.TXT_AUTO_ASS_EXPIRY_DATE = new Date($scope.selectedPolicy.TXT_AUTO_ASS_EXPIRY_DATE);
				if (!!$scope.selectedPolicy.DAT_INSTRUMENT_DATE)
					$scope.selectedPolicy.DAT_INSTRUMENT_DATE = new Date($scope.selectedPolicy.DAT_INSTRUMENT_DATE);
				if (!!$scope.selectedPolicy.DAT_RECONCILE_DATE)
					$scope.selectedPolicy.DAT_RECONCILE_DATE = new Date($scope.selectedPolicy.DAT_RECONCILE_DATE);
				if (!!$scope.selectedPolicy.DAT_ACCEPTANCE_DATE)
					$scope.selectedPolicy.DAT_ACCEPTANCE_DATE = new Date($scope.selectedPolicy.DAT_ACCEPTANCE_DATE);
				if (!!$scope.selectedPolicy.DAT_INSPECTION_DT)
					$scope.selectedPolicy.DAT_INSPECTION_DT = new Date($scope.selectedPolicy.DAT_INSPECTION_DT);
				if (!!$scope.selectedPolicy.RR_LIC_START_DT)
					$scope.selectedPolicy.RR_LIC_START_DT = new Date($scope.selectedPolicy.RR_LIC_START_DT);
				if (!!$scope.selectedPolicy.RR_LIC_EXP_DT)
					$scope.selectedPolicy.RR_LIC_EXP_DT = new Date($scope.selectedPolicy.RR_LIC_EXP_DT);
				if (!!$scope.selectedPolicy.RR_TRADEVALFRM)
					$scope.selectedPolicy.RR_TRADEVALFRM = new Date($scope.selectedPolicy.RR_TRADEVALTO);
				if (!!$scope.selectedPolicy.RT_ISSUE_DT)
					$scope.selectedPolicy.RT_ISSUE_DT = new Date($scope.selectedPolicy.RT_ISSUE_DT);
				if (!!$scope.selectedPolicy.RT_TRANSIT_DT)
					$scope.selectedPolicy.RT_TRANSIT_DT = new Date($scope.selectedPolicy.RT_TRANSIT_DT);
		

				var d = new Date();
				d.setSeconds(0, 0);





			} else {
				
				//$scope.hideReserveTab = false;
				$scope.showWarningMessage('Claim Status is Suspended');
			}

			$scope.FNOLInfo = true;
			$scope.getCauseofLossandSections();
			$scope.getCoversFNOL();
			$scope.newClaimSearch = false;


			$('#claimTypeSelection').css({
				'display': 'none'
			});
		}


		//Old fnol ends

		$scope.lossDateChange = function(){
			var lossDate = makeSQLServerComp($scope.LOSSDETAILS.DATEOFLOSS)
			lossDate = new Date(lossDate);
			var today = new Date();

			if($scope.policySelected){
				var lossDate = makeSQLServerComp($scope.LOSSDETAILS.DATEOFLOSS);
				DateDifferenceCalculation($scope.selectedPolicy.DAT_POLICY_STARTDATE, lossDate);
			}


			var timeDiffReported = Math.abs(today.getTime() - lossDate.getTime());
			var diffDaysReported = Math.ceil(timeDiffReported / (1000 * 3600 * 24));
			
			if (diffDaysReported >= 5){
				$("#lossDateModal").modal({
					backdrop: 'static'
				}, 'show');
				$scope.delay = true;
			}
			else{
				$scope.delay =false;
			}
		}


		$scope.getClaim = function (pagStart) {


			if (!(!!$scope.claimSearchText && ($scope.claimSearchText.length == 10))){

				$scope.showWarningMessage('Enter valid Claim Number');
			}
		

			var searchObject = [];

			searchObject[0] = {};
			searchObject[0] = {
				SearchParam: 'CLAIMNUMBER', searchText: $scope.claimSearchText
			}

			if (!!$scope.featureNumberSearch){
				searchObject[1] = {};
				searchObject[1] = {
					SearchParam: 'FEATURENUMBER', searchText: $scope.featureNumberSearch
				}
			}




			var input = {
				"multipleSearchInput": searchObject,
				"tableName": 'AIGCFEATURESUMMARY',
				"startPosition": (pagStart - 1) * 10,
				"orderBy": 'CLAIMNUMBER',
				"noOfRow": 20,
				"COLUMNS": '*',

			}
			var serviceArgs = {
				params: JSON.stringify(input),
				load: function (data) {
					debugger;
					if (!!data.searchResults && data.searchResults.items.length > 0) {
						$scope.claimSummary = data.searchResults.items;
						$scope.isClaimFound = true;
					}
					else {
						$scope.claimSummary = [];
						$scope.isClaimFound = false;
					}
				/* 	if (!!data.RESULTCOUNT) {
						$scope.SPresultCount = data.RESULTCOUNT;
					}
					$scope.assignPagVariables();*/
					$scope.$apply();
					$scope.decServiceCallCounter();
				},
				error: function (e) {
					debugger;
					$scope.decServiceCallCounter();
					$scope.showErrorMessage(e, "Error while fetching Claim Data");
				}
			};
			$scope.incServiceCallCounter();
			$scope.context.options.getMultipleSearch(serviceArgs);
		}


		$scope.editClaim = function (x) {

		
			if(x.CLAIMSTATUS == 'Suspended'){
				$scope.showWarningMessage('Claim is Suspended! Cannot generate feature claim!');
			}

			else{

			$scope.ClaimIndexSelected = $scope.claimSummary.indexOf(x);
			$scope.FEATURESUMMARY.DATEOFLOSS = x.DATEOFLOSS;
			$scope.FEATURESUMMARY.HYPOTHECATION = x.HYPOTHECATION;
			$scope.FEATURESUMMARY.PRODUCTNAME = x.PRODUCTNAME;
			$scope.FEATURESUMMARY.SUBPRODUCT = x.SUBPRODUCT;
			$scope.FEATURESUMMARY.INSUREDNAME = x.INSUREDNAME;
			$scope.FEATURESUMMARY.POLICYNUMBER = x.POLICYNUMBER;
			$scope.FEATURESUMMARY.CLAIMNUMBER = x.CLAIMNUMBER;
			$scope.fnol = true;
			//$scope.policySelected = true;
			$scope.featureClaimSearch = false;
			$scope.featureClaim = true;
			$scope.FNOLInfo = true;
				$('#claimTypeSelection').css({
					'display': 'none'
				});
			$scope.getCauseofLossandSections();
			}

		}


		$scope.typeOfClaimChange = function (claimtype) {

			if (claimtype == 'newClaim') {
				$scope.CLAIMTYPE = 'newClaim';
				$scope.featureClaimSearch = false;
				$scope.featureClaim = false;
				$scope.newClaimSearch = true;

			}
			else {
				$scope.CLAIMTYPE = 'featureClaim';
				$scope.featureClaimSearch = true;
				$scope.featureClaim = true;
				$scope.newClaimSearch = false;
			}
		}

		$scope.causeOfLossChange = function () {
			$scope.LOSSDETAILS.CAUSEOFLOSS = $scope.causeOfLoss.CAUSEOFLOSS;
			$scope.FEATURESUMMARY.SECTION = $scope.causeOfLoss.SECTION;
			$scope.FEATURESUMMARY.LOCATIONSPECIFIC = $scope.causeOfLoss.LOCATIONSPECIFIC;
			$scope.FEATURESUMMARY.SECTIONCODE = $scope.causeOfLoss.SECTIONCODE;
			$scope.FEATURESUMMARY.GCSECTION = $scope.causeOfLoss.GCMAPPING;

			if ($scope.FEATURESUMMARY.SECTION == "Workmen's compensation"){
					$scope.WCPA =true;
				$scope.HOSPITALDETAILS = {};
				$scope.FIRDETAILS = {};
				$scope.CLAIMANTDETAILS.INTERNATIONALLOCATION = '';
				$scope.CLAIMANTDETAILS.INTERNATIONALLANDMARK = '';
				$scope.CLAIMANTDETAILS.INTERNATIONALCOUNTRY = '';
				$scope.CLAIMANTDETAILS.INTERNATIONALCITY = '';

				if($scope.policySelected){
				$scope.coverList=[];
				$scope.reserve = [];
				$scope.getCoversFNOL();
				}
			}
			$scope.hideReserveTab = false;

		}

		$scope.SaveHospital = function (x) {
			$scope.HOSPITALDETAILS.HOSPITALNAME = x.HOSPITALNAME;
			$scope.HOSPITALDETAILS.HOSPITALLOCATION = x.CITY;
			$scope.HOSPITALDETAILS.HOSPITALMASTERID = x.HOSPITALMASTERID;

			$('#hospitalSearch').modal('hide');
		}

		$scope.getHospitalList = function (currentPosition) {

			if ($scope.hospitalSearchParam == undefined || $scope.hospitalSearchParam == '' || $scope.hospitalSearchParam == null) {
				$scope.showWarningMessage('Please select search criteria');
				return;
			}

			var input = {
				"searchText": $scope.hospitalSearch,
				"SearchParam": $scope.hospitalSearchParam,
				"tableName": "HOSPITALMASTER",
				"startPosition": currentPosition - 1,
				"orderBy": "HOSPITALNAME",
				"noOfRow": 7
			}
			var serviceArgs = {
				params: JSON.stringify(input),
				load: function (data) {

					if (data.searchResults.items.length > 0) {
						$scope.hospitalList = data.searchResults.items;
						$scope.modalresultCount = data.RESULTCOUNT;
						$scope.modaltableData = false;
						$scope.assignModalPagVariables();
					} else {
						$scope.modaltotalPages = 0;
						$scope.hospitalList = {};
						$scope.modaltableData = true;
					}
					$scope.decServiceCallCounter();
					$scope.$apply();
				},
				error: function (e) {
					$scope.decServiceCallCounter();
					$scope.showErrorMessage(e, "Error while getting hospital list");
				}
			};
			$scope.incServiceCallCounter();
			$scope.context.options.getSearchData(serviceArgs);

		}


		$scope.areaItems = [];
		$scope.$watch("AreaAutoCompleteService", function () {
			$scope.updateClaimantAreaItems = function (mode) {

				if (mode == 'area') {
					var input = {
						searchValue: $scope.CLAIMANTDETAILS.ADDRESSLINE1,
						searchParameter: 'area'
					}

					AreaAutoCompleteService.areaService($scope, input);
				}
				else if (mode == 'state') {
					var input = {
						searchValue: $scope.CLAIMANTDETAILS.STATE,
						searchParameter: 'state'
					}

					AreaAutoCompleteService.areaService($scope, input);
				}

				else if (mode == 'city') {
					var input = {
						searchValue: $scope.CLAIMANTDETAILS.CITY,
						searchParameter: 'city'
					}

					AreaAutoCompleteService.areaService($scope, input);
				}
				else if (mode == 'pincode') {
					var input = {
						searchValue: $scope.CLAIMANTDETAILS.PINCODE,
						searchParameter: 'pincode'
					}

					AreaAutoCompleteService.areaService($scope, input);
				}
			}
		});

		$scope.updateClaimantPincodeItems = function () {


			var input = {
				pincode: $scope.OwnDamageForm.VDPINCODE2.$viewValue,
				state: $scope.CLAIMANTDETAILS.STATE,
				city: $scope.CLAIMANTDETAILS.CITY
			};

			PincodeAutoCompleteService.pincodeService($scope, input);

			//console.log("Function Called:");
		}

		$scope.updateClaimantCityItems = function () {
			var input = {

				State: $scope.CLAIMANTDETAILS.STATE,
				City: $scope.CLAIMANTDETAILS.CITY,
			};

			CityOnStateAutoCompleteService.cityOnStateService($scope, input);
		}

		$scope.onClaimantAreaSelected = function ($item, $model, $label) {
			var dataarray = new Array();
			dataarray = $item.split("|");
			$scope.CLAIMANTDETAILS.PINCODE = dataarray[0].trim();
			$scope.CLAIMANTDETAILS.ADDRESSLINE1 = dataarray[1].trim();
			$scope.CLAIMANTDETAILS.STATE = dataarray[2].trim();
			$scope.CLAIMANTDETAILS.CITY = dataarray[3].trim();
			$scope.CLAIMANTDETAILS.DISTRICT = $scope.CLAIMANTDETAILS.CITY;
		};

		$scope.onClaimantCitySelected = function ($item, $model, $label) {

			var dataarray = new Array();
			dataarray = $item.split("|");
			$scope.CLAIMANTDETAILS.CITY = dataarray[0].trim();
			$scope.CLAIMANTDETAILS.STATE = dataarray[1].trim();
		};


		$scope.insuredClaimant = function (isInsuredTheClaimant) {



			if (isInsuredTheClaimant == "true") {
				if ($scope.policySelected == true) {
					$scope.CLAIMANTDETAILS.CLAIMANTNAME = $scope.selectedPolicy.TXT_INSURED_NAME;
					if (!!$scope.selectedPolicy.TXT_ADDRESS1) {
						$scope.CLAIMANTDETAILS.ADDRESSLINE1 = $scope.selectedPolicy.TXT_ADDRESS1.replace(/[^\w\ /`!@#$%^&*()=+?{}\<.>\[\] \\]/gi, '');
					}
					if (!!$scope.selectedPolicy.TXT_ADDRESS_2) {
						$scope.CLAIMANTDETAILS.ADDRESSLINE2 = $scope.selectedPolicy.TXT_ADDRESS_2.replace(/[^\w\ /`!@#$%^&*()=+?{}\<.>\[\] \\]/gi, '');
					}
					if (!!$scope.selectedPolicy.TXT_ADDRESS3) {
						$scope.CLAIMANTDETAILS.ADDRESSLINE3 = $scope.selectedPolicy.TXT_ADDRESS3.replace(/[^\w\ /`!@#$%^&*()=+?{}\<.>\[\] \\]/gi, '');
					}
					$scope.CLAIMANTDETAILS.CITY = $scope.selectedPolicy.TXT_CITYDISTRICT;
					$scope.CLAIMANTDETAILS.DISTRICT = $scope.selectedPolicy.TXT_CITYDISTRICT;
					$scope.CLAIMANTDETAILS.STATE = $scope.selectedPolicy.TXT_STATE;
					$scope.CLAIMANTDETAILS.PINCODE = $scope.selectedPolicy.PINCODE;
					$scope.CLAIMANTDETAILS.PINCODE += "";

				}

				else {
					$scope.CLAIMANTDETAILS.CLAIMANTNAME = $scope.FEATURESUMMARY.INSUREDNAME;
				}
			}

			else {
				$scope.CLAIMANTDETAILS.CLAIMANTNAME = "";
				$scope.CLAIMANTDETAILS.ADDRESSLINE1 = "";
				$scope.CLAIMANTDETAILS.ADDRESSLINE2 = "";
				$scope.CLAIMANTDETAILS.ADDRESSLINE3 = "";
				$scope.CLAIMANTDETAILS.CITY = "";
				$scope.CLAIMANTDETAILS.DISTRICT = "";
				$scope.CLAIMANTDETAILS.STATE = "";
				$scope.CLAIMANTDETAILS.PINCODE = "";

			}
		}



		$scope.SelectArea = function (type) {
			$("#SelectArea").modal({
				backdrop: 'static'
			}, 'show');

			$("#SelectArea").on('shown.bs.modal', function () {
				$(this).find('#AreaName').focus();
			});

			$scope.locationList = [];
			$scope.modalsearchText = '';
			$scope.modalSearchParameter = type;
		}

		$scope.modalCurrentPosition = 1;
		$scope.modalresultCount;
		$scope.modaltotalPages;
		var modalpageSize = 7;


		$scope.ZoomBoxSearch = function (modalpageStart, search) {

			$scope.modalCurrentPosition = modalpageStart;

			var input;
			if ($scope.search != "") {
				input = {
					startPosition: (modalpageStart - 1) * 7,
					search: $scope.modalsearchText,
					Location: search
				};
			} else {
				input = {
					startPosition: (modalpageStart - 1) * 7,
					search: $scope.modalsearchText,
					Location: search

				};
			}
			var serviceArgs = {
				params: JSON.stringify(input),
				load: function (data) {
					//$scope.decServiceCallCounter();

					if (data.list.items.length > 0) {
						$scope.modaltableData = false;
						$scope.locationList = data.list.items;
						$scope.modalresultCount = data.resultCount.items[0].COUNT;

						$scope.assignModalPagVariables();
					} else {
						$scope.locationList = new Array();
						$scope.modaltableData = true;
						$scope.modaltotalPages = 0;
					}
					$scope.$apply();
				},
				error: function (e) {
					$scope.decServiceCallCounter();
					$scope.showErrorMessage(e, "Error while getting location List");

				}
			};
			//$scope.incServiceCallCounter();
			$scope.context.options.ZoomBoxSearch(serviceArgs);
		}


		$scope.assignModalPagVariables = function () {

			$scope.modaltotalPages = Math.ceil($scope.modalresultCount / modalpageSize);

			switch ($scope.modaltotalPages) {

				case 1:
					$scope.modalpreviousPage = 0;
					$scope.modalnextPage = 0;
					$scope.modelmiddlePage = 1;
					break;
				case 2:
					if ($scope.modalCurrentPosition == 1) {
						$scope.modalnextPage = 2;
						$scope.modalpreviousPage = 0;

					} else if ($scope.modalCurrentPosition == 2) {
						$scope.modalpreviousPage = 1;
						$scope.modalnextPage = 0;
					}
					$scope.modelmiddlePage = $scope.modalCurrentPosition;
					break;
				default:
					if ($scope.modalCurrentPosition == 1) {

						$scope.modalpreviousPage = 1;
						$scope.modelmiddlePage = $scope.modalCurrentPosition + 1;
						$scope.modalnextPage = $scope.modalCurrentPosition + 2;

					} else if ($scope.modalCurrentPosition == $scope.modaltotalPages) {

						$scope.modalpreviousPage = $scope.modalCurrentPosition - 2;
						$scope.modelmiddlePage = $scope.modalCurrentPosition - 1;
						$scope.modalnextPage = $scope.modalCurrentPosition;

					} else {
						$scope.modalpreviousPage = $scope.modalCurrentPosition - 1;
						$scope.modelmiddlePage = $scope.modalCurrentPosition;
						$scope.modalnextPage = $scope.modalCurrentPosition + 1;
					}

			}

			$scope.$apply();

		}

		$scope.SaveSelectedLocation = function (location) {
			$scope.selectedLocation = location;
		}

		$scope.SaveSelectedLocationdb = function (location) {
			$scope.LOSSDETAILS.AREA = "";

			if (location.LOCALITY_DETAIL1 != undefined && location.LOCALITY_DETAIL1 != null && location.LOCALITY_DETAIL1 != '') {
				$scope.LOSSDETAILS.AREA += location.LOCALITY_DETAIL1;
			}

			if (location.LOCALITY_DETAIL2 != undefined && location.LOCALITY_DETAIL2 != null && location.LOCALITY_DETAIL2 != '') {
				$scope.LOSSDETAILS.AREA += location.LOCALITY_DETAIL2;
			}
			if (location.LOCALITY_DETAIL3 != undefined && location.LOCALITY_DETAIL3 != null && location.LOCALITY_DETAIL3 != '') {
				$scope.LOSSDETAILS.AREA += location.LOCALITY_DETAIL3;
			}
			$scope.LOSSDETAILS.STATE = location.STATENAME;
			$scope.LOSSDETAILS.CITY = location.DISTRICTNAME;
			$scope.LOSSDETAILS.PINCODE = location.PINCODE;

			$('#SelectArea').modal('hide');
		}



		$scope.getCatastropheDescription = function () {

			if ($scope.CATASTROPHE.CATASTROPHETYPE) {
				var catastrophe = $scope.CATASTROPHE.CATASTROPHETYPE;
			}
			else {
				var catastrophe = '';
			}
			if (!!$scope.LOSSDETAILS.DATEOFLOSS) {
				var lossDateCata = makeSQLServerComp($scope.LOSSDETAILS.DATEOFLOSS);
			}
			else {
				var lossDateCata = new Date();
			}
			var input = {
				catastropheType: catastrophe,
				catastropheDate: lossDateCata
			}

			var serviceArgs = {

				params: JSON.stringify(input),
				load: function (data) {
					$scope.decServiceCallCounter();

					if (data.CatastropheDescription.items.length > 0) {
						$scope.catastropheList = data.CatastropheDescription.items;
					}
					else {
						$scope.catastropheList = [];
					}
				},
				error: function (error) {
					$scope.decServiceCallCounter();
					$scope.showErrorMessage(error, "Error while Fetching Records");
				}
			};
			$scope.incServiceCallCounter();
			$scope.context.options.getCatastropheDescription(serviceArgs);
		};

		$scope.getCauseofLossandSections = function () {

			var serviceArgs = {

				//params: JSON.stringify(input),
				load: function (data) {
					$scope.decServiceCallCounter();

					if (!!data.causeOfLossList && !!data.causeOfLossList.items.length > 0) {
						$scope.CauseOfLossList = data.causeOfLossList.items;
					}
					else {
						$scope.showWarningMessage('Cause of Loss not found');
					}


				},
				error: function (error) {
					$scope.decServiceCallCounter();
					$scope.showErrorMessage(error, "Error while Fetching Cause Of Loss");
				}
			};
			$scope.incServiceCallCounter();
			$scope.context.options.getCauseofLossandSections(serviceArgs);
		};


		$scope.getCoversFNOL = function () {

			var input = {
				"productName": $scope.FEATURESUMMARY.PRODUCTNAME
			}

			var serviceArgs = {

				params: JSON.stringify(input),
				load: function (data) {
					$scope.decServiceCallCounter();

					if (!!data.coverList && !!data.coverList.items.length > 0) {
						$scope.coverList = data.coverList.items;
					}
					else {
						$scope.showWarningMessage('Covers not found');
					}


				},
				error: function (error) {
					$scope.decServiceCallCounter();
					$scope.showErrorMessage(error, "Error while Fetching Covers");
				}
			};
			$scope.incServiceCallCounter();
			$scope.context.options.getCoversFNOL(serviceArgs);
		};

		$scope.tableLength = 10;
		$scope.changeTableLength = function () {
			if ($scope.tableLength < $scope.coverList.length) {
				$scope.tableLength = angular.copy($scope.coverList.length);
			} else {
				$scope.tableLength = 10;
			}
		}


		$scope.featureModalToggle = function (x,index) {
			$scope.coverSelected={};
			if($scope.isChecked[index] == true){
			$scope.coverSelected.COVERNAME= x.COVERNAME;
			$scope.coverSelected.COVERCODE=x.COVERCODE;
			$scope.coverSelected.INDEX = index;
			$("#featureModal").modal({backdrop: 'static'}, 'show');
			}
			else{
			
				$scope.coverList[index].INDEMNITYFEATURESTATUS = '';
				$scope.coverList[index].INDEMNITYRESERVEAMOUNT = ''; 
				$scope.coverList[index].EXPENSEFEATURESTATUS = '';
				$scope.coverList[index].EXPENSERESERVEAMOUNT = ''; 
			}
		}

		$scope.ExpenseChanged= function (index){

		if ($scope.isCheckedExpense[index] == false){
		$scope.isCheckedExpense[index] = false;
		$scope.coverList[index].EXPENSEFEATURESTATUS = ''; 
		$scope.coverList[index].EXPENSERESERVEAMOUNT = ''; 
		}
			
		}

		$scope.getFeatureDetail = function(){
			$scope.reserve = [];
			
			for(var i= 0; i< $scope.isChecked.length;i++){
				if (i != $scope.coverSelected.INDEX)
				{
					$scope.isChecked[i]=false;
				}	
				$scope.coverList[i].INDEMNITYRESERVEAMOUNT = '';
				$scope.coverList[i].EXPENSERESERVEAMOUNT='';
				$scope.isCheckedExpense[i] = false;
			}
			$scope.reserve[0] = {};
			$scope.reserve[0].COVERCODE = $scope.coverSelected.COVERCODE;
			$scope.reserve[0].COVERNAME = $scope.coverSelected.COVERNAME;
			$scope.isChecked[$scope.coverSelected.INDEX] = true;
			$scope.reserve[0].INDEMNITYRESERVEAMOUNT = '10000';
			$scope.reserve[0].EXPENSEFEATURESTATUS = '';
			$scope.coverList[$scope.coverSelected.INDEX].INDEMNITYRESERVEAMOUNT = $scope.reserve[0].INDEMNITYRESERVEAMOUNT;
			$scope.reserve[0].INSUREDAMOUNT = '999999999';
			$scope.coverList[$scope.coverSelected.INDEX].SUM_INSURED = $scope.reserve[0].SUMINSURED;
			

		}


		$(".datetimepickerlosstime").on("dp.change", function () {

			$scope.LOSSDETAILS.LOSSTIME = $(".datetimepickerlosstime").val();
	
			var date = new Date();
			if ($scope.LOSSDETAILS.DATEOFLOSS) {
				$scope.lossDateT = $scope.LOSSDETAILS.DATEOFLOSS;
				$scope.lossDateT = makeSQLServerComp($scope.lossDateT);

				var dateofloss = new Date($scope.lossDateT);
				var currentDate = new Date();
				if (dateofloss.withoutTime().getTime() == currentDate.withoutTime().getTime()) {
					var x = moment($scope.LOSSDETAILS.LOSSTIME, "HH:mm");
					var y = moment($scope.FEATURESUMMARY.REPORTEDTIME, "HH:mm")
					x.isBefore(y);

					$scope.FNOLForm.LOSSTIME.$setValidity("Invalid Time", x.isBefore(y));
				} else {
					$scope.FNOLForm.LOSSTIME.$setValidity("Invalid Time", true);
				}
			} else {
				$scope.showWarningMessage("Please Enter Loss Date First");
				$scope.FNOLForm.LOSSTIME.$setValidity("Invalid Time", false);

			}


			debugger;
			$scope.$apply();

		});


		$scope.catastropheCode = function () {
			$scope.CATASTROPHE.CATASTROPHEDESCRIPTION = $scope.catastropheDetail.CATASTROPHEDESCRIPTION;
			$scope.CATASTROPHE.CATASTROPHECODE = $scope.catastropheDetail.CATASTROPHEID;
			$scope.CATASTROPHE.CATASTROPHEMASTERID = $scope.catastropheDetail.CATASTROPHEID;
		}


		$scope.saveFNOL = function () {
			if (!$scope.FNOLForm.$valid) {
				$scope.showWarningMessage('Enter valid mandatory data!');
				$scope.FNOLForm.submitted = true;
				return
			}

			else {

				if (!$scope.reserveForm.$valid  || $scope.reserve.length <= 0){
					$scope.showWarningMessage('Enter valid mandatory data in reserve!');
					$scope.reserveForm.submitted = true;
					return
				}
				else{
				$scope.FNOLForm.submitted = false;
				if (!$scope.policySelected) {
					$scope.FEATURESUMMARY.CLAIMSTATUS = 'Suspended';
				
					$scope.FEATURESUMMARY.SUSPENSEREASON = 'Policy number not available/ not bound';
				}

				else {

					var lossDate = makeSQLServerComp($scope.LOSSDETAILS.DATEOFLOSS);
					lossDate = new Date(lossDate);

					//var policyStartDate = makeSQLServerComp($scope.selectedPolicy.DAT_POLICY_STARTDATE);
					var policyStartDate = $scope.selectedPolicy.DAT_POLICY_STARTDATE;

					//var policyEndDate = makeSQLServerComp($scope.selectedPolicy.DAT_POLICY_ENDDATE);
					var policyEndDate = $scope.selectedPolicy.DAT_POLICY_ENDDATE;


					if (lossDate.withoutTime().getTime() > policyEndDate.withoutTime().getTime()) {
						$scope.FEATURESUMMARY.CLAIMSTATUS = 'Suspended';
						$scope.FEATURESUMMARY.SUSPENSEREASON = 'Date of loss not covered';
					}

					else if (policyStartDate.withoutTime().getTime() > lossDate.withoutTime().getTime()) {
						$scope.FEATURESUMMARY.CLAIMSTATUS = 'Suspended';
						$scope.FEATURESUMMARY.SUSPENSEREASON = 'Date of loss not covered';
					}

					else if ($scope.FEATURESUMMARY.POLICYSTATUS.toUpperCase == 'CANCELLED') {
						$scope.FEATURESUMMARY.CLAIMSTATUS = 'Suspended';
						$scope.FEATURESUMMARY.SUSPENSEREASON = ' Cancelled Policy';
					}

					else{
						$scope.FEATURESUMMARY.CLAIMSTATUS = 'Open';
					}
				}

				$scope.reserve[0].INDEMNITYFEATURESTATUS = $scope.FEATURESUMMARY.CLAIMSTATUS;
				if (!!$scope.reserve[0].EXPENSEFEATURESTATUS) {
					$scope.reserve[0].EXPENSEFEATURESTATUS = $scope.FEATURESUMMARY.CLAIMSTATUS;
				}
				
				$scope.AddFNOLDetails();
			}
		}
			
		}

		$scope.AddFNOLDetails = function () {

			if ($scope.callerMobileNumberList.length > 0) {
				$scope.CALLERDETAILS.INSUREDMOBILENUMBER = $scope.callerMobileNumberList.filter(Boolean).join();
			}

			if ($scope.callerInsuredEmailList.length > 0) {
				$scope.CALLERDETAILS.INSUREDEMAILID = $scope.callerInsuredEmailList.filter(Boolean).join();
			}

			if ($scope.emailIDCommunicationList.length > 0) {
				$scope.CALLERDETAILS.EMAILIDFORCOMMUNICATION = $scope.emailIDCommunicationList.filter(Boolean).join();
			}

			if($scope.WCPA){
				$scope.FEATURESUMMARY.TYPEOFCLAIM = 'WCPA'
			}
			else{
				$scope.FEATURESUMMARY.TYPEOFCLAIM = 'RESTOFAIGC'
			}

			$scope.FEATURESUMMARY.CLAIMANTNAME = $scope.CLAIMANTDETAILS.CLAIMANTNAME;

			$scope.FEATURESUMMARY.DATEOFLOSS = $scope.LOSSDETAILS.DATEOFLOSS;
			$scope.FEATURESUMMARY.REPORTEDDATE = makeSQLServerComp($scope.FEATURESUMMARY.REPORTEDDATE);
			$scope.FEATURESUMMARY.DATEOFLOSS = makeSQLServerComp($scope.FEATURESUMMARY.DATEOFLOSS);
			$scope.LOSSDETAILS.DATEOFLOSS = makeSQLServerComp($scope.LOSSDETAILS.DATEOFLOSS);

			if ($scope.FEATURESUMMARY.FIRDETAIL == 'true' && (!!$scope.FIRDETAILS.FIRDATE)) {
				$scope.FIRDETAILS.FIRDATE = makeSQLServerComp($scope.FIRDETAILS.FIRDATE);
			}

			var input = {
				"AddFNOLInput": {
					"CallerDetails": $scope.CALLERDETAILS,
					"LossDetails": $scope.LOSSDETAILS,
					"ClaimantDetails": $scope.CLAIMANTDETAILS,
					"Catastrophe": $scope.CATASTROPHE,
					"FIRDetails": $scope.FIRDETAILS,
					"HospitalDetails": $scope.HOSPITALDETAILS,
					"FeatureSummary": $scope.FEATURESUMMARY,
					"ClaimFeatureMapping": $scope.reserve
				}
			}

			var serviceArgs = {

				params: JSON.stringify(input),
				load: function (data) {
					$scope.decServiceCallCounter();

					if(!$scope.featureClaim){
					if (!!data.ClaimNumber) {
						
							
								$scope.claimNumber = data.ClaimNumber;

								$("#claimNumberModal").modal({
									backdrop: 'static'
								}, 'show');
							

						
					}
				}

				else{

						if (!!data.FeatureNumber) {
							

								$scope.featureNumber = data.FeatureNumber;

								$("#featureNumberModal").modal({
									backdrop: 'static'
								}, 'show');
							


						}
				}



				},
				error: function (error) {
					$scope.decServiceCallCounter();
					$scope.showErrorMessage(error, "Error while generating Claim!");
				}
			};
			$scope.incServiceCallCounter();
			$scope.context.options.AddFNOLInternal(serviceArgs);
		};

		$scope.back = function(){

			$scope.newClaimSearch = true;
			$scope.featureClaimSearch = false;
			$scope.LOSSDETAILS = {};
			$scope.FEATURESUMMARY = {};
			$scope.CALLERDETAILS = {};
			$scope.CLAIMANTDETAILS = {};
			$scope.fnol = false;
			$scope.callerMobileNumberList = [];
			$scope.callerInsuredEmailList = [];
			$scope.emailIDCommunicationList = [];
			$scope.callerMobileNumberList[0] = '';
			$scope.callerInsuredEmailList[0] = '';
			$scope.emailIDCommunicationList[0] = '';
			$scope.FEATURESUMMARY.REPORTEDDATE = new Date();
			$scope.FEATURESUMMARY.REPORTEDDATE = formatDateToDatePicker($scope.FEATURESUMMARY.REPORTEDDATE);
			$scope.modaltotalPages = 0;
			$scope.WCPA = false;
			$scope.catastropheDetail = {};
			$scope.HOSPITALDETAILS = {};
			$scope.CATASTROPHE = {};
			$scope.FIRDETAILS = {};
			$scope.hospitalSearchParam = 'HOSPITALNAME';
			var d = new Date();
			var h = addZero(d.getHours());
			var m = addZero(d.getMinutes());
			$scope.FNOLInfo = false;
			$scope.FEATURESUMMARY.REPORTEDTIME = [h, m].join(':');
			$scope.reserve = [];
			$scope.coverList = [];
			$scope.lossDateModalValue='';
			$scope.lossTimeModalValue='';
			$scope.policyNoModalValue='';
		

			$scope.isChecked = [];
			$scope.isCheckedExpense = [];

			$('#claimTypeSelection').css({
				'display': 'block'
			});

			for (var i = 0; i < $scope.isChecked.length; i++) {
				$scope.isChecked[i] = false;
				$scope.isCheckedExpense[i] = false;
			}

			if (!$scope.coverList) {
				for (var i = 0; i < $scope.coverList.length; i++) {
					$scope.coverList[i].EXPENSEFEATURESTATUS = '';
					$scope.coverList[i].INDEMNITYFEATURESTATUS = '';
					$scope.coverList[i].EXPENSERESERVEAMOUNT = '';
					$scope.coverList[i].INDEMNITYRESERVEAMOUNT = '';
				}
			}

		}

		$scope.clearFNOL = function(){

			$scope.LOSSDETAILS = {};
			$scope.FEATURESUMMARY = {};
			$scope.CALLERDETAILS = {};
			$scope.CLAIMANTDETAILS = {};
			$scope.callerMobileNumberList = [];
			$scope.callerInsuredEmailList = [];
			$scope.emailIDCommunicationList = [];
			$scope.callerMobileNumberList[0] = '';
			$scope.callerInsuredEmailList[0] = '';
			$scope.emailIDCommunicationList[0] = '';
			$scope.modaltotalPages = 0;
			$scope.WCPA = false;
			$scope.catastropheDetail = {};
			$scope.HOSPITALDETAILS = {};
			$scope.CATASTROPHE = {};
			$scope.FIRDETAILS = {};
			$scope.hospitalSearchParam = 'HOSPITALNAME';
			var d = new Date();
			var h = addZero(d.getHours());
			var m = addZero(d.getMinutes());
			$scope.FEATURESUMMARY.REPORTEDTIME = [h, m].join(':');
			$scope.reserve = [];
			
			if($scope.policySelected){

				$scope.FEATURESUMMARY.ISINSUREDCLAIMANT = '';
				$scope.FEATURESUMMARY.ISEVENTCATASTROPHE= '';
				$scope.FEATURESUMMARY.FIRDETAIL = '';
				$scope.FEATURESUMMARY.SECTION = '';
				$scope.FEATURESUMMARY.LOCATIONSPECIFIC = '';
				$scope.FEATURESUMMARY.CLAIMCATEGORY = '';
				$scope.FEATURESUMMARY.ISEVENTCATASTROPHE = '';
				$scope.FEATURESUMMARY.ISEVENTCATASTROPHE = '';
				$scope.FEATURESUMMARY.CLAIMSTATUS ='Suspended';


			}
			else{
				$scope.FEATURESUMMARY = {};
			}

			$scope.FEATURESUMMARY.REPORTEDDATE = new Date();
			$scope.FEATURESUMMARY.REPORTEDDATE = formatDateToDatePicker($scope.FEATURESUMMARY.REPORTEDDATE);
			$scope.isChecked = [];
			$scope.isCheckedExpense=[];
			for (var i = 0; i < $scope.isChecked.length; i++) {
				$scope.isChecked[i] = false;
				$scope.isCheckedExpense[i] = false;
			}

			if(!$scope.coverList){
			for(var i = 0; i< $scope.coverList.length;i++){
				$scope.coverList[i].EXPENSEFEATURESTATUS ='';
				$scope.coverList[i].INDEMNITYFEATURESTATUS = '';
				$scope.coverList[i].EXPENSERESERVEAMOUNT = '';
				$scope.coverList[i].INDEMNITYRESERVEAMOUNT = '';
			}
		}

		}



		$scope.showSuccessMessage = function (customMessage) {
			$scope.alertModal = true;
			$scope.alertModalError = false;
			$scope.error = false;
			$scope.success = true;
			$scope.errCode = "Success!";
			$scope.errDesc = customMessage;
			$scope.$apply();
			alertTimeoutCallback(4000);
		}

		$scope.showErrorMessage = function (e, customMessage) {

			$scope.alertModal = true;
			$scope.alertModalError = true;
			$scope.error = true;
			//console.log("inside error function");
			$scope.errCode = "Error!";
			$scope.errDesc = customMessage;
			console.log($scope.errDtls);
			$scope.errDtls = JSON.parse(e.responseText).Data.errorMessage;
			$scope.$apply();
			alertTimeoutCallback(12000);
		}

		$scope.showWarningMessage = function (customMessage) {

			$scope.alertModal = true;
			$scope.alertModalError = true;
			$scope.warning = true;
			//console.log("inside warning function");
			$scope.errCode = "Warning!";
			$scope.errDesc = customMessage;
			$scope.alertModalError = false;
			//$scope.$apply();
			alertTimeoutCallback(8000);
			$timeout.cancel();
		}

		$scope.currentPosition = 1;
		$scope.resultCount = 0;
		$scope.totalPages = 0;
		var pageSize = 10;
		$scope.assignPagVariables = function () {

			$scope.totalPages = Math.ceil($scope.resultCount / pageSize);

			switch ($scope.totalPages) {

				case 1:
					$scope.previousPage = 0;
					$scope.nextPage = 0;
					$scope.middlePage = 1;
					break;
				case 2:
					if ($scope.currentPosition == 1) {
						$scope.nextPage = 2;
						$scope.previousPage = 0;

					} else if ($scope.currentPosition == 2) {
						$scope.previousPage = 1;
						$scope.nextPage = 0;
					}
					$scope.middlePage = $scope.currentPosition;
					break;
				default:
					if ($scope.currentPosition == 1) {

						$scope.previousPage = 1;
						$scope.middlePage = $scope.currentPosition + 1;
						$scope.nextPage = $scope.currentPosition + 2;

					} else if ($scope.currentPosition == $scope.totalPages) {

						$scope.previousPage = $scope.currentPosition - 2;
						$scope.middlePage = $scope.currentPosition - 1;
						$scope.nextPage = $scope.currentPosition;

					} else {
						$scope.previousPage = $scope.currentPosition - 1;
						$scope.middlePage = $scope.currentPosition;
						$scope.nextPage = $scope.currentPosition + 1;
					}

			}

			$scope.$apply();

		}

		var start;
		var alertTimeoutCallback = function (time) {

			start = Date.now();

			$timeout(function () {
				var end = Date.now();

				if ((end - start) / 1000 > 4) {

					$('.alert').animate({
						opacity: 0
					}, 500, function () {

						$timeout(function () {

							$scope.alertModal = false;
							$scope.$apply(function () {
								$('.alert').css({
									'opacity': 1
								});
							});
						}, 50);

					});
				}
			}, time);
		}



	}]);




$("#logo").attr('src', getImageFileURL('LOGO'));

function getImageFileURL(filename) {
	return com_ibm_bpm_coach.getManagedAssetUrl('ExternalFiles.zip/images/' + filename + '.png', com_ibm_bpm_coach.assetType_WEB);
}