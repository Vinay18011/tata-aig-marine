angular.module("responsive.coaches")
    .service('PincodeAutoCompleteService', function () {
        this.pincodeService = function ($scope, input) {
            if (input.pincode != null) {
                var serviceArgs1 = {
                    params: JSON.stringify(input),
                    load: function (data) {
                        $scope.$evalAsync(function () {
                            $scope.pincodeItems = (!!data && !!data.results && !!data.results.items) ? data.results.items : [];
                            //remove metadata properties
                            var x, prop;
                            for (x = 0; x < $scope.pincodeItems.length; x++) {
                                for (prop in $scope.pincodeItems[x]) {
                                    if (prop.search("@") === 0) {
                                        delete ($scope.pincodeItems[x][prop]);
                                    }
                                }
                            }
                        });
                        $scope.$apply();
                        return $scope.pincodeItems;
                    },
                    error: function (e) {
                        console.log("service call failed: ", e);
                    }
                };
                $scope.context.options.PincodeSearchService(serviceArgs1);
            }
        }

    })

    .service('AreaAutoCompleteService', function () {

        this.areaService = function ($scope, input) {
            if (input.searchValue != null && input.searchValue.length >= 3) {
                var areaServiceArgs = {
                    params: JSON.stringify(input),
                    load: function (data) {

                        $scope.areaItems = (!!data && !!data.results && !!data.results.items) ? data.results.items : [];
                        $scope.$apply();
                        return $scope.areaItems;
                    },
                    error: function (e) {

                    }
                };
                $scope.context.options.areaAutocompletionService(areaServiceArgs);
            }
        }
    }
    )


    
    .directive('forAlphaNumericSpecial', function () {
        return {
            require: "ngModel",
            restrict: "A",
            link: function (scope, element, attrs, modelCtrl) {
                modelCtrl.$parsers.push(function (inputValue) {
                    if (inputValue == null) return "";
                    cleanInputValue = inputValue.replace(/[^\w\s\- ,/ ]/gi, "");
                    if (cleanInputValue != inputValue) {
                        modelCtrl.$setViewValue(cleanInputValue);
                        modelCtrl.$render();
                    }
                    return cleanInputValue;
                });
            }
        };
    })

    .directive('forNumber', function () {
        return {
            require: 'ngModel',
            restrict: 'A',
            link: function (scope, element, attrs, modelCtrl) {
                element.bind('keypress', function (event) {
                    if (event.which != 8 && event.which != 0 && event.which != 46 && event.which < 48 || event.which > 57) {
                        event.preventDefault();
                    }
                });
            }
        }
    })

    .directive("forAlphaNumeric", function () {
        return {
            require: "ngModel",
            restrict: "A",
            link: function (scope, element, attrs, modelCtrl) {
                modelCtrl.$parsers.push(function (inputValue) {
                    if (inputValue == null) return "";
                    cleanInputValue = inputValue.replace(/[^\w\s ]/gi, "");
                    if (cleanInputValue != inputValue) {
                        modelCtrl.$setViewValue(cleanInputValue);
                        modelCtrl.$render();
                    }
                    return cleanInputValue;
                });
            }
        };
    })
    .directive("forAlphaNumericNoUndsc", function () {
        return {
            require: "ngModel",
            restrict: "A",
            link: function (scope, element, attrs, modelCtrl) {
                modelCtrl.$parsers.push(function (inputValue) {
                    if (inputValue == null) return "";
                    cleanInputValue = inputValue.replace(/[^\w\s ]/gi, "");
                    if (cleanInputValue != inputValue) {
                        modelCtrl.$setViewValue(cleanInputValue);
                        modelCtrl.$render();
                    }
                    return cleanInputValue;
                });
                element.bind("keypress", function (event) {
                    if (event.keyCode === 95) {
                        event.preventDefault();
                    }
                });
            }
        };
    })
    .directive("forEmail", function () {
        return {
            require: "ngModel",
            restrict: "A",
            link: function (scope, element, attrs, modelCtrl) {
                modelCtrl.$parsers.push(function (inputValue) {
                    if (inputValue == null) return "";
                    cleanInputValue = inputValue.replace(/[^\w\s@.]/gi, "");
                    if (cleanInputValue != inputValue) {
                        modelCtrl.$setViewValue(cleanInputValue);
                        modelCtrl.$render();
                    }
                    return cleanInputValue;
                });
                element.bind("keypress", function (event) {
                    if (event.keyCode === 32) {
                        event.preventDefault();
                    }
                });
            }
        };
    })
    .directive("forDigit", function () {
        return {
            require: "ngModel",
            restrict: "A",
            link: function (scope, element, attrs, modelCtrl) {
                modelCtrl.$parsers.push(function (inputValue) {
                    if (inputValue == null) return "";
                    cleanInputValue = inputValue.replace(/[^\d]/gi, "");
                    if (cleanInputValue != inputValue) {
                        modelCtrl.$setViewValue(cleanInputValue);
                        modelCtrl.$render();
                    }
                    return cleanInputValue;
                });
                element.bind("keypress", function (event) {
                    if (event.keyCode === 32) {
                        event.preventDefault();
                    }
                });
            }
        };
    })

    .directive("forAddress", function () {
        return {
            require: "ngModel",
            restrict: "A",
            link: function (scope, element, attrs, modelCtrl) {
                modelCtrl.$parsers.push(function (inputValue) {
                    if (inputValue == null) return "";
                    cleanInputValue = inputValue.replace(/[^\w\s\- ,/ ]/gi, "");
                    if (cleanInputValue != inputValue) {
                        modelCtrl.$setViewValue(cleanInputValue);
                        modelCtrl.$render();
                    }
                    return cleanInputValue;
                });
            }
        };
    })
    .directive("forAlpha", function () {
        return {
            require: "ngModel",
            restrict: "A",
            link: function (scope, element, attrs, modelCtrl) {
                modelCtrl.$parsers.push(function (inputValue) {
                    if (inputValue == null) return "";
                    cleanInputValue = inputValue.replace(/[^a-z ]/gi, "");
                    if (cleanInputValue != inputValue) {
                        modelCtrl.$setViewValue(cleanInputValue);
                        modelCtrl.$render();
                    }
                    return cleanInputValue;
                });
            }
        };
    })
    .directive("forDateCalendar", function () {
        return {
            require: "ngModel",
            restrict: "A",
            link: function (scope, element, attrs, modelCtrl) {
                modelCtrl.$parsers.push(function (inputValue) {
                    if (inputValue == null || inputValue == "") return "";
                    cleanInputValue = inputValue.replace(/[^0-9-]/gi, "");
                    if (cleanInputValue != inputValue) {
                        modelCtrl.$setViewValue(cleanInputValue);
                        modelCtrl.$render();
                    }
                    return cleanInputValue;
                });
            }
        };
    })
    .directive("forPercentage", function () {
        return {
            require: "?ngModel",
            link: function (scope, element, attrs, ngModelCtrl) {
                if (!ngModelCtrl) {
                    return;
                }

                ngModelCtrl.$parsers.push(function (val) {
                    if (angular.isUndefined(val)) {
                        var val = "";
                    }

                    var clean = val.replace(/[^0-9\.]/g, "");
                    if (clean[clean.length - 1] != "." && clean != "")
                        clean = parseFloat(clean) + "";
                    var decimalCheck = clean.split(".");
                    if (!angular.isUndefined(decimalCheck[0])) {
                        decimalCheck[0] = decimalCheck[0].slice(0, 3);
                        if (parseInt(decimalCheck[0]) > 100) {
                            decimalCheck[0] = decimalCheck[0].slice(0, 2);
                        }
                        clean = decimalCheck[0];
                    }

                    if (!angular.isUndefined(decimalCheck[1])) {
                        decimalCheck[1] = decimalCheck[1].slice(0, 2);
                        if (parseInt(decimalCheck[0]) < 100) {
                            clean = decimalCheck[0] + "." + decimalCheck[1];
                        } else if (parseInt(decimalCheck[0]) == 100) {
                            clean = decimalCheck[0];
                        }
                    }

                    if (val !== clean) {
                        ngModelCtrl.$setViewValue(clean);
                        ngModelCtrl.$render();
                    }
                    return clean;
                });

                element.bind("keypress", function (event) {
                    if (event.keyCode === 32) {
                        event.preventDefault();
                    }
                });
            }
        };
    })
    .directive('forAlphaNumericCapsSpace', function () {
        return {
            require: 'ngModel',
            restrict: 'A',
            link: function (scope, element, attrs, modelCtrl) {
                modelCtrl.$parsers.push(function (inputValue) {
                    /*  if (inputValue == null)
                            return ''
                        cleanInputValue = inputValue.replace(/[^\w\s]/gi, '');
                        if (cleanInputValue != inputValue) {
                            modelCtrl.$setViewValue(cleaninputValue);
                            modelCtrl.$render();
                        } */
                    curenvalue = inputValue.toUpperCase();
                    cleanInputValue1 = curenvalue.replace(/[^\w ]/gi, '');
                    cleanInputValue = cleanInputValue1.replace(/[_]/gi, '');
                    /*  cleanInputValue = cleanInputValue.trim(); */
                    modelCtrl.$setViewValue(cleanInputValue);
                    modelCtrl.$render();
                    return cleanInputValue;
                });

            }
        }
    })  

    .directive("forDecimal", function () {
        return {
            require: "?ngModel",
            link: function (scope, element, attrs, ngModelCtrl) {
                if (!ngModelCtrl) {
                    return;
                }

                ngModelCtrl.$parsers.push(function (val) {
                    if (angular.isUndefined(val)) {
                        var val = "";
                    }

                    var clean = val.replace(/[^0-9\.]/g, "");
                    if (clean[clean.length - 1] != "." && clean != "")
                        clean = parseFloat(clean) + "";
                    var decimalCheck = clean.split(".");

                    if (!angular.isUndefined(decimalCheck[1])) {
                        decimalCheck[1] = decimalCheck[1].slice(0, 2);
                        clean = decimalCheck[0] + "." + decimalCheck[1];
                    }

                    if (val !== clean) {
                        ngModelCtrl.$setViewValue(clean);
                        ngModelCtrl.$render();
                    }
                    return clean;
                });

                element.bind("keypress", function (event) {
                    if (event.keyCode === 32) {
                        event.preventDefault();
                    }
                });
            }
        };
    })
    .directive('forDecimalNumber', function () {
        return {
            require: 'ngModel',
            restrict: 'A',
            link: function (scope, element, attrs, modelCtrl) {
                element.bind('keypress', function (event) {
                    if (event.which != 8 && event.which != 0 && event.which != 46 && event.which < 48 || event.which > 57) {
                        event.preventDefault();
                    }
                });
            }
        }
    })
    .directive("forAlphaNumericCaps", function () {
        return {
            require: "ngModel",
            restrict: "A",
            link: function (scope, element, attrs, modelCtrl) {
                modelCtrl.$parsers.push(function (inputValue) {
                    curenvalue = inputValue.toUpperCase();
                    cleanInputValue = curenvalue.replace(/[^\w]/gi, "");
                    cleanInputValue = cleanInputValue.trim();
                    modelCtrl.$setViewValue(cleanInputValue);
                    modelCtrl.$render();
                    return cleanInputValue;
                });
                element.bind("keypress", function (event) {
                    if (event.keyCode === 32) {
                        event.preventDefault();
                    }
                });
            }
        };
    })
    .directive("alphaNumericNospace", function () {
        return {
            require: "ngModel",
            restrict: "A",
            link: function (scope, element, attrs, modelCtrl) {
                modelCtrl.$parsers.push(function (inputValue) {
                    if (inputValue == null) return "";
                    cleanInputValue1 = inputValue.replace(/[^\w]/gi, "");
                    cleanInputValue = cleanInputValue1.replace(/[_]/gi, "");
                    if (cleanInputValue != inputValue) {
                        modelCtrl.$setViewValue(cleanInputValue);
                        modelCtrl.$render();
                    }
                    return cleanInputValue;
                });
                element.bind("keypress", function (event) {
                    if (event.keyCode === 32) {
                        event.preventDefault();
                    }
                });
            }
        };
    })
    .directive("forAlphaNumericWithComma", function () {
        return {
            require: "ngModel",
            restrict: "A",
            link: function (scope, element, attrs, modelCtrl) {
                modelCtrl.$parsers.push(function (inputValue) {
                    if (inputValue == null) return "";
                    cleanInputValue1 = inputValue.replace(/[_]/gi, "");
                    cleanInputValue = cleanInputValue1.replace(/[^\w\s , ]/gi, "");
                    if (cleanInputValue != inputValue) {
                        modelCtrl.$setViewValue(cleanInputValue);
                        modelCtrl.$render();
                    }
                    return cleanInputValue;
                });
            }
        };
    })
    .directive("forAlphaNumericWithAnd", function () {
        return {
            require: "ngModel",
            restrict: "A",
            link: function (scope, element, attrs, modelCtrl) {
                modelCtrl.$parsers.push(function (inputValue) {
                    if (inputValue == null) return "";
                    cleanInputValue1 = inputValue.replace(/[_]/gi, "");
                    cleanInputValue = cleanInputValue1.replace(/[^\w\s &]/gi, "");
                    if (cleanInputValue != inputValue) {
                        modelCtrl.$setViewValue(cleanInputValue);
                        modelCtrl.$render();
                    }
                    return cleanInputValue;
                });
            }
        };
    })
    .controller("AigcController", ["$scope", "$window", "$element", "$document", "$timeout", "Coach", "PincodeAutoCompleteService","AreaAutoCompleteService","$filter",
        function ($scope, $window, $element, $document, $timeout, Coach, PincodeAutoCompleteService,AreaAutoCompleteService, $filter) {
            ("use strict");
            angular.extend($scope, new Coach($scope, $element, $timeout, $window));
            debugger;
            console.log("Controller Called");
            $scope.litigation = {};

            $scope.leftTabs = [];
            $scope.allowedFileTypes = !!$scope.context.options.AllowedFileTypes ? !!$scope.context.options.AllowedFileTypes.boundObject ? $scope.context.options.AllowedFileTypes.boundObject.AllowedFileTypes : "application/zip, application/octet-stream, application/x-zip-compressed, multipart/x-zip,application/vnd.ms-excel,application/pdf,image/jpeg,image/jpg,text/plain,application/excel,application/x-excel,application/x-msexcel,image/pjpeg,image/gif,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.ms-powerpoint,application/vnd.openxmlformats-officedocument.presentationml.presentation" : " application/zip, application/octet-stream, application/x-zip-compressed, multipart/x-zip,application/vnd.ms-excel,application/pdf,image/jpeg,image/jpg,text/plain,application/excel,application/x-excel,application/x-msexcel,image/pjpeg,image/gif,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.ms-powerpoint,application/vnd.openxmlformats-officedocument.presentationml.presentation";
            $scope.getFnolFileURL = function(filename) {
                return com_ibm_bpm_coach.getManagedAssetUrl("FnolFiles.zip/html/" + filename + '.html', com_ibm_bpm_coach.assetType_WEB);
            }
            $scope.getClaimProcessingFileURL = function(filename) {
                return com_ibm_bpm_coach.getManagedAssetUrl("ClaimProcessingFiles.zip/html/" + filename + '.html', com_ibm_bpm_coach.assetType_WEB);
            }
            $scope.getAssessmentFileURL = function(filename) {
                return com_ibm_bpm_coach.getManagedAssetUrl("AssessmentFiles.zip/html/" + filename + '.html', com_ibm_bpm_coach.assetType_WEB);
            }
            $scope.getPaymentFileURL = function(filename) {
                return com_ibm_bpm_coach.getManagedAssetUrl("PaymentFiles.zip/html/" + filename + '.html', com_ibm_bpm_coach.assetType_WEB);
            }
            $scope.getRecoveryFileURL = function(filename) {
                return com_ibm_bpm_coach.getManagedAssetUrl("RecoveryFiles.zip/html/" + filename + '.html', com_ibm_bpm_coach.assetType_WEB);
            }
            $scope.getSummaryFileURL = function(filename) {
                return com_ibm_bpm_coach.getManagedAssetUrl("SummaryFiles.zip/html/" + filename + '.html', com_ibm_bpm_coach.assetType_WEB);
            }
            //  $scope.getImageFileURL = function(filename) {
            //     return com_ibm_bpm_coach.getManagedAssetUrl('ExternalFilesTP.zip/images/' + filename + '.png', com_ibm_bpm_coach.assetType_WEB);
            // }
             

            $scope.ClaimInfo = $scope.context.options.ClaimInfo.boundObject.ClaimInfo;
            $scope.defaultTabIndex = 1;

            $scope.leftTabs = [{
                name: "FNOL",
                id: "fnol",
                isLoaded: false
            },
            {
                name: "Claims Processing",
                id: "claimProcessing",
                isLoaded: false
            },
            {
                name: "Assessment Sheet",
                id: "assessmentSheet",
                isLoaded: false
            },
            {
                name: "Payment",
                id: "payment",
                isLoaded: false
            },
            {
                name: "Communication",
                id: "communication",
                isLoaded: false
            },
            {
                name: "Recovery",
                id: "recovery",
                isLoaded: false
            },
            {
                name: "Summary",
                id: "summary",
                isLoaded: false
            }
            ];
            //fnol tab
            $scope.fnolRegistrationURL  =  $scope.getFnolFileURL('fnol');
            $scope.resAndCovURL   =  $scope.getFnolFileURL('reserve');

            //claim processing tab
            $scope.claimDetailsURL   =  $scope.getClaimProcessingFileURL('claimDetails');
            $scope.surveyorAppointmnetURL   =  $scope.getClaimProcessingFileURL('surveyorAppointment');
            $scope.reportSubmissionURL   =  $scope.getClaimProcessingFileURL('reportSubmission');

            //assessment tab
            $scope.MBDURL =  $scope.getAssessmentFileURL('MBD');
            $scope.BAPURL =  $scope.getAssessmentFileURL('BAP');
            $scope.PNSBURL =  $scope.getAssessmentFileURL('PNSB');
            $scope.EEIURL =  $scope.getAssessmentFileURL('EEI');
            $scope.FGURL =  $scope.getAssessmentFileURL('FG');
            $scope.MTURL =  $scope.getAssessmentFileURL('MT');
            $scope.BURGLARYURL =  $scope.getAssessmentFileURL('Burglary');
            $scope.BAGGAGEURL =  $scope.getAssessmentFileURL('Baggage');
            $scope.BPPURL =  $scope.getAssessmentFileURL('BPP');
            $scope.FLOPURL =  $scope.getAssessmentFileURL('FLOP');
            $scope.PLURL =  $scope.getAssessmentFileURL('PL');
            $scope.FIREURL =  $scope.getAssessmentFileURL('Fire');

            //payment tab
            $scope.claimApprovalURL   =  $scope.getPaymentFileURL('claimApproval');
            $scope.indemnityPaymentURL   = $scope.getPaymentFileURL('indemnityPayment');
            $scope.expensePaymentURL   =  $scope.getPaymentFileURL('expensePayment');

            //reovery tab
            $scope.recoveryURL   =  $scope.getRecoveryFileURL('recovery');
            $scope.litigationURL   =  $scope.getRecoveryFileURL('litigation');

            //summary tab
            $scope.claimSummaryURL   =  $scope.getSummaryFileURL('claimSummary');
            $scope.riSummaryURL   =  $scope.getSummaryFileURL('riSummary');
            $scope.coInsuranceSummaryURL =  $scope.getSummaryFileURL('coInsuranceSummary');
            $scope.claimHistoryURL   =  $scope.getSummaryFileURL('claimHistory');

            $scope.serviceCallCount = 0;
            $scope.incServiceCallCounter = function () {
                $scope.$evalAsync(function () {
                    if($scope.serviceCallCount < 0)
                       $scope.serviceCallCount = 0;
                    $scope.serviceCallCount = $scope.serviceCallCount + 1;
                    console.log($scope.serviceCallCount);
                    if ($scope.serviceCallCount > 0) {
                        $("#cover").show();
                    }
                })
    
            }
            $scope.decServiceCallCounter = function () {
    
                $scope.$evalAsync(function () {
                    $scope.serviceCallCount = $scope.serviceCallCount - 1;
                    console.log($scope.serviceCallCount);
                    if ($scope.serviceCallCount <= 0) {
                        $("#cover").hide();
                    }
                });
                
            }

            $scope.showSuccessMessage = function (customMessage) {
                $scope.alertModal = true;
                $scope.alertModalError = false;
                $scope.error = false;
                $scope.success = true;
                $scope.warning = false;
                $scope.errCode = "Success!!";
                $scope.errDesc = customMessage;
    
                $scope.$apply();
    
                alertTimeoutCallback(5000);
            };
    
            $scope.showErrorMessage = function (e, customMessage) {
    
                $scope.alertModal = true;
                $scope.alertModalError = true;
                $scope.error = true;
                $scope.success = false;
                $scope.warning = false;
                $scope.errCode = "Error!!";
                $scope.errDesc = customMessage;
                $scope.errDtls = JSON.parse(e.responseText).Data.errorMessage;
    
                $scope.$apply();
    
                alertTimeoutCallback(12000);
            };
    
            $scope.showWarningMessage = function (warningmsg) {
                if ($scope.IFSCalertModal == false || $scope.IFSCalertModal == undefined)
                    $scope.alertModal = true;
                $scope.alertModalError = false;
                $scope.error = false;
                $scope.success = false;
                $scope.warning = true;
                $scope.customeError = false;
                $scope.errCode = "Warning!!";
                $scope.errDesc = warningmsg;
    
                // $scope.$apply();
    
                if ($scope.errDesc == "Upload Files Contains Harmful Files !!")
                    $scope.$apply();
    
                alertTimeoutCallback(5000);
    
            };


            var start;
            var alertTimeoutCallback = function (time) {

                start = Date.now();

                $timeout(function () {
                    var end = Date.now();

                    if ((end - start) / 1000 > 4) {

                        $('.alert').animate({
                            opacity: 0
                        }, 500, function () {

                            $timeout(function () {

                                $scope.alertModal = false;
                                $scope.$apply(function () {
                                    $('.alert').css({
                                        'opacity': 1
                                    });
                                });
                            }, 50);

                        });
                    }
                }, time);
            }



            $scope.OpenClaim = function(){
                
                var input = {
                    "ClaimInfo" : $scope.ClaimInfo
                };

                var serviceArgs = {
                    params: JSON.stringify(input),
                    load: function (data) {
                        debugger;
                        $scope.decServiceCallCounter();
                        $scope.claimSummary = data.claimSummary.items.map(d => d.indexedMap)[0];
                        $scope.claimSummaryFNOL = angular.copy($scope.claimSummary);
                        $scope.claimSummaryFNOL.REPORTEDDATE = formatDateToDatePicker($scope.claimSummaryFNOL.REPORTEDDATE);
                        $scope.STATEGSTNUMBER = data.tataAigGstNumber.items.map(d => d.indexedMap);
                        $scope.TAXRATE = data.taxrate.items.map(d => d.indexedMap);
                        switch($scope.ClaimInfo.TASKNAME){
                            case 'CP' : 
                                $scope.openTab('claimProcessing',1);
                                break;
                            default:
                                $scope.openTab('claimProcessing',1);
                                break;
                        }
                       
                    },
                    error: function (e) {
                        $scope.decServiceCallCounter();
                    }
                };
                $scope.incServiceCallCounter();
                $scope.context.options.OpenClaim(serviceArgs);
            }
            
            $scope.openTab = function (tabName, index) {
                $scope.defaultTabIndex = index;
                $("#sidebar").toggleClass("inactive");
                $("#content").toggleClass("shift-left");
                $scope.GetTabData(tabName);
            };

            

            $scope.GetTabData = function(tabName){
                var input = {
                    "tabName" : tabName,
                    "ClaimInfo" : $scope.ClaimInfo,
                    "SECTIONCODE" : $scope.claimSummary.SECTIONCODE
                };

                var serviceArgs = {
                    params: JSON.stringify(input),
                    load: function (data) {
                        $scope.decServiceCallCounter();
                        switch(tabName){
                            case 'fnol':
                                $scope.CALLERDETAILS = data.TABDATA.items[1].rows.items[0].indexedMap;

                                if (!!$scope.CALLERDETAILS.INSUREDMOBILENUMBER){
                                    $scope.callerMobileNumberList = [];
                                    $scope.callerMobileNumberList = $scope.CALLERDETAILS.INSUREDMOBILENUMBER.split(',');
                                }

                                if (!!$scope.CALLERDETAILS.INSUREDEMAILID){
                                    $scope.callerInsuredEmailList = [];
                                    $scope.callerInsuredEmailList = $scope.CALLERDETAILS.INSUREDEMAILID.split(',');
                                }

                                if (!!$scope.CALLERDETAILS.EMAILIDFORCOMMUNICATION){
                                    $scope.emailIDCommunicationList = [];
                                    $scope.emailIDCommunicationList = $scope.CALLERDETAILS.EMAILIDFORCOMMUNICATION.split(',');
                                }
                               
                                $scope.LOSSDETAILS = data.TABDATA.items[2].rows.items[0].indexedMap;

                                $scope.LOSSDETAILS.DATEOFLOSS = formatDateToDatePicker($scope.LOSSDETAILS.DATEOFLOSS);
                                
                                $scope.CLAIMANTDETAILS = data.TABDATA.items[3].rows.items[0].indexedMap;
                                if (data.TABDATA.items[4].rows.items.length > 0){
                                    $scope.CATASTROPHE = data.TABDATA.items[4].rows.items[0].indexedMap;
                                }
                                else{
                                    $scope.CATASTROPHE = {};
                                    $scope.catastropheDetail={};
                                }
                                if (data.TABDATA.items[5].rows.items.length > 0) {
                                    $scope.FIRDETAILS = data.TABDATA.items[5].rows.items[0].indexedMap;
                                }
                                else {
                                    $scope.FIRDETAILS = {};
                                }
                                if (data.TABDATA.items[6].rows.items.length > 0) {
                                    $scope.HOSPITALDETAILS = data.TABDATA.items[6].rows.items[0].indexedMap;
                                }
                                else {
                                    $scope.HOSPITALDETAILS = {};
                                }
                                if(data.TABDATA.items[7].rows.items.length > 0){
                                    $scope.reserveData = data.TABDATA.items[7].rows.items[0].indexedMap;
                                }

                                if (data.TABDATA.items[8].rows.items.length > 0) {
                                    $scope.CauseOfLossList = data.TABDATA.items[8].rows.items.map(d => d.indexedMap);
                                }

                                if($scope.claimSummary.ISEVENTCATASTROPHE == 'true'){
                                    if (data.TABDATA.items[9].rows.items.length > 0) {
                                        $scope.catastropheList = data.TABDATA.items[9].rows.items.map(d => d.indexedMap);
                                    }


                                    for (var i = 0; i < $scope.catastropheList.length; i++) {
                                        if ($scope.catastropheList[i].CATASTROPHEDESCRIPTION == $scope.CATASTROPHE.CATASTROPHEDESCRIPTION) {
                                            $scope.catastropheDetail = $scope.catastropheList[i];
                                            break;
                                        }

                                    }
                                }
                                //$scope.causeOfLoss ={};

                                for (var i = 0; i < $scope.CauseOfLossList.length;i++){
                                    if($scope.CauseOfLossList[i].CAUSEOFLOSS == $scope.LOSSDETAILS.CAUSEOFLOSS){
                                        $scope.causeOfLoss = $scope.CauseOfLossList[i];
                                        break;
                                    }
                                
                                }
                                $scope.$apply();
                                

                                break;
                            case 'assessmentSheet' : 
                                    $scope.openAssessment(data,$scope.claimSummary.SECTIONCODE);
                                break;
                            case 'reserve' :
                                if(data.TABDATA.items[0].rows.items.length > 0){
                                    $scope.reserveData = data.TABDATA.items[0].rows.items[0].indexedMap;
                                }
                                break;
                            case 'claimProcessing':
                                $scope.displayClaimDetails(data,$scope.claimSummary.SECTIONCODE);
                                break;
                            default:
                                break;
                        }
                       
                       
                    },
                    error: function (e) {
                        $scope.decServiceCallCounter();
                    }
                };
                $scope.incServiceCallCounter();
                $scope.context.options.getTabData(serviceArgs);  
            }

            /* FNOL */

            $scope.causeOfLossChange = function (x) {
                if(!!x){
                $scope.LOSSDETAILS.CAUSEOFLOSS = x.CAUSEOFLOSS;
                $scope.claimSummaryFNOL.SECTION = x.SECTION;
                $scope.claimSummaryFNOL.LOCATIONSPECIFIC = x.LOCATIONSPECIFIC;
                $scope.claimSummaryFNOL.SECTIONCODE = x.SECTIONCODE;
                $scope.claimSummaryFNOL.GCSECTION = x.GCMAPPING;

                if ($scope.claimSummaryFNOL.SECTION == "Workmen's compensation") {
                    $scope.claimSummaryFNOL.TYPEOFCLAIM = 'WCPA';
                    $scope.HOSPITALDETAILS = {};
                    $scope.FIRDETAILS = {};
                    $scope.CLAIMANTDETAILS.INTERNATIONALLOCATION = '';
                    $scope.CLAIMANTDETAILS.INTERNATIONALLANDMARK = '';
                    $scope.CLAIMANTDETAILS.INTERNATIONALCOUNTRY = '';
                    $scope.CLAIMANTDETAILS.INTERNATIONALCITY = '';

                    if ($scope.policySelected) {
                        $scope.coverList = [];
                        $scope.reserve = [];
                        //$scope.getCoversFNOL();
                    }
                }
            }
               

            }

       


		$scope.SelectArea = function (type) {
            $("#SelectArea").modal({
                backdrop: 'static'
            }, 'show');

            $("#SelectArea").on('shown.bs.modal', function () {
                $(this).find('#AreaName').focus();
            });

            $scope.locationList = [];
            $scope.modalsearchText = '';
            $scope.modalSearchParameter = type;
        }

                    $scope.modalCurrentPosition = 1;
            $scope.modalresultCount;
            $scope.modaltotalPages;
            var modalpageSize = 7;


            $scope.ZoomBoxSearch = function (modalpageStart, search) {

                $scope.modalCurrentPosition = modalpageStart;

                var input;
                if ($scope.search != "") {
                    input = {
                        startPosition: (modalpageStart - 1) * 7,
                        search: $scope.modalsearchText,
                        Location: search
                    };
                } else {
                    input = {
                        startPosition: (modalpageStart - 1) * 7,
                        search: $scope.modalsearchText,
                        Location: search

                    };
                }
                var serviceArgs = {
                    params: JSON.stringify(input),
                    load: function (data) {
                        //$scope.decServiceCallCounter();

                        if (data.list.items.length > 0) {
                            $scope.modaltableData = false;
                            $scope.locationList = data.list.items;
                            $scope.modalresultCount = data.resultCount.items[0].COUNT;

                            $scope.assignModalPagVariables();
                        } else {
                            $scope.locationList = new Array();
                            $scope.modaltableData = true;
                            $scope.modaltotalPages = 0;
                        }
                        $scope.$apply();
                    },
                    error: function (e) {
                        $scope.decServiceCallCounter();
                        $scope.showErrorMessage(e, "Error while getting location List");

                    }
                };
                //$scope.incServiceCallCounter();
                $scope.context.options.ZoomBoxSearch(serviceArgs);
            }


            $scope.assignModalPagVariables = function () {

                $scope.modaltotalPages = Math.ceil($scope.modalresultCount / modalpageSize);

                switch ($scope.modaltotalPages) {

                    case 1:
                        $scope.modalpreviousPage = 0;
                        $scope.modalnextPage = 0;
                        $scope.modelmiddlePage = 1;
                        break;
                    case 2:
                        if ($scope.modalCurrentPosition == 1) {
                            $scope.modalnextPage = 2;
                            $scope.modalpreviousPage = 0;

                        } else if ($scope.modalCurrentPosition == 2) {
                            $scope.modalpreviousPage = 1;
                            $scope.modalnextPage = 0;
                        }
                        $scope.modelmiddlePage = $scope.modalCurrentPosition;
                        break;
                    default:
                        if ($scope.modalCurrentPosition == 1) {

                            $scope.modalpreviousPage = 1;
                            $scope.modelmiddlePage = $scope.modalCurrentPosition + 1;
                            $scope.modalnextPage = $scope.modalCurrentPosition + 2;

                        } else if ($scope.modalCurrentPosition == $scope.modaltotalPages) {

                            $scope.modalpreviousPage = $scope.modalCurrentPosition - 2;
                            $scope.modelmiddlePage = $scope.modalCurrentPosition - 1;
                            $scope.modalnextPage = $scope.modalCurrentPosition;

                        } else {
                            $scope.modalpreviousPage = $scope.modalCurrentPosition - 1;
                            $scope.modelmiddlePage = $scope.modalCurrentPosition;
                            $scope.modalnextPage = $scope.modalCurrentPosition + 1;
                        }

                }

                $scope.$apply();

            }

            $scope.SaveSelectedLocation = function (location) {
                $scope.selectedLocation = location;
            }

            $scope.SaveSelectedLocationdb = function (location) {
                $scope.LOSSDETAILS.AREA = "";

                if (location.LOCALITY_DETAIL1 != undefined && location.LOCALITY_DETAIL1 != null && location.LOCALITY_DETAIL1 != '') {
                    $scope.LOSSDETAILS.AREA += location.LOCALITY_DETAIL1;
                }

                if (location.LOCALITY_DETAIL2 != undefined && location.LOCALITY_DETAIL2 != null && location.LOCALITY_DETAIL2 != '') {
                    $scope.LOSSDETAILS.AREA += location.LOCALITY_DETAIL2;
                }
                if (location.LOCALITY_DETAIL3 != undefined && location.LOCALITY_DETAIL3 != null && location.LOCALITY_DETAIL3 != '') {
                    $scope.LOSSDETAILS.AREA += location.LOCALITY_DETAIL3;
                }
                $scope.LOSSDETAILS.STATE = location.STATENAME;
                $scope.LOSSDETAILS.CITY = location.DISTRICTNAME;
                $scope.LOSSDETAILS.PINCODE = location.PINCODE;

                $('#SelectArea').modal('hide');
            }


       

            $scope.getCatastropheDescription = function () {

                if ($scope.CATASTROPHE.CATASTROPHETYPE) {
                    var catastrophe = $scope.CATASTROPHE.CATASTROPHETYPE;
                }
                else {
                    var catastrophe = '';
                }
                if (!!$scope.LOSSDETAILS.DATEOFLOSS) {
                    var lossDateCata = makeSQLServerComp($scope.LOSSDETAILS.DATEOFLOSS);
                }
                else {
                    var lossDateCata = new Date();
                }
                var input = {
                    catastropheType: catastrophe,
                    catastropheDate: lossDateCata
                }

                var serviceArgs = {

                    params: JSON.stringify(input),
                    load: function (data) {
                        $scope.decServiceCallCounter();

                        if (data.CatastropheDescription.items.length > 0) {
                            $scope.catastropheList = data.CatastropheDescription.items;
                        }
                        else {
                            $scope.catastropheList = [];
                        }
                    },
                    error: function (error) {
                        $scope.decServiceCallCounter();
                        $scope.showErrorMessage(error, "Error while Fetching Records");
                    }
                };
                $scope.incServiceCallCounter();
                $scope.context.options.getCatastropheDescription(serviceArgs);
            };

            $scope.$watch("AreaAutoCompleteService", function () {
                $scope.updateClaimantAreaItems = function (mode) {

                    if (mode == 'area') {
                        var input = {
                            searchValue: $scope.CLAIMANTDETAILS.ADDRESSLINE1,
                            searchParameter: 'area'
                        }

                        AreaAutoCompleteService.areaService($scope, input);
                    }
                    else if (mode == 'state') {
                        var input = {
                            searchValue: $scope.CLAIMANTDETAILS.STATE,
                            searchParameter: 'state'
                        }

                        AreaAutoCompleteService.areaService($scope, input);
                    }

                    else if (mode == 'city') {
                        var input = {
                            searchValue: $scope.CLAIMANTDETAILS.CITY,
                            searchParameter: 'city'
                        }

                        AreaAutoCompleteService.areaService($scope, input);
                    }
                    else if (mode == 'pincode') {
                        var input = {
                            searchValue: $scope.CLAIMANTDETAILS.PINCODE,
                            searchParameter: 'pincode'
                        }

                        AreaAutoCompleteService.areaService($scope, input);
                    }
                }
            });


            $scope.onClaimantAreaSelected = function ($item, $model, $label) {
                var dataarray = new Array();
                dataarray = $item.split("|");
                $scope.CLAIMANTDETAILS.PINCODE = dataarray[0].trim();
                $scope.CLAIMANTDETAILS.ADDRESSLINE1 = dataarray[1].trim();
                $scope.CLAIMANTDETAILS.STATE = dataarray[2].trim();
                $scope.CLAIMANTDETAILS.CITY = dataarray[3].trim();
                $scope.CLAIMANTDETAILS.DISTRICT = $scope.CLAIMANTDETAILS.CITY;
            };

            $scope.onClaimantCitySelected = function ($item, $model, $label) {

                var dataarray = new Array();
                dataarray = $item.split("|");
                $scope.CLAIMANTDETAILS.CITY = dataarray[0].trim();
                $scope.CLAIMANTDETAILS.STATE = dataarray[1].trim();
            };




            $scope.catastropheCode = function (x) {
                $scope.CATASTROPHE.CATASTROPHEDESCRIPTION = x.CATASTROPHEDESCRIPTION;
                $scope.CATASTROPHE.CATASTROPHECODE = x.CATASTROPHEID;
                $scope.CATASTROPHE.CATASTROPHEMASTERID = x.CATASTROPHEID;
            }

            $scope.SaveFNOL = function () {

                if ($scope.callerMobileNumberList.length > 0) {
                    $scope.CALLERDETAILS.INSUREDMOBILENUMBER = $scope.callerMobileNumberList.filter(Boolean).join();
                }

                if ($scope.callerInsuredEmailList.length > 0) {
                    $scope.CALLERDETAILS.INSUREDEMAILID = $scope.callerInsuredEmailList.filter(Boolean).join();
                }

                if ($scope.emailIDCommunicationList.length > 0) {
                    $scope.CALLERDETAILS.EMAILIDFORCOMMUNICATION = $scope.emailIDCommunicationList.filter(Boolean).join();
                }

                if ($scope.WCPA) {
                    $scope.claimSummaryFNOL.TYPEOFCLAIM = 'WCPA'
                }
                else {
                    $scope.claimSummaryFNOL.TYPEOFCLAIM = 'RESTOFAIGC'
                }

                $scope.claimSummaryFNOL.CLAIMANTNAME = $scope.CLAIMANTDETAILS.CLAIMANTNAME;

                $scope.claimSummaryFNOL.DATEOFLOSS = $scope.LOSSDETAILS.DATEOFLOSS;
                $scope.claimSummaryFNOL.REPORTEDDATE = makeSQLServerComp($scope.claimSummaryFNOL.REPORTEDDATE);
                $scope.claimSummaryFNOL.DATEOFLOSS = makeSQLServerComp($scope.claimSummaryFNOL.DATEOFLOSS);
                $scope.LOSSDETAILS.DATEOFLOSS = makeSQLServerComp($scope.LOSSDETAILS.DATEOFLOSS);

                if ($scope.claimSummaryFNOL.FIRDETAIL == 'true' && (!!$scope.FIRDETAILS.FIRDATE)) {
                    $scope.FIRDETAILS.FIRDATE = makeSQLServerComp($scope.FIRDETAILS.FIRDATE);
                    if($scope.claimSummary.FIRDETAIL == 'false'){
                        var FIR = 'create';
                    }
                    else{
                        var FIR = 'update';
                    }
                }
                else{
                    if ($scope.claimSummary.FIRDETAIL == 'true') {
                        var FIR = 'delete';
                    }
                    else{
                        var FIR = '';
                    }
                }

                if ($scope.claimSummary.ISEVENTCATASTROPHE == 'true'){

                    if ($scope.claimSummaryFNOL.ISEVENTCATASTROPHE == 'true'){
                        var catastrophe = 'update';
                    }
                    else{
                        var catastrophe = 'delete';
                    }
                }

                else{
                    if ($scope.claimSummaryFNOL.ISEVENTCATASTROPHE == 'true') {
                        var catastrophe = 'create';
                    }
                    else{
                        var catastrophe = '';
                    }
                }


 

                var input = {
                    "AddFNOLInput": {
                        "CallerDetails": $scope.CALLERDETAILS,
                        "LossDetails": $scope.LOSSDETAILS,
                        "ClaimantDetails": $scope.CLAIMANTDETAILS,
                        "Catastrophe": $scope.CATASTROPHE,
                        "FIRDetails": $scope.FIRDETAILS,
                        "HospitalDetails": $scope.HOSPITALDETAILS,
                        "FeatureSummary": $scope.claimSummaryFNOL
                        
                    },
                    
                        "FirChange": FIR,
                    "catastropheChange": catastrophe
                }

                var serviceArgs = {

                    params: JSON.stringify(input),
                    load: function (data) {
                        $scope.decServiceCallCounter();
                        $scope.showSuccessMessage('FNOL details updated!');
                    },
                    error: function (error) {
                        $scope.decServiceCallCounter();
                        $scope.showErrorMessage(error, "Error while updating FNOL details!");
                    }
                };
                $scope.incServiceCallCounter();
                $scope.context.options.saveFNOL(serviceArgs);
            };

            $scope.SaveHospital = function (x) {
                $scope.HOSPITALDETAILS.HOSPITALNAME = x.HOSPITALNAME;
                $scope.HOSPITALDETAILS.HOSPITALLOCATION = x.CITY;
                $scope.HOSPITALDETAILS.HOSPITALMASTERID = x.HOSPITALMASTERID;

                $('#hospitalSearch').modal('hide');
            }

            $scope.getHospitalList = function (currentPosition) {

                if ($scope.hospitalSearchParam == undefined || $scope.hospitalSearchParam == '' || $scope.hospitalSearchParam == null) {
                    $scope.showWarningMessage('Please select search criteria');
                    return;
                }

                var input = {
                    "searchText": $scope.hospitalSearch,
                    "SearchParam": $scope.hospitalSearchParam,
                    "tableName": "HOSPITALMASTER",
                    "startPosition": currentPosition - 1,
                    "orderBy": "HOSPITALNAME",
                    "noOfRow": 7
                }
                var serviceArgs = {
                    params: JSON.stringify(input),
                    load: function (data) {

                        if (data.searchResults.items.length > 0) {
                            $scope.hospitalList = data.searchResults.items;
                            $scope.modalresultCount = data.RESULTCOUNT;
                            $scope.modaltableData = false;
                            $scope.assignModalPagVariables();
                        } else {
                            $scope.modaltotalPages = 0;
                            $scope.hospitalList = {};
                            $scope.modaltableData = true;
                        }
                        $scope.decServiceCallCounter();
                        $scope.$apply();
                    },
                    error: function (e) {
                        $scope.decServiceCallCounter();
                        $scope.showErrorMessage(e, "Error while getting hospital list");
                    }
                };
                $scope.incServiceCallCounter();
                $scope.context.options.getSearchData(serviceArgs);

            }

            /*FNOL End*/

            $scope.openAssessment = function(data,SECTIONCODE){
                    var modifiedData ={}; 
                    switch(SECTIONCODE){
                        case 'BAP' : 
                            if(data.TABDATA.items[0].rows.items.length > 0){
                                modifiedData = data.TABDATA.items[0].rows.items[0].indexedMap;
                                modifiedData.LABOURCHARGES = parseFloat(modifiedData.LABOURCHARGES);
                                modifiedData.RRCOSTOFEQUIPMENT = parseFloat(modifiedData.RRCOSTOFEQUIPMENT);
                                modifiedData.GROSSASSESSEDLOSS = parseFloat(modifiedData.GROSSASSESSEDLOSS);
                                modifiedData.SGST = parseFloat(modifiedData.SGST);
                                modifiedData.CGST = parseFloat(modifiedData.CGST);
                                modifiedData.IGST = parseFloat(modifiedData.IGST);
                                modifiedData.LESSIMPROVEMENTS = parseFloat(modifiedData.LESSIMPROVEMENTS);
                                modifiedData.LESSDEPRECIATION = parseFloat(modifiedData.LESSDEPRECIATION);
                                modifiedData.LESSSALVAGE = parseFloat(modifiedData.LESSSALVAGE);
                                modifiedData.UNDERINSURANCE = parseFloat(modifiedData.UNDERINSURANCE);
                                modifiedData.ADJUSTMENTS = parseFloat(modifiedData.ADJUSTMENTS);
                                modifiedData.NETASSESSEDLOSS = parseFloat(modifiedData.NETASSESSEDLOSS);
                                modifiedData.LESSINTERIMPAYMENTS = parseFloat(modifiedData.LESSINTERIMPAYMENTS);
                                modifiedData.LESSEXCESS = parseFloat(modifiedData.LESSEXCESS);
                                modifiedData.LESSREINSTATEMENTPREMIUM = parseFloat(modifiedData.LESSREINSTATEMENTPREMIUM);
                                modifiedData.AMOUNTPAYABLE = parseFloat(modifiedData.AMOUNTPAYABLE);
                                $scope.BAP=modifiedData;
                            }
                                
                            break;
                        case 'PNSB' : 
                            if(data.TABDATA.items[0].rows.items.length > 0){
                                modifiedData=data.TABDATA.items[0].rows.items[0].indexedMap;
                                modifiedData.LABOURCHARGES = parseFloat(modifiedData.LABOURCHARGES);
                                modifiedData.MATERIALCOST = parseFloat(modifiedData.MATERIALCOST);
                                modifiedData.TRANSPORTATIONCOST = parseFloat(modifiedData.TRANSPORTATIONCOST);
                                modifiedData.GROSSASSESSEDLOSS = parseFloat(modifiedData.GROSSASSESSEDLOSS);
                                modifiedData.SGST = parseFloat(modifiedData.SGST);
                                modifiedData.CGST = parseFloat(modifiedData.CGST);
                                modifiedData.IGST = parseFloat(modifiedData.IGST);
                                modifiedData.LESSDEPRECIATION = parseFloat(modifiedData.LESSDEPRECIATION);
                                modifiedData.LESSSALVAGE = parseFloat(modifiedData.LESSSALVAGE);
                                modifiedData.UNDERINSURANCE = parseFloat(modifiedData.UNDERINSURANCE);
                                modifiedData.ADJUSTMENTS = parseFloat(modifiedData.ADJUSTMENTS);
                                modifiedData.NETASSESSEDLOSS = parseFloat(modifiedData.NETASSESSEDLOSS);
                                modifiedData.LESSINTERIMPAYMENTS = parseFloat(modifiedData.LESSINTERIMPAYMENTS);
                                modifiedData.LESSEXCESS = parseFloat(modifiedData.LESSEXCESS);
                                modifiedData.LESSREINSTATEMENTPREMIUM = parseFloat(modifiedData.LESSREINSTATEMENTPREMIUM);
                                modifiedData.AMOUNTPAYABLE = parseFloat(modifiedData.AMOUNTPAYABLE);
                                $scope.PNSB = modifiedData;
                            }
                                
                            break;
                        case 'EEI' : 
                            if(data.TABDATA.items[0].rows.items.length > 0){
                                modifiedData=data.TABDATA.items[0].rows.items[0].indexedMap;
                                modifiedData.EEILOSS = parseFloat(modifiedData.EEILOSS);
                                modifiedData.EXTERNALDATAMEDIA = parseFloat(modifiedData.EXTERNALDATAMEDIA);
                                modifiedData.ICWINCREASECOSTOFWORKING = parseFloat(modifiedData.ICWINCREASECOSTOFWORKING);
                                modifiedData.EXPRESSFREIGHT = parseFloat(modifiedData.EXPRESSFREIGHT);
                                modifiedData.OWNERSSURROUNDING = parseFloat(modifiedData.OWNERSSURROUNDING);
                                modifiedData.EQUIPMENT = parseFloat(modifiedData.EQUIPMENT);
                                modifiedData.THIRDPARTY = parseFloat(modifiedData.THIRDPARTY);
                                modifiedData.ADDLCUSTOMDUTY = parseFloat(modifiedData.ADDLCUSTOMDUTY);
                                modifiedData.GROSSASSESSEDLOSS = parseFloat(modifiedData.GROSSASSESSEDLOSS);
                                modifiedData.SGST = parseFloat(modifiedData.SGST);
                                modifiedData.CGST = parseFloat(modifiedData.CGST);
                                modifiedData.IGST = parseFloat(modifiedData.IGST);
                                modifiedData.IMPROVEMENTPENCENT = parseFloat(modifiedData.IMPROVEMENTPENCENT);
                                modifiedData.LESSIMPROVEMENTS = parseFloat(modifiedData.LESSIMPROVEMENTS);
                                modifiedData.LESSDEPRECIATION = parseFloat(modifiedData.LESSDEPRECIATION);
                                modifiedData.LESSSALVAGE = parseFloat(modifiedData.LESSSALVAGE);
                                modifiedData.UNDERINSURANCE = parseFloat(modifiedData.UNDERINSURANCE);
                                modifiedData.ADJUSTMENTS = parseFloat(modifiedData.ADJUSTMENTS);
                                modifiedData.NETASSESSEDLOSS = parseFloat(modifiedData.NETASSESSEDLOSS);
                                modifiedData.LESSINTERIMPAYMENTS = parseFloat(modifiedData.LESSINTERIMPAYMENTS);
                                modifiedData.LESSEXCESS = parseFloat(modifiedData.LESSEXCESS);
                                modifiedData.LESSREINSTATEMENTPREMIUM = parseFloat(modifiedData.LESSREINSTATEMENTPREMIUM);
                                modifiedData.AMOUNTPAYABLE = parseFloat(modifiedData.AMOUNTPAYABLE);
                                $scope.EEI=modifiedData;
                            }
                                
                            break;
                        case 'MBD' : 
                        if(data.TABDATA.items[0].rows.items.length > 0){
                            modifiedData=data.TABDATA.items[0].rows.items[0].indexedMap;
                            modifiedData.MBLOSS = parseFloat(modifiedData.MBLOSS);
                            modifiedData.EXPRESSFREIGHT = parseFloat(modifiedData.EXPRESSFREIGHT);
                            modifiedData.OWNERSSURROUNDING = parseFloat(modifiedData.OWNERSSURROUNDING);
                            modifiedData.EQUIPMENT = parseFloat(modifiedData.EQUIPMENT);
                            modifiedData.THIRDPARTY = parseFloat(modifiedData.THIRDPARTY);
                            modifiedData.ADDLCUSTOMDUTY = parseFloat(modifiedData.ADDLCUSTOMDUTY);
                            modifiedData.GROSSASSESSEDLOSS = parseFloat(modifiedData.GROSSASSESSEDLOSS);
                            modifiedData.SGST = parseFloat(modifiedData.SGST);
                            modifiedData.CGST = parseFloat(modifiedData.CGST);
                            modifiedData.IGST = parseFloat(modifiedData.IGST);
                            modifiedData.IMPROVEMENTPENCENT = parseFloat(modifiedData.IMPROVEMENTPENCENT);
                            modifiedData.LESSIMPROVEMENTS = parseFloat(modifiedData.LESSIMPROVEMENTS);
                            modifiedData.LESSDEPRECIATION = parseFloat(modifiedData.LESSDEPRECIATION);
                            modifiedData.LESSSALVAGE = parseFloat(modifiedData.LESSSALVAGE);
                            modifiedData.UNDERINSURANCE = parseFloat(modifiedData.UNDERINSURANCE);
                            modifiedData.ADJUSTMENTS = parseFloat(modifiedData.ADJUSTMENTS);
                            modifiedData.NETASSESSEDLOSS = parseFloat(modifiedData.NETASSESSEDLOSS);
                            modifiedData.LESSINTERIMPAYMENTS = parseFloat(modifiedData.LESSINTERIMPAYMENTS);
                            modifiedData.LESSEXCESS = parseFloat(modifiedData.LESSEXCESS);
                            modifiedData.LESSREINSTATEMENTPREMIUM = parseFloat(modifiedData.LESSREINSTATEMENTPREMIUM);
                            modifiedData.AMOUNTPAYABLE = parseFloat(modifiedData.AMOUNTPAYABLE);
                            $scope.MBD=modifiedData;
                        }
                                
                            break;
                        case 'MONEY' :
                            if(data.TABDATA.items[0].rows.items.length > 0){
                                modifiedData=data.TABDATA.items[0].rows.items[0].indexedMap; 
                                modifiedData.GROSSASSESSEDLOSS = parseFloat(modifiedData.GROSSASSESSEDLOSS);
                                modifiedData.UNDERINSURANCE = parseFloat(modifiedData.UNDERINSURANCE);
                                modifiedData.LESSOTHERDEDUCTIONS = parseFloat(modifiedData.LESSOTHERDEDUCTIONS);
                                modifiedData.NETASSESSEDLOSS = parseFloat(modifiedData.NETASSESSEDLOSS);
                                modifiedData.LESSINTERIMPAYMENTS = parseFloat(modifiedData.LESSINTERIMPAYMENTS);
                                modifiedData.LESSREINSTATEMENTPREMIUM = parseFloat(modifiedData.LESSREINSTATEMENTPREMIUM);
                                modifiedData.LESSEXCESS = parseFloat(modifiedData.LESSEXCESS);
                                $scope.MT=modifiedData; 
                            }
                                
                            break;
                        case 'BURGLARY' : 
                        if(data.TABDATA.items[0].rows.items.length > 0){
                            modifiedData=data.TABDATA.items[0].rows.items[0].indexedMap;
                            modifiedData.BUILDING = parseFloat(modifiedData.BUILDING);
                            modifiedData.PANDM = parseFloat(modifiedData.PANDM);
                            modifiedData.STOCK = parseFloat(modifiedData.STOCK);
                            modifiedData.OTHERCOST = parseFloat(modifiedData.OTHERCOST);
                            modifiedData.GROSSASSESSEDLOSS = parseFloat(modifiedData.GROSSASSESSEDLOSS);
                            modifiedData.SGST = parseFloat(modifiedData.SGST);
                            modifiedData.CGST = parseFloat(modifiedData.CGST);
                            modifiedData.IGST = parseFloat(modifiedData.IGST);
                            modifiedData.LESSDEPRECIATION = parseFloat(modifiedData.LESSDEPRECIATION);
                            modifiedData.LESSSALVAGE = parseFloat(modifiedData.LESSSALVAGE);
                            modifiedData.UNDERINSURANCE = parseFloat(modifiedData.UNDERINSURANCE);
                            modifiedData.ADJUSTMENTS = parseFloat(modifiedData.ADJUSTMENTS);
                            modifiedData.NETASSESSEDLOSS = parseFloat(modifiedData.NETASSESSEDLOSS);
                            modifiedData.LESSINTERIMPAYMENTS = parseFloat(modifiedData.LESSINTERIMPAYMENTS);
                            modifiedData.LESSEXCESS = parseFloat(modifiedData.LESSEXCESS);
                            modifiedData.LESSREINSTATEMENTPREMIUM = parseFloat(modifiedData.LESSREINSTATEMENTPREMIUM);
                            modifiedData.AMOUNTPAYABLE = parseFloat(modifiedData.AMOUNTPAYABLE);
                            $scope.BURGLARY=modifiedData;
                        }
                                
                            break;
                        case 'BAGGAGE' :
                            if(data.TABDATA.items[0].rows.items.length > 0){
                                modifiedData=data.TABDATA.items[0].rows.items[0].indexedMap;
                                modifiedData.GROSSASSESSEDLOSS = parseFloat(modifiedData.GROSSASSESSEDLOSS);
                                modifiedData.SGST = parseFloat(modifiedData.SGST);
                                modifiedData.CGST = parseFloat(modifiedData.CGST);
                                modifiedData.IGST = parseFloat(modifiedData.IGST);
                                modifiedData.LESSRECOVERY = parseFloat(modifiedData.LESSRECOVERY);
                                modifiedData.NETASSESSEDLOSS = parseFloat(modifiedData.NETASSESSEDLOSS);
                                modifiedData.LESSINTERIMPAYMENTS = parseFloat(modifiedData.LESSINTERIMPAYMENTS);
                                modifiedData.LESSEXCESS = parseFloat(modifiedData.LESSEXCESS);
                                modifiedData.LESSREINSTATEMENTPREMIUM = parseFloat(modifiedData.LESSREINSTATEMENTPREMIUM);
                                modifiedData.AMOUNTPAYABLE = parseFloat(modifiedData.AMOUNTPAYABLE);
                                $scope.BAGGAGE=modifiedData;
                            }
                                 
                            break;
                        case 'BPP' :
                            if(data.TABDATA.items[0].rows.items.length > 0){
                                modifiedData=data.TABDATA.items[0].rows.items[0].indexedMap; 
                                modifiedData.BOILERLOSS = parseFloat(modifiedData.BOILERLOSS);
                                modifiedData.EXPRESSFREIGHT = parseFloat(modifiedData.EXPRESSFREIGHT);
                                modifiedData.OWNERSSURROUNDING = parseFloat(modifiedData.OWNERSSURROUNDING);
                                modifiedData.THIRDPARTY = parseFloat(modifiedData.THIRDPARTY);
                                modifiedData.ADDLCUSTOMDUTY = parseFloat(modifiedData.ADDLCUSTOMDUTY);
                                modifiedData.GROSSASSESSEDLOSS = parseFloat(modifiedData.GROSSASSESSEDLOSS);
                                modifiedData.SGST = parseFloat(modifiedData.SGST);
                                modifiedData.CGST = parseFloat(modifiedData.CGST);
                                modifiedData.IGST = parseFloat(modifiedData.IGST);
                                modifiedData.IMPROVEMENTPENCENT = parseFloat(modifiedData.IMPROVEMENTPENCENT);
                                modifiedData.LESSIMPROVEMENTS = parseFloat(modifiedData.LESSIMPROVEMENTS);
                                modifiedData.LESSDEPRECIATION = parseFloat(modifiedData.LESSDEPRECIATION);
                                modifiedData.LESSSALVAGE = parseFloat(modifiedData.LESSSALVAGE);
                                modifiedData.UNDERINSURANCE = parseFloat(modifiedData.UNDERINSURANCE);
                                modifiedData.ADJUSTMENTS = parseFloat(modifiedData.ADJUSTMENTS);
                                modifiedData.NETASSESSEDLOSS = parseFloat(modifiedData.NETASSESSEDLOSS);
                                modifiedData.LESSINTERIMPAYMENTS = parseFloat(modifiedData.LESSINTERIMPAYMENTS);
                                modifiedData.LESSEXCESS = parseFloat(modifiedData.LESSEXCESS);
                                modifiedData.LESSREINSTATEMENTPREMIUM = parseFloat(modifiedData.LESSREINSTATEMENTPREMIUM);
                                modifiedData.AMOUNTPAYABLE = parseFloat(modifiedData.AMOUNTPAYABLE);
                                $scope.BPP=modifiedData; 
                            }  
                            break;
                        case 'FG' :
                            if(data.TABDATA.items[0].rows.items.length > 0){
                                modifiedData=data.TABDATA.items[0].rows.items[0].indexedMap;
                                modifiedData.NUMBEROFEMPLOYEES = parseFloat(modifiedData.NUMBEROFEMPLOYEES);
                                modifiedData.SIPERPERSON = parseFloat(modifiedData.SIPERPERSON);
                                modifiedData.AOALIMIT = parseFloat(modifiedData.AOALIMIT);
                                modifiedData.GROSSASSESSEDLOSS = parseFloat(modifiedData.GROSSASSESSEDLOSS);
                                modifiedData.LESSRECOVERY = parseFloat(modifiedData.LESSRECOVERY);
                                modifiedData.LESSEXCESS = parseFloat(modifiedData.LESSEXCESS);
                                modifiedData.LESSOTHERDEDUCTION = parseFloat(modifiedData.LESSOTHERDEDUCTION);
                                modifiedData.NETASSESSEDLOSS = parseFloat(modifiedData.NETASSESSEDLOSS);
                                modifiedData.LESSREINSTATEMENTPREMIUM = parseFloat(modifiedData.LESSREINSTATEMENTPREMIUM);
                                modifiedData.LESSINTERIMPAYMENTS = parseFloat(modifiedData.LESSINTERIMPAYMENTS);
                                modifiedData.AMOUNTPAYABLE = parseFloat(modifiedData.AMOUNTPAYABLE);
                                $scope.FG=modifiedData;
                            }
                            break;
                        case 'FLOP' :
                            if(data.TABDATA.items[0].rows.items.length > 0){
                                modifiedData=data.TABDATA.items[0].rows.items[0].indexedMap;
                                modifiedData.GROSSPROFIT = parseFloat(modifiedData.GROSSPROFIT);
                                modifiedData.GPRATIO = parseFloat(modifiedData.GPRATIO);
                                modifiedData.LOSSDUETOTURNOVER = parseFloat(modifiedData.LOSSDUETOTURNOVER);
                                modifiedData.INCREASECOSTOFWORKING = parseFloat(modifiedData.INCREASECOSTOFWORKING);
                                modifiedData.AUDITORSFEES = parseFloat(modifiedData.AUDITORSFEES);
                                modifiedData.PROFESSIONALFEES = parseFloat(modifiedData.PROFESSIONALFEES);
                                modifiedData.STANDINGCHARGES = parseFloat(modifiedData.STANDINGCHARGES);
                                modifiedData.GROSSASSESSEDLOSS = parseFloat(modifiedData.GROSSASSESSEDLOSS);
                                modifiedData.UNDERINSURANCE = parseFloat(modifiedData.UNDERINSURANCE);
                                modifiedData.LOSSBEFOREEXCESS = parseFloat(modifiedData.LOSSBEFOREEXCESS);
                                modifiedData.LESSEXCESS = parseFloat(modifiedData.LESSEXCESS);
                                modifiedData.ADJUSTMENTS = parseFloat(modifiedData.ADJUSTMENTS);
                                modifiedData.NETASSESSEDLOSS = parseFloat(modifiedData.NETASSESSEDLOSS);
                                modifiedData.LESSINTERIMPAYMENTS = parseFloat(modifiedData.LESSINTERIMPAYMENTS);
                                modifiedData.LESSREINSTATEMENTPREMIUM = parseFloat(modifiedData.LESSREINSTATEMENTPREMIUM);
                                modifiedData.AMOUNTPAYABLE = parseFloat(modifiedData.AMOUNTPAYABLE);
                                $scope.FLOP=modifiedData;
                            }
                            break;
                        case 'PL' :
                            if(data.TABDATA.items[0].rows.items.length > 0){
                                modifiedData=data.TABDATA.items[0].rows.items[0].indexedMap;
                                modifiedData.DEFENCECOST =parseFloat(modifiedData.DEFENCECOST); 
                                modifiedData.MEDICALEXPENSES =parseFloat(modifiedData.MEDICALEXPENSES); 
                                modifiedData.DAMAGES =parseFloat(modifiedData.DAMAGES); 
                                modifiedData.OTHERCOST =parseFloat(modifiedData.OTHERCOST); 
                                modifiedData.NETASSESSEDLOSS =parseFloat(modifiedData.NETASSESSEDLOSS); 
                                modifiedData.LESSREINSTATEMENTPREMIUM =parseFloat(modifiedData.LESSREINSTATEMENTPREMIUM); 
                                modifiedData.LESSEXCESS =parseFloat(modifiedData.LESSEXCESS); 
                                modifiedData.AMOUNTPAYABLE =parseFloat(modifiedData.AMOUNTPAYABLE); 
                                $scope.PL=modifiedData;
                            }
                            break;
                        case 'FIRE' :
                            if(data.TABDATA.items[0].rows.items.length > 0){
                                modifiedData=data.TABDATA.items[0].rows.items[0].indexedMap;
                                modifiedData.BUILDINGGROSSASSESSEDLOSS =parseFloat(modifiedData.BUILDINGGROSSASSESSEDLOSS); 
                                modifiedData.BUILDINGLESSDEPRECIATION =parseFloat(modifiedData.BUILDINGLESSDEPRECIATION); 
                                modifiedData.BUILDINGLESSSALVAGE =parseFloat(modifiedData.BUILDINGLESSSALVAGE); 
                                modifiedData.BUILDINGUNDERINSURANCE =parseFloat(modifiedData.BUILDINGUNDERINSURANCE); 
                                modifiedData.BUILDINGNETASSESSEDLOSS =parseFloat(modifiedData.BUILDINGNETASSESSEDLOSS); 
                                modifiedData.STOCKGROSSASSESSEDLOSS =parseFloat(modifiedData.STOCKGROSSASSESSEDLOSS); 
                                modifiedData.STOCKLESSDEPRECIATION =parseFloat(modifiedData.STOCKLESSDEPRECIATION); 
                                modifiedData.STOCKLESSSALVAGE =parseFloat(modifiedData.STOCKLESSSALVAGE); 
                                modifiedData.STOCKUNDERINSURANCE =parseFloat(modifiedData.STOCKUNDERINSURANCE); 
                                modifiedData.STOCKNETASSESSEDLOSS =parseFloat(modifiedData.STOCKNETASSESSEDLOSS); 
                                modifiedData.PLANTMACHINERYGROSSASSESSEDLOSS =parseFloat(modifiedData.PLANTMACHINERYGROSSASSESSEDLOSS); 
                                modifiedData.PLANTMACHINERYLESSDEPRECIATION =parseFloat(modifiedData.PLANTMACHINERYLESSDEPRECIATION); 
                                modifiedData.PLANTMACHINERYLESSSALVAGE =parseFloat(modifiedData.PLANTMACHINERYLESSSALVAGE); 
                                modifiedData.PLANTMACHINERYUNDERINSURANCE =parseFloat(modifiedData.PLANTMACHINERYUNDERINSURANCE); 
                                modifiedData.PLANTMACHINERYNETASSESSEDLOSS =parseFloat(modifiedData.PLANTMACHINERYNETASSESSEDLOSS); 
                                modifiedData.PLINTHFOUNDGROSSASSESSEDLOSS =parseFloat(modifiedData.PLINTHFOUNDGROSSASSESSEDLOSS); 
                                modifiedData.PLINTHFOUNDLESSDEPRECIATION =parseFloat(modifiedData.PLINTHFOUNDLESSDEPRECIATION); 
                                modifiedData.PLINTHFOUNDLESSSALVAGE =parseFloat(modifiedData.PLINTHFOUNDLESSSALVAGE); 
                                modifiedData.PLINTHFOUNDUNDERINSURANCE =parseFloat(modifiedData.PLINTHFOUNDUNDERINSURANCE); 
                                modifiedData.PLINTHFOUNDNETASSESSEDLOSS =parseFloat(modifiedData.PLINTHFOUNDNETASSESSEDLOSS); 
                                modifiedData.COMPOUNDWALLGROSSASSESSEDLOSS =parseFloat(modifiedData.COMPOUNDWALLGROSSASSESSEDLOSS); 
                                modifiedData.COMPOUNDWALLLESSDEPRECIATION =parseFloat(modifiedData.COMPOUNDWALLLESSDEPRECIATION); 
                                modifiedData.COMPOUNDWALLLESSSALVAGE =parseFloat(modifiedData.COMPOUNDWALLLESSSALVAGE); 
                                modifiedData.COMPOUNDWALLUNDERINSURANCE =parseFloat(modifiedData.COMPOUNDWALLUNDERINSURANCE); 
                                modifiedData.COMPOUNDWALLNETASSESSEDLOSS =parseFloat(modifiedData.COMPOUNDWALLNETASSESSEDLOSS); 
                                modifiedData.RAWMATGROSSASSESSEDLOSS =parseFloat(modifiedData.RAWMATGROSSASSESSEDLOSS); 
                                modifiedData.RAWMATLESSDEPRECIATION =parseFloat(modifiedData.RAWMATLESSDEPRECIATION); 
                                modifiedData.RAWMATLESSSALVAGE =parseFloat(modifiedData.RAWMATLESSSALVAGE); 
                                modifiedData.RAWMATUNDERINSURANCE =parseFloat(modifiedData.RAWMATUNDERINSURANCE); 
                                modifiedData.RAWMATNETASSESSEDLOSS =parseFloat(modifiedData.RAWMATNETASSESSEDLOSS); 
                                modifiedData.WORKINPROGROSSASSESSEDLOSS =parseFloat(modifiedData.WORKINPROGROSSASSESSEDLOSS); 
                                modifiedData.WORKINPROLESSDEPRECIATION =parseFloat(modifiedData.WORKINPROLESSDEPRECIATION); 
                                modifiedData.WORKINPROLESSSALVAGE =parseFloat(modifiedData.WORKINPROLESSSALVAGE); 
                                modifiedData.WORKINPROUNDERINSURANCE =parseFloat(modifiedData.WORKINPROUNDERINSURANCE); 
                                modifiedData.WORKINPRONETASSESSEDLOSS =parseFloat(modifiedData.WORKINPRONETASSESSEDLOSS); 
                                modifiedData.FINISHSTOCKGROSSASSESSEDLOSS =parseFloat(modifiedData.FINISHSTOCKGROSSASSESSEDLOSS); 
                                modifiedData.FINISHSTOCKLESSDEPRECIATION =parseFloat(modifiedData.FINISHSTOCKLESSDEPRECIATION); 
                                modifiedData.FINISHSTOCKLESSSALVAGE =parseFloat(modifiedData.FINISHSTOCKLESSSALVAGE); 
                                modifiedData.FINISHSTOCKUNDERINSURANCE =parseFloat(modifiedData.FINISHSTOCKUNDERINSURANCE); 
                                modifiedData.FINISHSTOCKNETASSESSEDLOSS =parseFloat(modifiedData.FINISHSTOCKNETASSESSEDLOSS); 
                                modifiedData.FFGROSSASSESSEDLOSS =parseFloat(modifiedData.FFGROSSASSESSEDLOSS); 
                                modifiedData.FFLESSDEPRECIATION =parseFloat(modifiedData.FFLESSDEPRECIATION); 
                                modifiedData.FFLESSSALVAGE =parseFloat(modifiedData.FFLESSSALVAGE); 
                                modifiedData.FFUNDERINSURANCE =parseFloat(modifiedData.FFUNDERINSURANCE); 
                                modifiedData.FFNETASSESSEDLOSS =parseFloat(modifiedData.FFNETASSESSEDLOSS); 
                                modifiedData.ELECTRICALFITGROSSASSESSEDLOSS =parseFloat(modifiedData.ELECTRICALFITGROSSASSESSEDLOSS); 
                                modifiedData.ELECTRICALFITLESSDEPRECIATION =parseFloat(modifiedData.ELECTRICALFITLESSDEPRECIATION); 
                                modifiedData.ELECTRICALFITLESSSALVAGE =parseFloat(modifiedData.ELECTRICALFITLESSSALVAGE); 
                                modifiedData.ELECTRICALFITUNDERINSURANCE =parseFloat(modifiedData.ELECTRICALFITUNDERINSURANCE); 
                                modifiedData.ELECTRICALFITNETASSESSEDLOSS =parseFloat(modifiedData.ELECTRICALFITNETASSESSEDLOSS); 
                                modifiedData.BRISKGROSSASSESSEDLOSS =parseFloat(modifiedData.BRISKGROSSASSESSEDLOSS); 
                                modifiedData.BRISKLESSDEPRECIATION =parseFloat(modifiedData.BRISKLESSDEPRECIATION); 
                                modifiedData.BRISKLESSSALVAGE =parseFloat(modifiedData.BRISKLESSSALVAGE); 
                                modifiedData.BRISKUNDERINSURANCE =parseFloat(modifiedData.BRISKUNDERINSURANCE); 
                                modifiedData.BRISKNETASSESSEDLOSS =parseFloat(modifiedData.BRISKNETASSESSEDLOSS); 
                                modifiedData.SPECIFIEDGROSSASSESSEDLOSS =parseFloat(modifiedData.SPECIFIEDGROSSASSESSEDLOSS); 
                                modifiedData.SPECIFIEDLESSDEPRECIATION =parseFloat(modifiedData.SPECIFIEDLESSDEPRECIATION); 
                                modifiedData.SPECIFIEDLESSSALVAGE =parseFloat(modifiedData.SPECIFIEDLESSSALVAGE); 
                                modifiedData.SPECIFIEDUNDERINSURANCE =parseFloat(modifiedData.SPECIFIEDUNDERINSURANCE); 
                                modifiedData.SPECIFIEDNETASSESSEDLOSS =parseFloat(modifiedData.SPECIFIEDNETASSESSEDLOSS); 
                                modifiedData.TAXRATE =parseFloat(modifiedData.TAXRATE); 
                                modifiedData.SGST =parseFloat(modifiedData.SGST); 
                                modifiedData.CGST =parseFloat(modifiedData.CGST); 
                                modifiedData.IGST =parseFloat(modifiedData.IGST); 
                                modifiedData.TOTALNETASSESSEDLOSS =parseFloat(modifiedData.TOTALNETASSESSEDLOSS); 
                                modifiedData.LESSEXCESS =parseFloat(modifiedData.LESSEXCESS); 
                                modifiedData.LESSOTHER =parseFloat(modifiedData.LESSOTHER); 
                                modifiedData.LESSRIPREMIUM =parseFloat(modifiedData.LESSRIPREMIUM); 
                                modifiedData.LESSINTERIMPAYMENTS =parseFloat(modifiedData.LESSINTERIMPAYMENTS); 
                                modifiedData.NETPAYABLE =parseFloat(modifiedData.NETPAYABLE); 
                                modifiedData.SIFORDEBRIS =parseFloat(modifiedData.SIFORDEBRIS); 
                                modifiedData.SIFORARCHITECTURAL =parseFloat(modifiedData.SIFORARCHITECTURAL); 
                                modifiedData.ADDDEBRISREMOVAL =parseFloat(modifiedData.ADDDEBRISREMOVAL); 
                                modifiedData.ADDARCHITECTURAL =parseFloat(modifiedData.ADDARCHITECTURAL); 
                                modifiedData.AMOUNTPAYABLE =parseFloat(modifiedData.AMOUNTPAYABLE); 
                                $scope.FIRE=modifiedData;
                            }
                            break;
                    }
            }
            //$scope.OpenClaim();


            /******************************** ASSESSMENT ***************************** */
            $scope.BAP = {
                "ISINVOICERAISEDINFAVOFTATAAIG" : 'NO',"REINSTPREMIUMAPPLICABLE" : 'NO',"LABOURCHARGES" : 0,"RRCOSTOFEQUIPMENT":0 ,"TAXRATE" :'0'
                ,"LESSIMPROVEMENTS":0,"LESSDEPRECIATION" : 0,"LESSSALVAGE" : 0,"UNDERINSURANCE" : 0,"ADJUSTMENTS" : 0,"LESSEXCESS" : 0, "LESSREINSTATEMENTPREMIUM" : 0,"LESSINTERIMPAYMENTS" : 0
            }
            $scope.PNSB ={
                "TAXRATE" :'0',"LABOURCHARGES" : 0,"MATERIALCOST" : 0,"TRANSPORTATIONCOST" : 0,"GROSSASSESSEDLOSS" : 0,"SGST" : 0,"CGST" : 0,"IGST" : 0,"LESSDEPRECIATION" : 0,
                "LESSSALVAGE" : 0,"UNDERINSURANCE" : 0,"ADJUSTMENTS" : 0,"NETASSESSEDLOSS" : 0,"LESSINTERIMPAYMENTS" : 0,"LESSEXCESS" : 0,"LESSREINSTATEMENTPREMIUM" : 0,
                "AMOUNTPAYABLE"  : 0,"ISINVOICERAISEDINFAVOFTATAAIG" : 'NO'
            };
            
            $scope.EEI ={
                "TAXRATE" :'0',"EEILOSS" : 0,"EXTERNALDATAMEDIA" : 0,"ICWINCREASECOSTOFWORKING" : 0,"EXPRESSFREIGHT" : 0,"OWNERSSURROUNDING" : 0,"EQUIPMENT" : 0,"THIRDPARTY" : 0,
                "ADDLCUSTOMDUTY" : 0,"GROSSASSESSEDLOSS" : 0,"SGST" : 0,"CGST" : 0,"IGST" : 0,"IMPROVEMENTPENCENT" : 0,"LESSIMPROVEMENTS" : 0,"LESSDEPRECIATION" : 0,
                "LESSSALVAGE" : 0,"UNDERINSURANCE" : 0,"ADJUSTMENTS" : 0,"NETASSESSEDLOSS" : 0,"LESSINTERIMPAYMENTS" : 0,"LESSEXCESS" : 0,"LESSREINSTATEMENTPREMIUM" : 0,
                "AMOUNTPAYABLE"  : 0,"ISINVOICERAISEDINFAVOFTATAAIG" : 'NO'
            } ;

            $scope.MBD ={
                "TAXRATE" :'0',"MBLOSS" : 0,"EXPRESSFREIGHT" : 0,"OWNERSSURROUNDING" : 0,"EQUIPMENT" : 0,"THIRDPARTY" : 0,"ADDLCUSTOMDUTY" : 0,"GROSSASSESSEDLOSS" : 0,"SGST" : 0,
                "CGST" : 0,"IGST" : 0,"IMPROVEMENTPENCENT" : 0,"LESSIMPROVEMENTS" : 0,"LESSDEPRECIATION" : 0,"LESSSALVAGE" : 0,"UNDERINSURANCE" : 0,"ADJUSTMENTS" : 0,
                "NETASSESSEDLOSS" : 0,"LESSINTERIMPAYMENTS" : 0,"LESSEXCESS" : 0,"LESSREINSTATEMENTPREMIUM" : 0,"AMOUNTPAYABLE": 0,"ISINVOICERAISEDINFAVOFTATAAIG" : 'NO',
            };

            $scope.MT ={
                "GROSSASSESSEDLOSS" : 0,"UNDERINSURANCE" : 0,"LESSOTHERDEDUCTIONS" : 0,"NETASSESSEDLOSS" : 0,"LESSINTERIMPAYMENTS" : 0,
                "LESSREINSTATEMENTPREMIUM" : 0,"LESSEXCESS" : 0
            };
            $scope.BURGLARY ={
                "TAXRATE" :'0',"BUILDING" : 0,"PANDM" : 0,"STOCK" : 0,"OTHERCOST" : 0,"GROSSASSESSEDLOSS" : 0,"SGST" : 0,"CGST" : 0,"IGST" : 0,"LESSDEPRECIATION" : 0,
                "LESSSALVAGE" : 0,"UNDERINSURANCE" : 0,"ADJUSTMENTS" : 0,"NETASSESSEDLOSS" : 0,"LESSINTERIMPAYMENTS" : 0,"LESSEXCESS" : 0,"LESSREINSTATEMENTPREMIUM" : 0,
                "AMOUNTPAYABLE" : 0,"ISINVOICERAISEDINFAVOFTATAAIG" : 'NO',"LESSIMPROVEMENTS" :0
            };
            $scope.BAGGAGE ={
                "TAXRATE" :'0',"GROSSASSESSEDLOSS" : 0,"SGST" : 0,"CGST" : 0,"IGST" : 0,"LESSRECOVERY" : 0,"NETASSESSEDLOSS" : 0,'LESSINTERIMPAYMENTS' : 0,
                "LESSEXCESS" : 0,"LESSREINSTATEMENTPREMIUM" : 0,"AMOUNTPAYABLE" : 0,"ISINVOICERAISEDINFAVOFTATAAIG" : 'NO'
            };
            $scope.BPP ={
                "TAXRATE" :'0',"BOILERLOSS" : 0,"EXPRESSFREIGHT" : 0,"OWNERSSURROUNDING" : 0,"THIRDPARTY" : 0,"ADDLCUSTOMDUTY" : 0,"GROSSASSESSEDLOSS" : 0,"SGST" : 0,
                "CGST" : 0,"IGST" : 0,"IMPROVEMENTPENCENT" : 0,"LESSIMPROVEMENTS" : 0,"LESSDEPRECIATION" : 0,"LESSSALVAGE" : 0,"UNDERINSURANCE" : 0,"ADJUSTMENTS" : 0,
                "NETASSESSEDLOSS" : 0,"LESSINTERIMPAYMENTS" : 0,"LESSEXCESS" : 0,"LESSREINSTATEMENTPREMIUM" : 0,"AMOUNTPAYABLE" : 0,"ISINVOICERAISEDINFAVOFTATAAIG" : 'NO'
            };
            $scope.FG ={
                "NUMBEROFEMPLOYEES" : 0,"SIPERPERSON" : 0,"AOALIMIT" : 0,"GROSSASSESSEDLOSS" : 0,"LESSRECOVERY" : 0,"LESSEXCESS" : 0,"LESSOTHERDEDUCTION" : 0,"NETASSESSEDLOSS" : 0,
                "LESSREINSTATEMENTPREMIUM" : 0,"LESSINTERIMPAYMENTS" : 0,"AMOUNTPAYABLE" : 0
            };
            $scope.FLOP = {
                "GROSSPROFIT" : 0,"GPRATIO" : 0,"LOSSDUETOTURNOVER" : 0,"INCREASECOSTOFWORKING" : 0,"AUDITORSFEES" : 0,"PROFESSIONALFEES" : 0,"STANDINGCHARGES" : 0,
                "GROSSASSESSEDLOSS" : 0,"UNDERINSURANCE" : 0,"LOSSBEFOREEXCESS" : 0,"LESSEXCESS" : 0,"ADJUSTMENTS" : 0,"NETASSESSEDLOSS" : 0,
                "LESSINTERIMPAYMENTS" : 0,"LESSREINSTATEMENTPREMIUM" : 0,"AMOUNTPAYABLE" : 0
            };
            $scope.PL = {
                "DEFENCECOST" : 0,"MEDICALEXPENSES" : 0,"DAMAGES" : 0,"OTHERCOST" : 0,"NETASSESSEDLOSS" : 0,"LESSREINSTATEMENTPREMIUM" : 0,"LESSEXCESS" : 0,
                "AMOUNTPAYABLE" : 0
            };
            $scope.FIRE = {
                "BUILDINGGROSSASSESSEDLOSS" : 0,"BUILDINGLESSDEPRECIATION" : 0,"BUILDINGLESSSALVAGE" : 0,"BUILDINGUNDERINSURANCE" : 0,"BUILDINGNETASSESSEDLOSS" : 0,
                "STOCKGROSSASSESSEDLOSS" : 0,"STOCKLESSDEPRECIATION" : 0,"STOCKLESSSALVAGE" : 0,"STOCKUNDERINSURANCE" : 0,"STOCKNETASSESSEDLOSS" : 0,"PLANTMACHINERYGROSSASSESSEDLOSS" : 0,
                "PLANTMACHINERYLESSDEPRECIATION" : 0,"PLANTMACHINERYLESSSALVAGE" : 0,"PLANTMACHINERYUNDERINSURANCE" : 0,"PLANTMACHINERYNETASSESSEDLOSS" : 0,
                "PLINTHFOUNDGROSSASSESSEDLOSS" : 0,"PLINTHFOUNDLESSDEPRECIATION" : 0,"PLINTHFOUNDLESSSALVAGE" : 0,"PLINTHFOUNDUNDERINSURANCE" : 0,"PLINTHFOUNDNETASSESSEDLOSS" : 0,
                "COMPOUNDWALLGROSSASSESSEDLOSS" : 0,"COMPOUNDWALLLESSDEPRECIATION" : 0,"COMPOUNDWALLLESSSALVAGE" : 0,"COMPOUNDWALLUNDERINSURANCE" : 0,"COMPOUNDWALLNETASSESSEDLOSS" : 0,
                "RAWMATGROSSASSESSEDLOSS" : 0,"RAWMATLESSDEPRECIATION" : 0,"RAWMATLESSSALVAGE" : 0,"RAWMATUNDERINSURANCE" : 0,"RAWMATNETASSESSEDLOSS" : 0,"WORKINPROGROSSASSESSEDLOSS" : 0,
                "WORKINPROLESSDEPRECIATION" : 0,"WORKINPROLESSSALVAGE" : 0,"WORKINPROUNDERINSURANCE" : 0,"WORKINPRONETASSESSEDLOSS" : 0,"FINISHSTOCKGROSSASSESSEDLOSS" : 0,
                "FINISHSTOCKLESSDEPRECIATION" : 0,"FINISHSTOCKLESSSALVAGE" : 0,"FINISHSTOCKUNDERINSURANCE" : 0,"FINISHSTOCKNETASSESSEDLOSS" : 0,"FFGROSSASSESSEDLOSS" : 0,
                "FFLESSDEPRECIATION" : 0,"FFLESSSALVAGE" : 0,"FFUNDERINSURANCE" : 0,"FFNETASSESSEDLOSS" : 0,"ELECTRICALFITGROSSASSESSEDLOSS" : 0,"ELECTRICALFITLESSDEPRECIATION" : 0,
                "ELECTRICALFITLESSSALVAGE" : 0,"ELECTRICALFITUNDERINSURANCE" : 0,"ELECTRICALFITNETASSESSEDLOSS" : 0,"BRISKGROSSASSESSEDLOSS" : 0,"BRISKLESSDEPRECIATION" : 0,
                "BRISKLESSSALVAGE" : 0,"BRISKUNDERINSURANCE" : 0,"BRISKNETASSESSEDLOSS" : 0,"SPECIFIEDGROSSASSESSEDLOSS" : 0,"SPECIFIEDLESSDEPRECIATION" : 0,"SPECIFIEDLESSSALVAGE" : 0,
                "SPECIFIEDUNDERINSURANCE" : 0,"SPECIFIEDNETASSESSEDLOSS" : 0,"SGST" : 0,"CGST" : 0,"IGST" : 0,"TOTALNETASSESSEDLOSS" : 0,"LESSEXCESS" : 0,"LESSOTHER" : 0,"LESSRIPREMIUM" : 0,
                "LESSINTERIMPAYMENTS" : 0,"NETPAYABLE" : 0,"SIFORDEBRIS" : 0,"SIFORARCHITECTURAL" : 0,"ADDDEBRISREMOVAL" : 0,"ADDARCHITECTURAL" : 0,"AMOUNTPAYABLE" : 0,"ISINVOICERAISEDINFAVOFTATAAIG" : 'NO'
            };
            $scope.sameState = false;
            $scope.checkGstState =  function(tataAigGstNumber,payeeGstNumber){
                //$scope.sameState =  !$scope.sameState;
                if (!!tataAigGstNumber && !!payeeGstNumber) {
                    var payeeGstNumberCode = payeeGstNumber.substring(0, 2);
                    var tataAigGstNumberStateCode = tataAigGstNumber.substring(0, 2);
                    if (payeeGstNumberCode == tataAigGstNumberStateCode) {
                        $scope.sameState = true;
                    } else {
                        $scope.sameState = false;
                    }
                }
            }

            $scope.saveAssessment = function(data,assessmentName,assessmentID,formName){

                console.log(!formName.$valid)
                if (!formName.$valid) {
                    formName.submitted = true;
                    //$scope.showWarningMessage("invalid form please fill all required field");
                    return;
                } else {
                    formName.submitted = false;
                }

                //if(data.GROSSASSESSEDLOSS < 0 || data.NETASSESSEDLOSS < 0 || data.AMOUNTPAYABLE <=0){
                if(data.AMOUNTPAYABLE <=0){
                    console.log("AMOUNTPAYABLE can't be less than zero");
                    $scope.showWarningMessage("AMOUNTPAYABLE can't be less than zero");
                    return;
                }
                $scope.dataToInsert = JSON.parse(JSON.stringify(data));
                $scope.dataToInsert.FEATURESUMMARYID = $scope.claimSummary.AIGCFEATURESUMMARYID;
                var input = {
                    "ASSESSMENTNAME" : assessmentName,
                    "FEATURESUMMARYID" : $scope.claimSummary.AIGCFEATURESUMMARYID,
                    "ASSESSMENTID" : assessmentID
                };
                switch(assessmentName){
                    case 'BAP' :
                        $scope.dataToInsert.LABOURCHARGES = $scope.dataToInsert.LABOURCHARGES + '';
                        $scope.dataToInsert.RRCOSTOFEQUIPMENT = $scope.dataToInsert.RRCOSTOFEQUIPMENT + '';
                        $scope.dataToInsert.GROSSASSESSEDLOSS = $scope.dataToInsert.GROSSASSESSEDLOSS + '';
                        $scope.dataToInsert.SGST = $scope.dataToInsert.SGST + '';
                        $scope.dataToInsert.CGST = $scope.dataToInsert.CGST + '';
                        $scope.dataToInsert.IGST = $scope.dataToInsert.IGST + '';
                        $scope.dataToInsert.LESSIMPROVEMENTS = $scope.dataToInsert.LESSIMPROVEMENTS + '';
                        $scope.dataToInsert.LESSDEPRECIATION = $scope.dataToInsert.LESSDEPRECIATION + '';
                        $scope.dataToInsert.LESSSALVAGE = $scope.dataToInsert.LESSSALVAGE + '';
                        $scope.dataToInsert.UNDERINSURANCE = $scope.dataToInsert.UNDERINSURANCE + '';
                        $scope.dataToInsert.ADJUSTMENTS = $scope.dataToInsert.ADJUSTMENTS + '';
                        $scope.dataToInsert.ADJUSTMENTDETAILS = $scope.dataToInsert.ADJUSTMENTDETAILS + '';
                        $scope.dataToInsert.NETASSESSEDLOSS = $scope.dataToInsert.NETASSESSEDLOSS + '';
                        $scope.dataToInsert.LESSINTERIMPAYMENTS = $scope.dataToInsert.LESSINTERIMPAYMENTS + '';
                        $scope.dataToInsert.LESSEXCESS = $scope.dataToInsert.LESSEXCESS + '';
                        $scope.dataToInsert.LESSREINSTATEMENTPREMIUM = $scope.dataToInsert.LESSREINSTATEMENTPREMIUM + '';
                        $scope.dataToInsert.AMOUNTPAYABLE = $scope.dataToInsert.AMOUNTPAYABLE +'';
                        input.BAPASSESSMENTDATA = $scope.dataToInsert;
                        break;
                    case 'PNSB' : 
                        $scope.dataToInsert.LABOURCHARGES = $scope.dataToInsert.LABOURCHARGES+'';
                        $scope.dataToInsert.MATERIALCOST = $scope.dataToInsert.MATERIALCOST+'';
                        $scope.dataToInsert.TRANSPORTATIONCOST = $scope.dataToInsert.TRANSPORTATIONCOST+'';
                        $scope.dataToInsert.GROSSASSESSEDLOSS = $scope.dataToInsert.GROSSASSESSEDLOSS+'';
                        $scope.dataToInsert.SGST = $scope.dataToInsert.SGST+'';
                        $scope.dataToInsert.CGST = $scope.dataToInsert.CGST+'';
                        $scope.dataToInsert.IGST = $scope.dataToInsert.IGST+'';
                        $scope.dataToInsert.LESSDEPRECIATION = $scope.dataToInsert.LESSDEPRECIATION+'';
                        $scope.dataToInsert.LESSSALVAGE = $scope.dataToInsert.LESSSALVAGE+'';
                        $scope.dataToInsert.UNDERINSURANCE = $scope.dataToInsert.UNDERINSURANCE+'';
                        $scope.dataToInsert.ADJUSTMENTS = $scope.dataToInsert.ADJUSTMENTS+'';
                        $scope.dataToInsert.NETASSESSEDLOSS = $scope.dataToInsert.NETASSESSEDLOSS+'';
                        $scope.dataToInsert.LESSINTERIMPAYMENTS = $scope.dataToInsert.LESSINTERIMPAYMENTS+'';
                        $scope.dataToInsert.LESSEXCESS = $scope.dataToInsert.LESSEXCESS+'';
                        $scope.dataToInsert.LESSREINSTATEMENTPREMIUM = $scope.dataToInsert.LESSREINSTATEMENTPREMIUM+'';
                        $scope.dataToInsert.AMOUNTPAYABLE = $scope.dataToInsert.AMOUNTPAYABLE+'';
                        input.PNSBASSESSMENTDATA =  $scope.dataToInsert;
                        break;
                    case 'EEI' :
                        $scope.dataToInsert.EEILOSS = $scope.dataToInsert.EEILOSS+'';
                        $scope.dataToInsert.EXTERNALDATAMEDIA = $scope.dataToInsert.EXTERNALDATAMEDIA+'';
                        $scope.dataToInsert.ICWINCREASECOSTOFWORKING = $scope.dataToInsert.ICWINCREASECOSTOFWORKING+'';
                        $scope.dataToInsert.EXPRESSFREIGHT = $scope.dataToInsert.EXPRESSFREIGHT+'';
                        $scope.dataToInsert.OWNERSSURROUNDING = $scope.dataToInsert.OWNERSSURROUNDING+'';
                        $scope.dataToInsert.EQUIPMENT = $scope.dataToInsert.EQUIPMENT+'';
                        $scope.dataToInsert.THIRDPARTY = $scope.dataToInsert.THIRDPARTY+'';
                        $scope.dataToInsert.ADDLCUSTOMDUTY = $scope.dataToInsert.ADDLCUSTOMDUTY+'';
                        $scope.dataToInsert.GROSSASSESSEDLOSS = $scope.dataToInsert.GROSSASSESSEDLOSS+'';
                        $scope.dataToInsert.SGST = $scope.dataToInsert.SGST+'';
                        $scope.dataToInsert.CGST = $scope.dataToInsert.CGST+'';
                        $scope.dataToInsert.IGST = $scope.dataToInsert.IGST+'';
                        $scope.dataToInsert.IMPROVEMENTPENCENT = $scope.dataToInsert.IMPROVEMENTPENCENT+'';
                        $scope.dataToInsert.LESSIMPROVEMENTS = $scope.dataToInsert.LESSIMPROVEMENTS+'';
                        $scope.dataToInsert.LESSDEPRECIATION = $scope.dataToInsert.LESSDEPRECIATION+'';
                        $scope.dataToInsert.LESSSALVAGE = $scope.dataToInsert.LESSSALVAGE+'';
                        $scope.dataToInsert.UNDERINSURANCE = $scope.dataToInsert.UNDERINSURANCE+'';
                        $scope.dataToInsert.ADJUSTMENTS = $scope.dataToInsert.ADJUSTMENTS+'';
                        $scope.dataToInsert.NETASSESSEDLOSS = $scope.dataToInsert.NETASSESSEDLOSS+'';
                        $scope.dataToInsert.LESSINTERIMPAYMENTS = $scope.dataToInsert.LESSINTERIMPAYMENTS+'';
                        $scope.dataToInsert.LESSEXCESS = $scope.dataToInsert.LESSEXCESS+'';
                        $scope.dataToInsert.LESSREINSTATEMENTPREMIUM = $scope.dataToInsert.LESSREINSTATEMENTPREMIUM+'';
                        $scope.dataToInsert.AMOUNTPAYABLE = $scope.dataToInsert.AMOUNTPAYABLE+'';
                        input.EEIASSESSMENTDATA =  $scope.dataToInsert; 
                        break;
                    case 'MBD' : 
                        $scope.dataToInsert.MBLOSS = $scope.dataToInsert.MBLOSS+'';
                        $scope.dataToInsert.EXPRESSFREIGHT = $scope.dataToInsert.EXPRESSFREIGHT+'';
                        $scope.dataToInsert.OWNERSSURROUNDING = $scope.dataToInsert.OWNERSSURROUNDING+'';
                        $scope.dataToInsert.EQUIPMENT = $scope.dataToInsert.EQUIPMENT+'';
                        $scope.dataToInsert.THIRDPARTY = $scope.dataToInsert.THIRDPARTY+'';
                        $scope.dataToInsert.ADDLCUSTOMDUTY = $scope.dataToInsert.ADDLCUSTOMDUTY+'';
                        $scope.dataToInsert.GROSSASSESSEDLOSS = $scope.dataToInsert.GROSSASSESSEDLOSS+'';
                        $scope.dataToInsert.SGST = $scope.dataToInsert.SGST+'';
                        $scope.dataToInsert.CGST = $scope.dataToInsert.CGST+'';
                        $scope.dataToInsert.IGST = $scope.dataToInsert.IGST+'';
                        $scope.dataToInsert.IMPROVEMENTPENCENT = $scope.dataToInsert.IMPROVEMENTPENCENT+'';
                        $scope.dataToInsert.LESSIMPROVEMENTS = $scope.dataToInsert.LESSIMPROVEMENTS+'';
                        $scope.dataToInsert.LESSDEPRECIATION = $scope.dataToInsert.LESSDEPRECIATION+'';
                        $scope.dataToInsert.LESSSALVAGE = $scope.dataToInsert.LESSSALVAGE+'';
                        $scope.dataToInsert.UNDERINSURANCE = $scope.dataToInsert.UNDERINSURANCE+'';
                        $scope.dataToInsert.ADJUSTMENTS = $scope.dataToInsert.ADJUSTMENTS+'';
                        $scope.dataToInsert.NETASSESSEDLOSS = $scope.dataToInsert.NETASSESSEDLOSS+'';
                        $scope.dataToInsert.LESSINTERIMPAYMENTS = $scope.dataToInsert.LESSINTERIMPAYMENTS+'';
                        $scope.dataToInsert.LESSEXCESS = $scope.dataToInsert.LESSEXCESS+'';
                        $scope.dataToInsert.LESSREINSTATEMENTPREMIUM = $scope.dataToInsert.LESSREINSTATEMENTPREMIUM+'';
                        $scope.dataToInsert.AMOUNTPAYABLE = $scope.dataToInsert.AMOUNTPAYABLE+'';
                        input.MBDASSESSMENTDATA =  $scope.dataToInsert;
                        break;
                    case 'MT' :
                        $scope.dataToInsert.GROSSASSESSEDLOSS = $scope.dataToInsert.GROSSASSESSEDLOSS+'';
                        $scope.dataToInsert.UNDERINSURANCE = $scope.dataToInsert.UNDERINSURANCE+'';
                        $scope.dataToInsert.LESSOTHERDEDUCTIONS = $scope.dataToInsert.LESSOTHERDEDUCTIONS+'';
                        $scope.dataToInsert.NETASSESSEDLOSS = $scope.dataToInsert.NETASSESSEDLOSS+'';
                        $scope.dataToInsert.LESSINTERIMPAYMENTS = $scope.dataToInsert.LESSINTERIMPAYMENTS+'';
                        $scope.dataToInsert.LESSREINSTATEMENTPREMIUM = $scope.dataToInsert.LESSREINSTATEMENTPREMIUM+'';
                        $scope.dataToInsert.LESSEXCESS = $scope.dataToInsert.LESSEXCESS+'';
                        $scope.dataToInsert.AMOUNTPAYABLE = $scope.dataToInsert.AMOUNTPAYABLE+'';
                        input.MTASSESSMENTDATA =  $scope.dataToInsert; 
                        break;
                    case 'BURGLARY' :
                        $scope.dataToInsert.BUILDING = $scope.dataToInsert.BUILDING+'';
                        $scope.dataToInsert.PANDM = $scope.dataToInsert.PANDM+'';
                        $scope.dataToInsert.STOCK = $scope.dataToInsert.STOCK+'';
                        $scope.dataToInsert.OTHERCOST = $scope.dataToInsert.OTHERCOST+'';
                        $scope.dataToInsert.GROSSASSESSEDLOSS = $scope.dataToInsert.GROSSASSESSEDLOSS+'';
                        $scope.dataToInsert.SGST = $scope.dataToInsert.SGST+'';
                        $scope.dataToInsert.CGST = $scope.dataToInsert.CGST+'';
                        $scope.dataToInsert.IGST = $scope.dataToInsert.IGST+'';
                        $scope.dataToInsert.LESSDEPRECIATION = $scope.dataToInsert.LESSDEPRECIATION+'';
                        $scope.dataToInsert.LESSSALVAGE = $scope.dataToInsert.LESSSALVAGE+'';
                        $scope.dataToInsert.UNDERINSURANCE = $scope.dataToInsert.UNDERINSURANCE+'';
                        $scope.dataToInsert.ADJUSTMENTS = $scope.dataToInsert.ADJUSTMENTS+'';
                        $scope.dataToInsert.NETASSESSEDLOSS = $scope.dataToInsert.NETASSESSEDLOSS+'';
                        $scope.dataToInsert.LESSINTERIMPAYMENTS = $scope.dataToInsert.LESSINTERIMPAYMENTS+'';
                        $scope.dataToInsert.LESSEXCESS = $scope.dataToInsert.LESSEXCESS+'';
                        $scope.dataToInsert.LESSREINSTATEMENTPREMIUM = $scope.dataToInsert.LESSREINSTATEMENTPREMIUM+'';
                        $scope.dataToInsert.AMOUNTPAYABLE = $scope.dataToInsert.AMOUNTPAYABLE+'';
                        input.BURGLARYASSESSMENTDATA =  $scope.dataToInsert; 
                        break;
                    case 'BAGGAGE' :
                        $scope.dataToInsert.GROSSASSESSEDLOSS = $scope.dataToInsert.GROSSASSESSEDLOSS+'';
                        $scope.dataToInsert.SGST = $scope.dataToInsert.SGST+'';
                        $scope.dataToInsert.CGST = $scope.dataToInsert.CGST+'';
                        $scope.dataToInsert.IGST = $scope.dataToInsert.IGST+'';
                        $scope.dataToInsert.LESSRECOVERY = $scope.dataToInsert.LESSRECOVERY+'';
                        $scope.dataToInsert.NETASSESSEDLOSS = $scope.dataToInsert.NETASSESSEDLOSS+'';
                        $scope.dataToInsert.LESSINTERIMPAYMENTS = $scope.dataToInsert.LESSINTERIMPAYMENTS+'';
                        $scope.dataToInsert.LESSEXCESS = $scope.dataToInsert.LESSEXCESS+'';
                        $scope.dataToInsert.LESSREINSTATEMENTPREMIUM = $scope.dataToInsert.LESSREINSTATEMENTPREMIUM+'';
                        $scope.dataToInsert.AMOUNTPAYABLE = $scope.dataToInsert.AMOUNTPAYABLE+'';
                        input.BAGGAGEASSESSMENTDATA =  $scope.dataToInsert; 
                        break;
                    case 'BPP' :
                        $scope.dataToInsert.BOILERLOSS = $scope.dataToInsert.BOILERLOSS+'';
                        $scope.dataToInsert.EXPRESSFREIGHT = $scope.dataToInsert.EXPRESSFREIGHT+'';
                        $scope.dataToInsert.OWNERSSURROUNDING = $scope.dataToInsert.OWNERSSURROUNDING+'';
                        $scope.dataToInsert.THIRDPARTY = $scope.dataToInsert.THIRDPARTY+'';
                        $scope.dataToInsert.ADDLCUSTOMDUTY = $scope.dataToInsert.ADDLCUSTOMDUTY+'';
                        $scope.dataToInsert.GROSSASSESSEDLOSS = $scope.dataToInsert.GROSSASSESSEDLOSS+'';
                        $scope.dataToInsert.SGST = $scope.dataToInsert.SGST+'';
                        $scope.dataToInsert.CGST = $scope.dataToInsert.CGST+'';
                        $scope.dataToInsert.IGST = $scope.dataToInsert.IGST+'';
                        $scope.dataToInsert.IMPROVEMENTPENCENT = $scope.dataToInsert.IMPROVEMENTPENCENT+'';
                        $scope.dataToInsert.LESSIMPROVEMENTS = $scope.dataToInsert.LESSIMPROVEMENTS+'';
                        $scope.dataToInsert.LESSDEPRECIATION = $scope.dataToInsert.LESSDEPRECIATION+'';
                        $scope.dataToInsert.LESSSALVAGE = $scope.dataToInsert.LESSSALVAGE+'';
                        $scope.dataToInsert.UNDERINSURANCE = $scope.dataToInsert.UNDERINSURANCE+'';
                        $scope.dataToInsert.ADJUSTMENTS = $scope.dataToInsert.ADJUSTMENTS+'';
                        $scope.dataToInsert.NETASSESSEDLOSS = $scope.dataToInsert.NETASSESSEDLOSS+'';
                        $scope.dataToInsert.LESSINTERIMPAYMENTS = $scope.dataToInsert.LESSINTERIMPAYMENTS+'';
                        $scope.dataToInsert.LESSEXCESS = $scope.dataToInsert.LESSEXCESS+'';
                        $scope.dataToInsert.LESSREINSTATEMENTPREMIUM = $scope.dataToInsert.LESSREINSTATEMENTPREMIUM+'';
                        $scope.dataToInsert.AMOUNTPAYABLE = $scope.dataToInsert.AMOUNTPAYABLE+'';
                        input.BPPASSESSMENTDATA =  $scope.dataToInsert; 
                        break;
                    case 'FG' : 
                        $scope.dataToInsert.NUMBEROFEMPLOYEES = $scope.dataToInsert.NUMBEROFEMPLOYEES+'';
                        $scope.dataToInsert.SIPERPERSON = $scope.dataToInsert.SIPERPERSON+'';
                        $scope.dataToInsert.AOALIMIT = $scope.dataToInsert.AOALIMIT+'';
                        $scope.dataToInsert.GROSSASSESSEDLOSS = $scope.dataToInsert.GROSSASSESSEDLOSS+'';
                        $scope.dataToInsert.LESSRECOVERY = $scope.dataToInsert.LESSRECOVERY+'';
                        $scope.dataToInsert.LESSEXCESS = $scope.dataToInsert.LESSEXCESS+'';
                        $scope.dataToInsert.LESSOTHERDEDUCTION = $scope.dataToInsert.LESSOTHERDEDUCTION+'';
                        $scope.dataToInsert.NETASSESSEDLOSS = $scope.dataToInsert.NETASSESSEDLOSS+'';
                        $scope.dataToInsert.LESSREINSTATEMENTPREMIUM = $scope.dataToInsert.LESSREINSTATEMENTPREMIUM+'';
                        $scope.dataToInsert.LESSINTERIMPAYMENTS = $scope.dataToInsert.LESSINTERIMPAYMENTS+'';
                        $scope.dataToInsert.AMOUNTPAYABLE = $scope.dataToInsert.AMOUNTPAYABLE+'';
                        input.FGASSESSMENTDATA = $scope.dataToInsert;
                        break;
                    case 'FLOP' :
                        $scope.dataToInsert.GROSSPROFIT = $scope.dataToInsert.GROSSPROFIT +'';
                        $scope.dataToInsert.GPRATIO = $scope.dataToInsert.GPRATIO +'';
                        $scope.dataToInsert.LOSSDUETOTURNOVER = $scope.dataToInsert.LOSSDUETOTURNOVER +'';
                        $scope.dataToInsert.INCREASECOSTOFWORKING = $scope.dataToInsert.INCREASECOSTOFWORKING +'';
                        $scope.dataToInsert.AUDITORSFEES = $scope.dataToInsert.AUDITORSFEES +'';
                        $scope.dataToInsert.PROFESSIONALFEES = $scope.dataToInsert.PROFESSIONALFEES +'';
                        $scope.dataToInsert.STANDINGCHARGES = $scope.dataToInsert.STANDINGCHARGES +'';
                        $scope.dataToInsert.GROSSASSESSEDLOSS = $scope.dataToInsert.GROSSASSESSEDLOSS +'';
                        $scope.dataToInsert.UNDERINSURANCE = $scope.dataToInsert.UNDERINSURANCE +'';
                        $scope.dataToInsert.LOSSBEFOREEXCESS = $scope.dataToInsert.LOSSBEFOREEXCESS +'';
                        $scope.dataToInsert.LESSEXCESS = $scope.dataToInsert.LESSEXCESS +'';
                        $scope.dataToInsert.ADJUSTMENTS = $scope.dataToInsert.ADJUSTMENTS +'';
                        $scope.dataToInsert.NETASSESSEDLOSS = $scope.dataToInsert.NETASSESSEDLOSS +'';
                        $scope.dataToInsert.LESSINTERIMPAYMENTS = $scope.dataToInsert.LESSINTERIMPAYMENTS +'';
                        $scope.dataToInsert.LESSREINSTATEMENTPREMIUM = $scope.dataToInsert.LESSREINSTATEMENTPREMIUM +'';
                        $scope.dataToInsert.AMOUNTPAYABLE = $scope.dataToInsert.AMOUNTPAYABLE +'';
                        input.FLOPASSESSMENTDATA = $scope.dataToInsert;
                        break;
                    case 'PL' : 
                        $scope.dataToInsert.DEFENCECOST = $scope.dataToInsert.DEFENCECOST + '';
                        $scope.dataToInsert.MEDICALEXPENSES = $scope.dataToInsert.MEDICALEXPENSES + '';
                        $scope.dataToInsert.DAMAGES = $scope.dataToInsert.DAMAGES + '';
                        $scope.dataToInsert.OTHERCOST = $scope.dataToInsert.OTHERCOST + '';
                        $scope.dataToInsert.NETASSESSEDLOSS = $scope.dataToInsert.NETASSESSEDLOSS + '';
                        $scope.dataToInsert.LESSREINSTATEMENTPREMIUM = $scope.dataToInsert.LESSREINSTATEMENTPREMIUM + '';
                        $scope.dataToInsert.LESSEXCESS = $scope.dataToInsert.LESSEXCESS + '';
                        $scope.dataToInsert.AMOUNTPAYABLE = $scope.dataToInsert.AMOUNTPAYABLE + '';
                        input.PLASSESSMENTDATA = $scope.dataToInsert;
                        break;
                    case 'FIRE' :
                        $scope.dataToInsert.BUILDINGGROSSASSESSEDLOSS = $scope.dataToInsert.BUILDINGGROSSASSESSEDLOSS + '';
                        $scope.dataToInsert.BUILDINGLESSDEPRECIATION = $scope.dataToInsert.BUILDINGLESSDEPRECIATION + '';
                        $scope.dataToInsert.BUILDINGLESSSALVAGE = $scope.dataToInsert.BUILDINGLESSSALVAGE + '';
                        $scope.dataToInsert.BUILDINGUNDERINSURANCE = $scope.dataToInsert.BUILDINGUNDERINSURANCE + '';
                        $scope.dataToInsert.BUILDINGNETASSESSEDLOSS = $scope.dataToInsert.BUILDINGNETASSESSEDLOSS + '';
                        $scope.dataToInsert.STOCKGROSSASSESSEDLOSS = $scope.dataToInsert.STOCKGROSSASSESSEDLOSS + '';
                        $scope.dataToInsert.STOCKLESSDEPRECIATION = $scope.dataToInsert.STOCKLESSDEPRECIATION + '';
                        $scope.dataToInsert.STOCKLESSSALVAGE = $scope.dataToInsert.STOCKLESSSALVAGE + '';
                        $scope.dataToInsert.STOCKUNDERINSURANCE = $scope.dataToInsert.STOCKUNDERINSURANCE + '';
                        $scope.dataToInsert.STOCKNETASSESSEDLOSS = $scope.dataToInsert.STOCKNETASSESSEDLOSS + '';
                        $scope.dataToInsert.PLANTMACHINERYGROSSASSESSEDLOSS = $scope.dataToInsert.PLANTMACHINERYGROSSASSESSEDLOSS + '';
                        $scope.dataToInsert.PLANTMACHINERYLESSDEPRECIATION = $scope.dataToInsert.PLANTMACHINERYLESSDEPRECIATION + '';
                        $scope.dataToInsert.PLANTMACHINERYLESSSALVAGE = $scope.dataToInsert.PLANTMACHINERYLESSSALVAGE + '';
                        $scope.dataToInsert.PLANTMACHINERYUNDERINSURANCE = $scope.dataToInsert.PLANTMACHINERYUNDERINSURANCE + '';
                        $scope.dataToInsert.PLANTMACHINERYNETASSESSEDLOSS = $scope.dataToInsert.PLANTMACHINERYNETASSESSEDLOSS + '';
                        $scope.dataToInsert.PLINTHFOUNDGROSSASSESSEDLOSS = $scope.dataToInsert.PLINTHFOUNDGROSSASSESSEDLOSS + '';
                        $scope.dataToInsert.PLINTHFOUNDLESSDEPRECIATION = $scope.dataToInsert.PLINTHFOUNDLESSDEPRECIATION + '';
                        $scope.dataToInsert.PLINTHFOUNDLESSSALVAGE = $scope.dataToInsert.PLINTHFOUNDLESSSALVAGE + '';
                        $scope.dataToInsert.PLINTHFOUNDUNDERINSURANCE = $scope.dataToInsert.PLINTHFOUNDUNDERINSURANCE + '';
                        $scope.dataToInsert.PLINTHFOUNDNETASSESSEDLOSS = $scope.dataToInsert.PLINTHFOUNDNETASSESSEDLOSS + '';
                        $scope.dataToInsert.COMPOUNDWALLGROSSASSESSEDLOSS = $scope.dataToInsert.COMPOUNDWALLGROSSASSESSEDLOSS + '';
                        $scope.dataToInsert.COMPOUNDWALLLESSDEPRECIATION = $scope.dataToInsert.COMPOUNDWALLLESSDEPRECIATION + '';
                        $scope.dataToInsert.COMPOUNDWALLLESSSALVAGE = $scope.dataToInsert.COMPOUNDWALLLESSSALVAGE + '';
                        $scope.dataToInsert.COMPOUNDWALLUNDERINSURANCE = $scope.dataToInsert.COMPOUNDWALLUNDERINSURANCE + '';
                        $scope.dataToInsert.COMPOUNDWALLNETASSESSEDLOSS = $scope.dataToInsert.COMPOUNDWALLNETASSESSEDLOSS + '';
                        $scope.dataToInsert.RAWMATGROSSASSESSEDLOSS = $scope.dataToInsert.RAWMATGROSSASSESSEDLOSS + '';
                        $scope.dataToInsert.RAWMATLESSDEPRECIATION = $scope.dataToInsert.RAWMATLESSDEPRECIATION + '';
                        $scope.dataToInsert.RAWMATLESSSALVAGE = $scope.dataToInsert.RAWMATLESSSALVAGE + '';
                        $scope.dataToInsert.RAWMATUNDERINSURANCE = $scope.dataToInsert.RAWMATUNDERINSURANCE + '';
                        $scope.dataToInsert.RAWMATNETASSESSEDLOSS = $scope.dataToInsert.RAWMATNETASSESSEDLOSS + '';
                        $scope.dataToInsert.WORKINPROGROSSASSESSEDLOSS = $scope.dataToInsert.WORKINPROGROSSASSESSEDLOSS + '';
                        $scope.dataToInsert.WORKINPROLESSDEPRECIATION = $scope.dataToInsert.WORKINPROLESSDEPRECIATION + '';
                        $scope.dataToInsert.WORKINPROLESSSALVAGE = $scope.dataToInsert.WORKINPROLESSSALVAGE + '';
                        $scope.dataToInsert.WORKINPROUNDERINSURANCE = $scope.dataToInsert.WORKINPROUNDERINSURANCE + '';
                        $scope.dataToInsert.WORKINPRONETASSESSEDLOSS = $scope.dataToInsert.WORKINPRONETASSESSEDLOSS + '';
                        $scope.dataToInsert.FINISHSTOCKGROSSASSESSEDLOSS = $scope.dataToInsert.FINISHSTOCKGROSSASSESSEDLOSS + '';
                        $scope.dataToInsert.FINISHSTOCKLESSDEPRECIATION = $scope.dataToInsert.FINISHSTOCKLESSDEPRECIATION + '';
                        $scope.dataToInsert.FINISHSTOCKLESSSALVAGE = $scope.dataToInsert.FINISHSTOCKLESSSALVAGE + '';
                        $scope.dataToInsert.FINISHSTOCKUNDERINSURANCE = $scope.dataToInsert.FINISHSTOCKUNDERINSURANCE + '';
                        $scope.dataToInsert.FINISHSTOCKNETASSESSEDLOSS = $scope.dataToInsert.FINISHSTOCKNETASSESSEDLOSS + '';
                        $scope.dataToInsert.FFGROSSASSESSEDLOSS = $scope.dataToInsert.FFGROSSASSESSEDLOSS + '';
                        $scope.dataToInsert.FFLESSDEPRECIATION = $scope.dataToInsert.FFLESSDEPRECIATION + '';
                        $scope.dataToInsert.FFLESSSALVAGE = $scope.dataToInsert.FFLESSSALVAGE + '';
                        $scope.dataToInsert.FFUNDERINSURANCE = $scope.dataToInsert.FFUNDERINSURANCE + '';
                        $scope.dataToInsert.FFNETASSESSEDLOSS = $scope.dataToInsert.FFNETASSESSEDLOSS + '';
                        $scope.dataToInsert.ELECTRICALFITGROSSASSESSEDLOSS = $scope.dataToInsert.ELECTRICALFITGROSSASSESSEDLOSS + '';
                        $scope.dataToInsert.ELECTRICALFITLESSDEPRECIATION = $scope.dataToInsert.ELECTRICALFITLESSDEPRECIATION + '';
                        $scope.dataToInsert.ELECTRICALFITLESSSALVAGE = $scope.dataToInsert.ELECTRICALFITLESSSALVAGE + '';
                        $scope.dataToInsert.ELECTRICALFITUNDERINSURANCE = $scope.dataToInsert.ELECTRICALFITUNDERINSURANCE + '';
                        $scope.dataToInsert.ELECTRICALFITNETASSESSEDLOSS = $scope.dataToInsert.ELECTRICALFITNETASSESSEDLOSS + '';
                        $scope.dataToInsert.BRISKGROSSASSESSEDLOSS = $scope.dataToInsert.BRISKGROSSASSESSEDLOSS + '';
                        $scope.dataToInsert.BRISKLESSDEPRECIATION = $scope.dataToInsert.BRISKLESSDEPRECIATION + '';
                        $scope.dataToInsert.BRISKLESSSALVAGE = $scope.dataToInsert.BRISKLESSSALVAGE + '';
                        $scope.dataToInsert.BRISKUNDERINSURANCE = $scope.dataToInsert.BRISKUNDERINSURANCE + '';
                        $scope.dataToInsert.BRISKNETASSESSEDLOSS = $scope.dataToInsert.BRISKNETASSESSEDLOSS + '';
                        $scope.dataToInsert.SPECIFIEDGROSSASSESSEDLOSS = $scope.dataToInsert.SPECIFIEDGROSSASSESSEDLOSS + '';
                        $scope.dataToInsert.SPECIFIEDLESSDEPRECIATION = $scope.dataToInsert.SPECIFIEDLESSDEPRECIATION + '';
                        $scope.dataToInsert.SPECIFIEDLESSSALVAGE = $scope.dataToInsert.SPECIFIEDLESSSALVAGE + '';
                        $scope.dataToInsert.SPECIFIEDUNDERINSURANCE = $scope.dataToInsert.SPECIFIEDUNDERINSURANCE + '';
                        $scope.dataToInsert.SPECIFIEDNETASSESSEDLOSS = $scope.dataToInsert.SPECIFIEDNETASSESSEDLOSS + '';
                        $scope.dataToInsert.TAXRATE = $scope.dataToInsert.TAXRATE + '';
                        $scope.dataToInsert.SGST = $scope.dataToInsert.SGST + '';
                        $scope.dataToInsert.CGST = $scope.dataToInsert.CGST + '';
                        $scope.dataToInsert.IGST = $scope.dataToInsert.IGST + '';
                        $scope.dataToInsert.TOTALNETASSESSEDLOSS = $scope.dataToInsert.TOTALNETASSESSEDLOSS + '';
                        $scope.dataToInsert.LESSEXCESS = $scope.dataToInsert.LESSEXCESS + '';
                        $scope.dataToInsert.LESSOTHER = $scope.dataToInsert.LESSOTHER + '';
                        $scope.dataToInsert.LESSRIPREMIUM = $scope.dataToInsert.LESSRIPREMIUM + '';
                        $scope.dataToInsert.LESSINTERIMPAYMENTS = $scope.dataToInsert.LESSINTERIMPAYMENTS + '';
                        $scope.dataToInsert.NETPAYABLE = $scope.dataToInsert.NETPAYABLE + '';
                        $scope.dataToInsert.SIFORDEBRIS = $scope.dataToInsert.SIFORDEBRIS + '';
                        $scope.dataToInsert.SIFORARCHITECTURAL = $scope.dataToInsert.SIFORARCHITECTURAL + '';
                        $scope.dataToInsert.ADDDEBRISREMOVAL = $scope.dataToInsert.ADDDEBRISREMOVAL + '';
                        $scope.dataToInsert.ADDARCHITECTURAL = $scope.dataToInsert.ADDARCHITECTURAL + '';
                        $scope.dataToInsert.AMOUNTPAYABLE = $scope.dataToInsert.AMOUNTPAYABLE + '';
                        input.FIREASSESSMENTDATA = $scope.dataToInsert;
                        break;


                }
                var serviceArgs = {
                    params: JSON.stringify(input),
                    load: function (data) {
                        debugger;
                        $scope.decServiceCallCounter();
                        if(!(!!assessmentID)){
                            switch(assessmentName){
                                case 'BAP' : 
                                        $scope.BAP.BAPASSESSMENTID = data.ASSESSEMENTID;
                                    break;
                                case 'PNSB' : 
                                        $scope.PNSB.PNSBASSESSMENTID = data.ASSESSEMENTID;
                                    break;
                                case 'EEI' : 
                                        $scope.EEI.EEIASSESSMENTID = data.ASSESSEMENTID;
                                    break;
                                case 'MBD' : 
                                        $scope.MBD.MBDASSESSMENTID = data.ASSESSEMENTID;
                                    break;
                                case 'MT' :
                                        $scope.MT.MTASSESSMENTID = data.ASSESSEMENTID; 
                                    break;
                                case 'BURGLARY' :
                                        $scope.BURGLARY.BURGLARYASSESSMENTID = data.ASSESSEMENTID; 
                                    break;
                                case 'BAGGAGE' : 
                                        $scope.BAGGAGE.BAGGAGEASSESSMENTID = data.ASSESSEMENTID;
                                    break;
                                case 'BPP' : 
                                        $scope.BPP.BPPASSESSMENTID = data.ASSESSEMENTID;
                                    break;
                                case 'FG' : 
                                        $scope.FG.FGASSESSMENTID = data.ASSESSEMENTID;
                                break;
                                case 'FLOP' : 
                                        $scope.FLOP.FLOPASSESSMENTID = data.ASSESSEMENTID;
                                break; 
                                case 'PL' :
                                        $scope.PL.PLASSESSMENTID = data.ASSESSEMENTID;
                                break; 
                                case 'FIRE' :
                                        $scope.FIRE.FIREASSESSMENTID = data.ASSESSEMENTID;
                                break;    
                            }
                        }
                        $scope.showSuccessMessage('Record Inserted/Updated Successfully');
                       $scope.$apply();
                    },
                    error: function (e) {
                        $scope.decServiceCallCounter();
                    }
                };
                $scope.incServiceCallCounter();
                $scope.context.options.saveAssessment(serviceArgs);

            }

            $scope.fraudData =[];
            $scope.addRow = function () {
                $scope.fraudData.push({
                    'TYPE': 'Named',
                    'NAMEOFEMPLOYEE':'',
                    'TYPEOFEMPLOYMENT': 'Permanent',
                    'FRAUDCOMMITED': 0
                });

            };
            $scope.calculateFruadAmount = function(){
                var amount=0;
                $scope.NUMBEROFEMPLOYEES = $scope.fraudData.length;
                for(var i=0;i<$scope.fraudData.length;i++){
                    amount+=$scope.fraudData[i].FRAUDCOMMITED;
                }
                $scope.FG.GROSSASSESSEDLOSS = amount;
                $scope.FG.NUMBEROFEMPLOYEES = $scope.fraudData.length;

            }


            var formatDateToDatePicker = function (date) {
                if (!date) {
                    return null;
                }
                var d = new Date(date),
                    month = "" + (d.getMonth() + 1),
                    day = "" + d.getDate(),
                    year = d.getFullYear();

                if (month.length < 2) month = "0" + month;
                if (day.length < 2) day = "0" + day;

                return [day, month, year].join("-");
            };

            // mm/dd/yyyy to yyyy-mm-dd
            var makeSQLServerCompExcel = function (date) {
                if (!date) return null;
                else {
                    var pos = date.indexOf("/");
                    if (pos == 2) {
                        var res1 = date.split("/").reverse();
                        var res = res1[0] + "-" + date.split("/", 2).join("-");
                        return res;
                    } else {
                        return date;
                    }
                }
            };

            // dd-mm-yyyy to yyyy-mm-dd
            var makeSQLServerComp = function (date) {
                if (!date) return null;
                else {
                    var pos = date.indexOf("-");
                    if (pos == 2) {
                        return date
                            .split("-")
                            .reverse()
                            .join("-");
                    } else {
                        return date;
                    }
                }
            };

            //opposite
            var makePickerComp = function (date) {
                if (!date) {
                    return null;
                } else {
                    var pos = date.indexOf("-");
                    if (pos != 2) {
                        return date
                            .split("-")
                            .reverse()
                            .join("-");
                    } else {
                        return date;
                    }
                }
            };
            
            $scope.OpenClaim();


            /*********************************RESERVE TAB*********************************************** */

            $scope.saveReserveData = function () {
                if (parseFloat($scope.reserveData.INDEMNITYRESERVEAMOUNT) <= 0) {
                    $scope.showWarningMessage('Indemnity amount should be greater than zero');
                    return;
                }
                if (!!$scope.reserveData.EXPENSERESERVEAMOUNT && parseFloat($scope.reserveData.EXPENSERESERVEAMOUNT) <= 0) {
                    $scope.showWarningMessage('Expense amount should be greater than zero');
                    return;
                }
                var input = {
                    "updatedReserveData": $scope.reserveData,
                    "ClaimInfo": $scope.ClaimInfo
                }
                var serviceArgs = {
                    params: JSON.stringify(input),
                    load: function (data) {
                        $scope.decServiceCallCounter();
                        $scope.GetTabData('reserve');
                    },
                    error: function (e) {
                        $scope.decServiceCallCounter();
                        $scope.showErrorMessage(e, "Error in reserve creation");
                    }
                };
                $scope.incServiceCallCounter();
                $scope.context.options.addOrUpdateReserve(serviceArgs);
            }

            $scope.getApprovalHistoryData = function (APPROVALNAME) {
                var input = {
                    "FEATURESUMMARYID": $scope.claimSummary.AIGCFEATURESUMMARYID,
                    "FEATURENUMBER": $scope.claimSummary.FEATURENUMBER,
                    "APPROVALNAME": APPROVALNAME
                }
                var serviceArgs = {
                    params: JSON.stringify(input),
                    load: function (data) {
                        switch (APPROVALNAME) {
                            case 'RESERVE':
                                if (data.ApprovalHistoryData.items.length > 0)
                                    $scope.ReserveApprovalHistoryData = data.ApprovalHistoryData.items;
                                else
                                    $scope.ReserveApprovalHistoryData = {};
                                $('#ReserveApprovalHistoryModal').modal('show');
                                break;
                            default:
                                break;
                        }
    
                        $scope.decServiceCallCounter();
                        $scope.$apply();
                    },
                    error: function (e) {
                        $scope.decServiceCallCounter();
                        $scope.showErrorMessage(e, "Error while getting reserve aprroval history data");
                    }
                };
                $scope.incServiceCallCounter();
                $scope.context.options.getApprovalHistoryData(serviceArgs);
            }

            /***********************************FOOTER DATA***************************************************/

            $scope.getFooterData = function (clickedTag) {
                var input = {
                    TAGNAME: clickedTag,
                    FEATURESUMMARYID: $scope.ClaimInfo.AIGCFEATURESUMMARYID
                }
                var serviceArgs = {
                    params: JSON.stringify(input),
                    load: function (data) {
                        switch (clickedTag) {
                            case 'TRANSACTION':
                                $scope.gctransactionData = data.FOOOTERDATA.items;
                                $('#gctransactionModal').modal('show');
                                break;

                            case 'FEATUREACTIVITY' :
                                   $scope.featureActivityList = data.FOOOTERDATA.items;
                                   $('#FeatureActivityModel').modal('show');
                                   break;
    
                        }
                        $scope.$apply();
                        $scope.decServiceCallCounter();
                    },
                    error: function (e) {
                        $scope.decServiceCallCounter();
                        $scope.showErrorMessage(e, "Error while featching " + clickedTag);
                    }
                };
                $scope.incServiceCallCounter();
                $scope.context.options.getFooterData(serviceArgs);
            }

            /**************************claim detials start******************************************/
            /**************************claim details start******************************************/
            $scope.claimDetail={};
            $scope.claimDetail.IIB={};
            $scope.claimDetail.WC={};
            $scope.claimDetail.PA={};

            var tempPlateNeonClaimDetails = undefined;
            var tempAllRiskPortableClaimDetails = undefined;
            var tempElecEquipClaimDetails = undefined;
            var tempMBDClaimDetails = undefined;
            var tempMoneyInTransitClaimDetails = undefined;
            var tempMoneyInSafeClaimDetails = undefined;
            var tempBurglaryClaimDetails = undefined;
            var tempBaggageClaimDetails = undefined;
            var tempBoilerpressClaimDetails = undefined;
            var tempFireClaimDetails = undefined;
            var tempFLOPClaimDetails = undefined;
            var tempPublicLiabilityClaimDetails = undefined;
            var tempBusinessAgriPumpClaimDetails = undefined;
            var tempWCClaimDetails = undefined;
            var tempPAClaimDetails = undefined;
            var tableName;

            $scope.TYPEOFGLASSIIBLIST=["Toughened","Tinted","Plain","Phasard"];
            $scope.CAUSEOFLOSSIIBLIST=["Accidental Damage", "Wind","Heat","Natural Calamity","Impact damage"];
            $scope.TYPEOFSETTLEMENTLIST=["Standard","Litigation","Repudiation","CWP","Compromise"];
            $scope.REASONFORPENDENCYOFCLAIMLIST=["Awaiting Supporting Bills/Invoices/Documents","Under Repair/Reinstatement","Claim under litigation","Awaiting policy Endorsement","Awaiting Survey/Investigation report","Awaiting Clarification from insured","Awaiting NOC from Bank","Awaiting EFT/KYC documents from insured"];
            $scope.FIRLODGEDIIBLIST=["Yes","No"];
            $scope.TYPEOFLOSSIIBLIST=["Repair","Replacement","Theft"];
            $scope.AMCIIBLIST=["Yes","No"];
            $scope.WITHINWARRANTYIIBLIST=["Yes","No"];
            $scope.ISTHELOSSCOVEREDUNDERWARRANTYIIBLIST=["Yes","No"];
            $scope.RETROACTIVECOVERDATEIIBLIST=["Yes","No"];
            $scope.POLICEFINALREPORTRECEIVEDIIBLIST=["Yes","No"];
            $scope.MODEOFTRANSPORTIIBLIST=["Four wheeler","Two wheeler","Public transport"];
            $scope.TYPEOFCASHIIBLIST=["Business cash","Others"];
            $scope.CASHSTOLENFROMIIBLIST=["Till","Counter","Safe"];
            $scope.BASISOFSIIIBLIST=["Total Value","First loss basis"];
            $scope.CUSTODYOFBAGGAGEIIBLIST=["Carrier","Hotel","Self"];
            $scope.BASISOFINSURANCEIIBLIST=["Turnover","Output"];
            $scope.FIRLODGEDIIBLIST=["Yes","No"];
            $scope.PUMPIIBLIST=["Yes","No"];
            $scope.AGEWCLIST=["16","17","18","19"];
            $scope.GENDERWCLIST=["Male","Female"];
            $scope.OCCUPATIONOFEMPLOYMENTWCLIST=["Electrician","Mason","Plumber","Machine operator","Welder","Traveller","Casual Labour","Blacksmith","Fitter","Others"];
            $scope.TYPEOFEMPLOYMENTWCLIST=["Employee","Contractor Employee","Subcontractor Employee"];
            $scope.TYPEOFHOSPITALWCLIST=["Government","Private","Government Medical College","Private Medical College","Taluka Hospital","Primary Health Centre"];
            $scope.INJURYTYPEWCLIST=["Grievous","Non Grievous","Fatal"];
            $scope.BENEFITSWCLIST=["Death","PPD","PTD","TTD"];
            $scope.TYPEOFINJURYWCLIST=["Fracture","Burn","Amputation","Head injury","Abrasion","Open wound"];
            $scope.TRANSACTIONTYPEWCLIST=["Partial Payment","Final Payment","Payment after closure"];

            $scope.TYPEOFCLAIMPALIST=["Accident","Reimbursement"];
            $scope.NATUREOFBENEFITPALIST=["Death/PTD","Dismemberment","TTD"];
            $scope.HOSPITALIZATIONTYPEPALIST=["Government","Private","ICU","Non ICU"];
            $scope.NATUREOFACCIDENTPALIST=["Road Traffic Accident by Four Wheeler / Car","Road Traffic Accident by Two Wheeler / Motor Cycle","Road Traffic Accident by Public Transport / Commercial Carrier","Rail Accident","Accident took place at Home","Accident took place at work place","Drowning Case","Murder / Assault / Attack","Whilst engaged in any leisure / sports activity","Snake Bite","Other"];
            $scope.FIRMLCPALIST=["Yes","No"];
            $scope.REASONOFREDUCTIONOFCLAIMSPALIST=["Closing of Claims by the Insurer","False/Fradulent Claim","Outside scope of Cover - Other reasons","Pre - existing disease  not covered","Sum Insured exhausted","Supression of Material Information","Waiting Period","Withdrawl by Insured"];
            $scope.RELATIONSHIPPALIST=[""];
            $scope.PARTAFFECTEDPALIST=["Life Both Hands or Both Feet","Sight of Both Eyes","One Hand and One Foot","Either Hand or Foot and Sight of One Eye","Speech and Hearing in Both Ears","Either Hand or Foot","Sight of One Eye","Speech", "Hearing in Both Ears","Thumb and Index Finger of Same Hand"];
            $scope.CAUSEOFLOSSIIBLIST=["Aircraft Damage","Riot, Strike and Malicious Damage","Storm, Cyclone, Typhoon, Tempest, Hurricane, Tornado, Flood and Inundation", "Impact Damage","Subsidence and Landslide including Rock slide","Bursting and/or overflowing of Water Tanks, Apparatus and Pipes","Missile Testing operations","Leakage from Automatic Sprinkler Installations","Bush Fire","Forest Fire","Spontaneous Combustion","Spoilage Material Damage","Leakage and Contamination","Deterioration of Stock in Cold Storage - Accident Power Failure","Deterioration of Stock in Cold Storage - Changes in Temperature","Molten Material Damage","Earthquake (Fire and Shock)","Terrorism Damage"];


            $scope.PrevOrNextTabSelection = function (clickedButton) {
                if (clickedButton == 'next') {
                    $('.next-tab > .active').next('li').find('a').trigger('click');
                } else if (clickedButton == 'prev') {
                    $('.next-tab > .active').prev('li').find('a').trigger('click');
                }
            }


            var modifyDate = function (date) {
                if (date != undefined && date != "") {
                    if (date.indexOf('-') > 2)
                    {
                        var temp = date.substring(0,date.indexOf('T'));
                        return temp.split('-').reverse().join('-');
                    }
                    else
                        return date.split('-').reverse().join('-');
                }
                else
                    return null;
            }

            $scope.$evalAsync(function () {

                $scope.saveClaimDetails = function (clickedButton,formName) {
                    debugger;
    
                    if($scope.claimSummary.TYPEOFCLAIM=="RESTOFAIGC")
                    {
                       // if (formName.$valid)
                      //  {                       
                            $scope.saveToClaimDetailsDB(clickedButton);
                       //     formName.submitted = false;                        
                       // }
                       // else 
                       // {
                       //    formName.submitted = true;
                        //}
                    }
                    if($scope.claimSummary.TYPEOFCLAIM=="WC")
                    {
                        if ($scope.claimDetailsWCSubmissionForm.$valid)
                        {                       
                            $scope.saveToClaimDetailsDB(clickedButton);
                            $scope.claimDetailsWCSubmissionForm.submitted = false;                        
                        }
                        else 
                        {
                            $scope.claimDetailsWCSubmissionForm.submitted = true;
                        }
                    }
                    if($scope.claimSummary.TYPEOFCLAIM=="PA")
                    {
                        if ($scope.form.claimDetailsPASubmissionForm.$valid)
                        {                       
                            $scope.saveToClaimDetailsDB(clickedButton);
                            $scope.claimDetailsPASubmissionForm.submitted = false;                        
                        }
                        else 
                        {
                            $scope.claimDetailsPASubmissionForm.submitted = true;
                        }
                    }
                }
            });

            $scope.saveToClaimDetailsDB = function (clickedButton) {

                switch($scope.claimSummary.TYPEOFCLAIM)
                {
                    case 'RESTOFAIGC':
                        switch ($scope.claimSummary.SECTION)
                        {
                            case 'Neon Sign Board':
                            case 'Plate Glass':
                            tempPlateNeonClaimDetails = angular.copy($scope.claimDetail.IIB.PlateNeon);
                            tempPlateNeonClaimDetails.FEATURESUMMARYID = $scope.ClaimInfo.FeatureSummaryId;
                            tableName="AIGCPLATEGLASSNEONCLAIMDETAILS";
                            break;
                            case 'All Risk':
                            tempAllRiskPortableClaimDetails = angular.copy($scope.claimDetail.IIB.AllRiskPortable);
                            tempAllRiskPortableClaimDetails.FEATURESUMMARYID = $scope.ClaimInfo.FeatureSummaryId;
                            tableName="AIGCALLRISKPORTABLECLAIMDETAILS";
                            break;
                            case 'EEI':
                            tempElecEquipClaimDetails = angular.copy($scope.claimDetail.IIB.ElecEquip);
                            tableName="AIGCEEICLAIMDETAILS";
                            tempElecEquipClaimDetails.FEATURESUMMARYID = $scope.ClaimInfo.FeatureSummaryId;
                            break;
                            case 'Machinery Breakdown':
                            tempMBDClaimDetails = angular.copy($scope.claimDetail.IIB.MBD);
                            tempMBDClaimDetails.FIRDATE = modifyDate(tempMBDClaimDetails.FIRDATE);
                            tempMBDClaimDetails.DATEOFCOMMENCEMENTOFFRAUD = modifyDate(tempMBDClaimDetails.DATEOFCOMMENCEMENTOFFRAUD);
                            tempMBDClaimDetails.FEATURESUMMARYID = $scope.ClaimInfo.FeatureSummaryId;
                            tableName="AIGCMBDCLAIMDETAILS";
                            break;
                            case 'Cash in Transit':
                            tempMoneyInTransitClaimDetails = angular.copy($scope.claimDetail.IIB.MoneyInTransit);
                            tableName="AIGCMONEYINTRANSITCLAIMDETAILS";
                            tempMoneyInTransitClaimDetails.FIRDATE= modifyDate(tempMoneyInTransitClaimDetails.FIRDATE);
                            tempMoneyInTransitClaimDetails.FEATURESUMMARYID = $scope.ClaimInfo.FeatureSummaryId;
                            break;
                            case 'Cash In safe safe':
                            tempMoneyInSafeClaimDetails = angular.copy($scope.claimDetail.IIB.MoneyInSafe);
                            tableName="AIGCMONEYINSAFECLAIMDETAILS";
                            tempMoneyInSafeClaimDetails.FIRDATE= modifyDate(tempMoneyInSafeClaimDetails.FIRDATE);
                            tempMoneyInTransitClaimDetails.FEATURESUMMARYID = $scope.ClaimInfo.FeatureSummaryId;
                            break;
                            case 'Burglary':
                            tempBurglaryClaimDetails = angular.copy($scope.claimDetail.IIB.Burglary);
                            tableName="AIGCBURGLARYCLAIMDETAILS";
                            tempBurglaryClaimDetails.FIRDATE= modifyDate(tempBurglaryClaimDetails.FIRDATE);
                            tempMoneyInTransitClaimDetails.FEATURESUMMARYID = $scope.ClaimInfo.FeatureSummaryId;
                            break;
                            case 'Baggage':
                            tempBaggageClaimDetails = angular.copy($scope.claimDetail.IIB.Baggage);
                            tableName="";
                            break;
                            case 'Boiler pressure plant':
                            tempBoilerpressClaimDetails = angular.copy($scope.claimDetail.IIB.Boilerpress);
                            tableName="AIGCBPPCLAIMDETAILS";
                            tempBoilerpressClaimDetails.LASTINSPECTIONDATEBYBOILERINSPECTOR= modifyDate(tempBoilerpressClaimDetails.FIRDATE);
                            tempBoilerpressClaimDetails.FEATURESUMMARYID = $scope.ClaimInfo.FeatureSummaryId;
                            break;
                            case 'Fire':
                            case 'Fire If add on taken':
                            case 'Fire if Eq opted otherwise suspense':
                            case 'Fire if terrorism cover opted else suspense':
                            tempFireClaimDetails = angular.copy($scope.claimDetail.IIB.Fire);
                            tempFireClaimDetails.DATE = modifyDate(tempFireClaimDetails.DATE);
                            tempFireClaimDetails.FEATURESUMMARYID = $scope.ClaimInfo.FeatureSummaryId;
                            tableName = "AIGCFIRECLAIMDETAILS";
                            break;
                            case 'FLOP':
                            tempFLOPClaimDetails = angular.copy($scope.claimDetail.IIB.FLOP);
                            tableName="";
                            break;
                            case 'Liability':
                            tempPublicLiabilityClaimDetails = angular.copy($scope.claimDetail.IIB.PublicLiability);
                            tableName="AIGCPUBLICLIABILITYCLAIMDETAILS";
                            tempPublicLiabilityClaimDetails.FEATURESUMMARYID = $scope.ClaimInfo.FeatureSummaryId;
                            break;
                            case 'Agri Pump Set':
                            tempBusinessAgriPumpClaimDetails = angular.copy($scope.claimDetail.IIB.BusinessAgriPump);
                            tableName="AIGCBUSINESSAGRIPUMPCLAIMDETAILS";
                            tempBusinessAgriPumpClaimDetails.FEATURESUMMARYID = $scope.ClaimInfo.FeatureSummaryId;
                            break;
                        }
                    break;
                    case 'WC':
                        tempWCClaimDetails = angular.copy($scope.claimDetail.WC);
                        tableName="";
                    break;
                    case 'PA':
                        tempPAClaimDetails = angular.copy($scope.claimDetail.PA);
                        tableName="";
                    break;
                }
                

   
                var input = {
                    "tableName": tableName,
                    "fireClaimDetailsInput": tempFireClaimDetails,
                    "plateglassNeonSignBoardInput":tempPlateNeonClaimDetails,
                    "allRiskPortableInput":tempAllRiskPortableClaimDetails,
                    "mbdInput":tempMBDClaimDetails,
                    "EEIInput":tempElecEquipClaimDetails,
                    "moneyInTransitInput":tempMoneyInTransitClaimDetails,                    
                    "moneyInSafeInput":tempMoneyInSafeClaimDetails,
                    "burglaryInput":tempBurglaryClaimDetails,                    
                    "boilerpressurePlantInput":tempBoilerpressClaimDetails,
                    "publicLiabilityInput":tempPublicLiabilityClaimDetails,                    
                    "businessAgriPumpInput":tempBusinessAgriPumpClaimDetails
    
                }
                var serviceArgs = {
                    params: JSON.stringify(input),
                    load: function (data) {

                        $scope.showSuccessMessage("Record Inserted/Updated Successfully");
                        $scope.decServiceCallCounter();
                        $scope.PrevOrNextTabSelection(clickedButton);
    
    
                    },
                    error: function (e) {
    
                        $scope.showErrorMessage(e, "Error while saving details");
                        $scope.decServiceCallCounter();
                    }
                };
                $scope.incServiceCallCounter();
                $scope.context.options.saveClaimDetails(serviceArgs);
            }

            $scope.resetClaimDetails = function (form) {
                if(form=='RESTOFAIGC')
                {
                    $scope.claimDetailsIIBSubmissionForm.$setPristine();
                    $scope.claimDetailsIIBSubmissionForm.$setUntouched();
                    $("#claimDetailsIIBForm")[0].reset();
                    $scope.claimDetail={};
                    $scope.claimDetail.IIB={};
                    $scope.claimDetail.WC={};
                    $scope.claimDetail.PA={};
                    $scope.claimDetailsIIBSubmissionForm.submitted = false;
                }
                if(form=='WC')
                {
                    $scope.claimDetailsWCSubmissionForm.$setPristine();
                    $scope.claimDetailsWCSubmissionForm.$setUntouched();
                    $("#claimDetailsWCForm")[0].reset();
                    $scope.claimDetail={};
                    $scope.claimDetail.IIB={};
                    $scope.claimDetail.WC={};
                    $scope.claimDetail.PA={};
                    $scope.claimDetailsWCSubmissionForm.submitted = false;
                }
                if(form=='PA')
                {
                    $scope.claimDetailsPASubmissionForm.$setPristine();
                    $scope.claimDetailsPASubmissionForm.$setUntouched();
                    $("#claimDetailsPAForm")[0].reset();
                    $scope.claimDetail={};
                    $scope.claimDetail.IIB={};
                    $scope.claimDetail.WC={};
                    $scope.claimDetail.PA={};
                    $scope.claimDetailsPASubmissionForm.submitted = false;
                }


            }

            $scope.displayClaimDetails = function(data,section)
            {
                var tempData = data.TABDATA.items[0].rows.items[0].indexedMap;
                switch(section)
                {
                case "MBD":
                    tempData.DATEOFCOMMENCEMENTOFFRAUD=modifyDate(tempData.DATEOFCOMMENCEMENTOFFRAUD);
                    tempData.FIRDATE=modifyDate(tempData.FIRDATE);
                    $scope.claimDetail.IIB.MBD=tempData;
                break;
                case "BAP":
                    $scope.claimDetail.IIB.BusinessAgriPump=tempData;
                break;
                case "PNSB":
                    $scope.claimDetail.IIB.PlateNeon=tempData;
                break;
                case "EEI":
                    $scope.claimDetail.IIB.ElecEquip=tempData;
                break;
                case "BURGLARY":
                    tempData.FIRDATE=modifyDate(tempData.FIRDATE);
                    $scope.claimDetail.IIB.Burglary=tempData;
                break;
                case "BPP":
                    tempData.LASTINSPECTIONDATEBYBOILERINSPECTOR=modifyDate(tempData.LASTINSPECTIONDATEBYBOILERINSPECTOR);
                    $scope.claimDetail.IIB.Boilerpress=tempData;
                break;
                case"FIRE":
                    tempData.DATE=modifyDate(tempData.DATE);
                    $scope.claimDetail.IIB.Fire=tempData;
                break;
                default:
                break;
                }
            }
            
            /**************************claim details end******************************************/
        
        
        }]);

    